<?php

// no namespace set, loaded from composer.json autoload/files

/**
 * @todo document this
 */
function locale ()
{
    return Localize::getCurrentLocale();
}


/**
 * Shortcut function to get alternate locale.
 *
 * @return string
 */
function altLocale ()
{
    return LocalizationHelper::getAlternateLocale();
}


/**
 * Shortcut function to get alternate locale URL for the current page.
 *
 * @return  string
 */
function altLocaleURL ()
{
    return LocalizationHelper::getAlternateLocaleURL();
}


/**
 * Get alternate locale name.
 *
 * @return string
 */
function altLocaleName ()
{
    return ucfirst(LocalizationHelper::getLocaleProperty( 'native', altLocale() ));
}


/**
 * @todo
 */
function getResource ( $path, $subfolder = null, $https = false )
{
    if ( $subfolder )
    {
        $path = $subfolder . '/' . $path;
    }

    if ( env('CDN') )
    {
        return env('CDN') . '/' . $path;
    }

    if ( $https )
    {
        return secure_asset($path);
    }

    return asset($path);
}


/**
 * @todo
 */
function img ( $path, $https = false )
{
    return getResource( $path, 'images', $https );
}

/**
 * @todo
 */
function css ( $path, $https = false )
{
    return getResource( $path, 'css', $https ) . '.css';
}


/**
 * @todo
 */
function js ( $path, $https = false )
{
    return getResource( $path, 'js' ) . '.js';
}
