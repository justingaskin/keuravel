<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::group(
[
    'prefix' => Localize::setLocale()
],
function()
{
    // ------------------------------------
    // Caribou Promo
    // ------------------------------------

    Route::get(  '/', 'CaribouController@index');
    Route::post( 'generate', 'CaribouController@generatePdf' );
    Route::get(  Localize::transRoute('routes.form'), 'CaribouController@form' );
    Route::get(  Localize::transRoute('routes.findStore'), 'CaribouController@findStore' );
    Route::get(  Localize::transRoute('routes.end'), 'CaribouController@end' );
    Route::post( Localize::transRoute('routes.submitForm'), 'CaribouController@submitForm' );


    // ------------------------------------
    // Holiday Planner
    // ------------------------------------

    Route::get( Localize::transRoute('routes.planner').'/{section?}',
    [
        'as'   => 'planner',
        'uses' => 'PlannerController@index'
    ]);

});
