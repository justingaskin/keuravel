<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;

class SubmitFormRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname'  => 'required',
            'email' => 'required',
            'address_1' => 'required',
            'city'  => 'required',
            'province'   => 'required',
            'postalcode' => ['required','postalcode'],
            'phone' => ['required','phone'],
            'model' => 'required',
            'purchasedate'  => ['required','purchasedate'],
            'purchasedfrom' => 'required',
            'accepted' => 'accepted',
        ];
    }
}
