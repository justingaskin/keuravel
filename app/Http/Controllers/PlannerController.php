<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitFormRequest;
use App\Coupon;
use App\Services\Mailer;
use PDF;
use Carbon;
use Localize;
use Request;
use Storage;

class PlannerController extends Controller
{
    private $plannerSections =
    [
        'en' => ['gift-guide','tips-recipes','holiday-flavours','party-planner'],
        'fr' => ['guide-cadeaux','conseils-recettes','saveurs','planificateur']
    ];


    // KPP
    public function index ( $section = '' )
    {
        $locale = Localize::getCurrentLocale();

        if ( $section && !in_array( $section, $this->plannerSections[$locale] ) )
        {
            return redirect($locale . '/' . Localize::transRoute('routes.planner'));
        }

        return view('planner.index')->with(compact('section'));
    }

}
