<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitFormRequest;
use App\Coupon;
use App\Services\Mailer;
use PDF;
use Carbon;
use Localize;
use Request;
use Storage;

class CaribouController extends Controller
{

    public function index ()
    {
        $endDate = Carbon::createFromFormat( 'd.m.Y H:i', config('app.end_of_promo') );

        if ( $endDate->isPast() )
        {
            return view('end');
        }

        $countDown = Carbon::now()->diffInDays($endDate);

        return view('discover')->with( compact('countDown') );
    }

    public function form ()
    {
        return view('form');
    }

    public function submitForm (SubmitFormRequest $request, Mailer $mailer)
    {
        $coupon = Coupon::create($request->all());

        $data = $request->all();
        $data['id'] = $coupon->id;
        unset( $data['_token'] );

        $filename = $coupon->id . '00' . time();

        $coupon->filename = $filename;
        $coupon->save();

        PDF::loadView( 'pdf', [ 'id' => $filename ] )->save( sys_get_temp_dir().'/'.$filename.'.pdf' );

        $mailer->sendCouponEmail($coupon);

        return view('thankyou')->with( compact('data') );
    }

    public function generatePdf ()
    {
        Localize::setLocale(Request::input('lang'));

        $coupon = Coupon::find(Request::input('id'));

        return response()->download( sys_get_temp_dir().'/'.$coupon->filename.'.pdf' );
    }

    public function findStore ()
    {
        return view('findastore');
    }

}
