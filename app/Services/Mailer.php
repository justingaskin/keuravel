<?php

namespace App\Services;

use Contest;
use Mail;
use Lang;

/**
 * Handles setting up and sending emails.
 *
 */
class Mailer
{

  /**
   * Sends post-participation email.
   *
   */
  public function sendCouponEmail ( $coupon )
  {
    try
    {
      // send email
      Mail::send( 'emails.coupon', [], function($message) use ( $coupon )
      {

        $message->from( 'noreply@keurig.ca', 'Keurig Canada' );
        $message->subject( Lang::get('mail.subject') );

        if ( env('APP_DEBUG') )
        {
          $message->to( env('DEBUG_EMAIL_RECIPIENT') );
        }
        else
        {
          $message->to( $coupon->email );
        }

        $message->attach( sys_get_temp_dir().'/'.$coupon->filename.'.pdf' );

      });
    }
    catch ( Exception $e )
    {
      error_log($e->message);
    }
  }

}