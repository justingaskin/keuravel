<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend( 'purchasedate', function ($attribute, $value, $parameters)
        {
            if ( !preg_match( '/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/', $value ) )
            {
                return false;
            }

            $date = explode( '/', $value );

            $date =
            [
                'day'   => (int) $date[0],
                'month' => (int) $date[1],
                'year'  => (int) $date[2]
            ];

            if ( $date['day'] > 31 && $date['month'] > 12 && $date['year'] !== 2015 )
            {
                return false;
            }

            $date  = Carbon::createFromDate( $date['year'], $date['month'], $date['day'] );
            $start = Carbon::createFromDate( 2015, 10, 1 );
            $end   = Carbon::createFromDate( 2015, 15, 11 );

            if ( !$date->between( $start, $end ) )
            {
                return false;
            }

            return true;
        });

        Validator::extend( 'phone', function ($attribute, $value, $parameters)
        {
            if ( !preg_match( '/[0-9]{3}-[0-9]{3}-[0-9]{4}/', $value ) )
            {
                return false;
            }

            return true;
        });

        Validator::extend( 'postalcode', function( $attribute, $value, $parameters )
        {
            return preg_match( '/^([A-CEGHJ-NPR-TV-Z]{1}[0-9]{1}){3}$/i', str_replace( ' ', '', $value ));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
