<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;

class Coupon extends Model
{
    protected $table = 'coupons';

    protected $guarded = ['_token'];

    /**
     * Mutator for postal code formatting.
     *
     * @param string $value
     */
    public function setPostalCodeAttribute ( $value )
    {
        $this->attributes['postalcode'] = strtoupper( str_replace( ' ', '', $value ));
    }

        /**
     * Mutator for postal code formatting.
     *
     * @param string $value
     */
    public function setPurchasedateAttribute ( $value )
    {
        $date = explode( '/', $value );

        $this->attributes['purchasedate'] = $date[2] . '-' . $date[1] . '-' . $date[0];
    }
}
