//'use strict';

/*******************************************************************************
DEPENDENCIES
*******************************************************************************/

/*
var gulp   = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass   = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
*/


var gulp = require('gulp'),
    notify = require('gulp-notify'),
    gutil = require('gulp-util'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload  = browserSync.reload,


    /* STYLES DEPENDENCIES */
    //postcss = require('gulp-postcss'),
    sass = require('gulp-sass') ,
    //sourcemaps = require('gulp-sourcemaps'),
    precss = require('precss'),
    autoprefixer = require('gulp-autoprefixer'),
    cmq = require('gulp-combine-media-queries'),


    /* JS DEPENDENCIES */
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    stylish = require('jshint-stylish'),


    /* IMAGES MINIFICATION DEPENDENCIES */
    imageOptim = require('gulp-imageoptim');



    /*

    !! IMPORTANT
    #### Ajustement du plugin de Media Queries
    > Un des plugins de Gulp contient un bug. Voici comment le corriger.

    - Ouvrir le fichier /node_modules/gulp-combine-media-queries/index.js
    - Commenter la ligne 152 : file.contents = new Buffer(cssJson);
    - Enregistrer

    */



/*******************************************************************************
Variables and files destinations
*******************************************************************************/

var path =
{
    scss : 'resources/assets/scss/planner/',
    js   : 'resources/assets/js/planner/'
};


var root_paths = {

    assets : 'public/',
    src : 'resources/assets/'

};


var target = {

    main_css_src : root_paths.src + 'scss/planner/styles.scss',
    css_src : root_paths.src + 'scss/planner/**/*.scss',            // all scss files
    css_dest : root_paths.assets + 'css/planner',                   // where to put minified css

    js_src_all : root_paths.src + 'js/planner/*.js',                // all js files
    js_src : root_paths.src + 'js/planner/',                        // all js files
    js_src_lib : root_paths.src + 'js/planner/lib/',                // all js files
    js_dest : root_paths.assets + 'js/planner',                     // where to put minified js

    img_src : root_paths.src + 'images/**/*.{png,jpg,gif}',         // all img files
    img_dest : root_paths.assets + 'images/planner',                // where to put minified im

};


var site = 'http://dev.kpp.com/fr/guide-des-fetes';



/*******************************************************************************
Process styles.
*******************************************************************************/
function scss ( build )
{
    
    var optns = build === 'build' ? { outputStyle:'compressed' } : {};
    /*
    return gulp.src( path.scss+'styles.scss' )
        .pipe(sass(optns).on('error', sass.logError))
        .pipe(autoprefixer(
        {
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css/planner'));
    */

    return gulp.src(target.css_src)
        .pipe(plumber(function(error){
            gutil.log(gutil.colors.red(error.message))
            this.emit('end')
        }))
        //.pipe(sourcemaps.init())
        .pipe(sass(
            optns
        ))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cmq({
            log: true
        }))
        //.pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(target.css_dest))
        .pipe(reload({stream:true}))
        .pipe(notify('CSS task completed'));
}



/*******************************************************************************
Process scripts.
*******************************************************************************/
function js ( build )
{
    var stream = gulp.src([ target.js_src_lib+'jquery.js', target.js_src_lib+'modernizr-detectzr.js', target.js_src+'scripts.js', target.js_src+'party-planner.js', target.js_src+'facebook-share.js', target.js_src+'gtm.js', ])
        .pipe(concat('scripts.js'));

    if ( build === 'build' )
    {
        stream.pipe(uglify());
    }

    return stream.pipe(gulp.dest(target.js_dest))
}



/*******************************************************************************
IMAGES TASK
*******************************************************************************/
gulp.task('images', function() {
    return gulp.src(target.img_src)
        .pipe(plumber(function(error){
            gutil.log(gutil.colors.red(error.message));
                this.emit('end');
            }))
        .pipe(imageOptim.optimize())
        .pipe(gulp.dest(target.img_dest));
});



/*******************************************************************************
Tasks
*******************************************************************************/
gulp.task ( 'sass:dev', function()
{
    scss('dev');
});


gulp.task ( 'sass:build', function()
{
    scss('build');
});


gulp.task ( 'js:dev', function()
{
    js('dev');
});


gulp.task ( 'js:build', function()
{
    js('build');
})



/*******************************************************************************
Default tasks
*******************************************************************************/
gulp.task( 'default', [ 'sass:dev', 'js:dev' ]);
gulp.task( 'build',   [ 'sass:build', 'js:build' ]);


gulp.task('browser-sync', function() {
    browserSync({
        proxy: site,
        tunnel: false // mettre a true si on veut un url accessible de l'extérieur
    });
});



/*******************************************************************************
Watch
*******************************************************************************/

gulp.task('watch', ['browser-sync'], function() {

    gulp.watch(target.css_src, ['sass:build']);                // Watch .css files
    gulp.watch(target.js_src_all, ['js:build']);                 // Watch .js files

    //gulp.watch( [ path.scss+'**/*.scss' ], ['sass:dev'] );
    //gulp.watch( [ path.js+'**/*.js' ],     ['js:dev']   );

});
