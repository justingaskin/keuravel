$(function() {
    //Menu mobile
    $('#btn-menu').on('click', function(){
    	$('.main-menu.mobile-only ul').slideToggle();
    });
    //Popup
    $('.how-it-works .journey .block.three').on('click', function(){
		$('.overlay').fadeIn();
    	$('.popup.pop1').fadeIn();
    });
    $('.how-it-works .journey .block.five').on('click', function(){
		$('.overlay').fadeIn();
    	$('.popup.pop2').fadeIn();
    });
    $('.offer .compare').on('click', function(){
		$('.overlay').fadeIn();
    	$('.popup.popcompare').fadeIn();
    });
    $('.close, .overlay').on('click',function(){
    	$('.popup, .overlay').fadeOut();
    });
    // TRACKING
        //GENERAL
            //Purchased in store
            $(document).on('click', ".purchasedinstore", function () {
                utag.link({ga_category:'caribou-2-0-page', ga_actions: 'header', ga_label: 'purchased-in-store'});
            });
        //HOMEPAGE
            //Purchased in store Banner
            $(document).on('click', ".banner .container .circle a", function () {
                utag.link({ga_category:'caribou-2-0-page', ga_actions: 'main-banner', ga_label: 'purchased-in-store-get-your-free-pods-here'});
            });
            // //Buy in store
             // $(document).on('click', ".products .btn", function () {
             //     var idProd = $(this).attr('class').substring(3);
             //     utag.link({ga_category:'caribou-2-0-page-find-a-store', ga_actions: 'step1', ga_label: idProd});
             // });
            //Buy in store
            $(document).on('click', ".products .bis", function () {
                var idProd = $(this).attr('data-brewername');
                utag.link({ga_category:'caribou-2-0-page-find-a-store', ga_actions: 'step1', ga_label: idProd});
            });
             //Compare
            $(document).on('click', ".offer .compare .comp", function () {
                utag.link({ga_category:'caribou-2-0-page', ga_actions: 'step1', ga_label: 'compare-all-brewers'});
            });
            //Retailers
            $(document).on('click', ".journey.grey .block.first a", function () {
                utag.link({ga_category:'caribou-2-0-page-find-a-store', ga_actions: 'step2', ga_label: 'retailers'});
            });
            //Redeem here
            $(document).on('click', ".journey.grey .block.two a", function () {
                utag.link({ga_category:'caribou-2-0-page', ga_actions: 'step2', ga_label: 'redeem-your-offer'});
            });
        //PAGE FORM
            //Popup 1
            $(document).on('click', ".how-it-works .journey .block.three .hover-box", function () {
               utag.link({ga_category:'caribou-2-0-form', ga_actions: 'rollover', ga_label: 'upc-details'});
            });
            //Popup use coupon
            $(document).on('click', ".how-it-works .journey .block.five .hover-box", function () {
                utag.link({ga_category:'caribou-2-0-form', ga_actions: 'main-banner', ga_label: 'use-your-coupon-at-keurig.ca'});
            });
            //Block FAQ
            $(document).on('click', ".form .blocks .block.faq a.btn", function () {
                utag.link({ga_category:'caribou-2-0-form', ga_actions: 'faq', ga_label: 'learn-more'});
            });
            //Block Get in Touch
            $(document).on('click', ".form .blocks .block.git a.btn", function () {
                utag.link({ga_category:'caribou-2-0-form', ga_actions: 'want-to-get-in-touch-with-us', ga_label: 'learn-more'});
            });
            // Newsletter
            if( $('#generatePdfForm [name="contact"]').length){
                utag.link({ga_category:'newsletter', ga_actions: 'subscription', ga_label: 'caribou-2-0',  ga_nonInteraction: 1});
            }




    var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

    if ( viewportWidth > 1024 && $('#generatePdfForm').length )
    {
        window.setTimeout( function()
        {
            $('#generatePdfForm').submit();
        }
        , 1000 );
    }




    // show/hide other retailer field

    function updateOtherRetailerField ()
    {
        var form     = $('#requestCouponForm');
        var retailer = form.find('[name=purchasedfrom]');
        var other    = form.find('[name=purchasedfromother]');

        if ( retailer.val() === 'OTHER' )
        {
            other.parents('div').addClass('show');
        }
        else
        {
            other.val('').parents('div').removeClass('show');
        }
    }

    $('#requestCouponForm [name=purchasedfrom]').on( 'change', updateOtherRetailerField );

})
