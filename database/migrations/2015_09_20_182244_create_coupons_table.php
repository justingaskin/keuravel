<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email');
            $table->char('lang',2);
            $table->string('address_1');
            $table->string('address_2');
            $table->string('city');
            $table->char('province',2);
            $table->string('postalcode');
            $table->string('phone');
            $table->string('model');
            $table->date('purchasedate');
            $table->string('purchasedfrom');
            $table->string('purchasedfromother');
            $table->boolean('contact');
            $table->boolean('accepted');
            $table->string('filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coupons');
    }
}
