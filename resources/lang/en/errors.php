<?php return
[

    '404.text' => 'Page not found.',
    '500.text' => 'Oops, something went wrong.'

];