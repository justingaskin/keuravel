<?php

return
[
    'form'        => 'already-purchased',
    'submitForm'  => 'get-coupon',
    'findStore'   => 'find-a-store',
    'planner'     => 'holiday-guide'
];
