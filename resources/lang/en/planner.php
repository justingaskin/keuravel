<?php return
[
	'site.url' => 'holiday-guide',

	// ******************************************************
	// Home
	'home.title' => 'Holiday Planner',
	'home.welcome' => 'Brew up some holiday cheer with Keurig<sup>®</sup> <br /> and make the most of every moment.',
	'home.welcome.meta' => 'Brew up some holiday cheer with Keurig® and make the most of every moment.',

	'home.loading' => 'Loading in progress',


	// ******************************************************
	// Holidays Tips
	'page.holiday-tips.title' => 'Holiday Tips',
	'page.holiday-tips.url' => 'tips-recipes',

		// Generic
		'page.holiday-tips.subtitle' => 'We trust our assortment of holiday tips and tricks will have you savouring every single minute of the season. From decadent dessert recipes to fun and festive activities, Keurig<sup>®</sup> is bringing out the best of the holidays.',
		'page.holiday-tips.quicktips.title' => 'Quick tips',
		'page.holiday-tips.ingredients.title' => 'Ingredients',
		'page.holiday-tips.preparation.title' => 'Preparation',
		'page.holiday-tips.tip.title' => 'Tip',

		// Quick tips
		'page.holiday-tips.quicktips1.content' => 'Think of your teetotallers! Stock up on a variety of <a href="http://www.keurig.ca/beverages/tea/c/tea101" target="_blank" title="Keurig® K-Cup® pods" class="tracking-ga" data-url-ga="keurig-k-cup-pods">Keurig<sup>®</sup> K-Cup<sup>®</sup> pods</a>.',
		'page.holiday-tips.quicktips2.content' => 'Keep the kids entertained by hosting a snowman-building competition! Don’t forget the <a href="http://www.keurig.ca/search?text=+hot+chocolate" target="_blank" title="hot chocolate" class="tracking-ga" data-url-ga="hot-chocolate">hot chocolate</a>.',
		'page.holiday-tips.quicktips3.content' => 'Purchase your turkey sooner rather than later.',
		'page.holiday-tips.quicktips4.content' => 'Set up a Holiday Photo Booth to capture all of those great memories!',
		'page.holiday-tips.quicktips5.content' => 'Stock up on <a href="http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee" target="_blank" title="Keurig® holiday blends K-Cup® pods" class="tracking-ga" data-url-ga="keurig-holiday-blends-k-cup-pods">Keurig® holiday blends K-Cup® pods</a> to really get you and your guests in the spirit!',
		'page.holiday-tips.quicktips6.content' => 'Gingerbread men cookies make everyone happy!',
		'page.holiday-tips.quicktips7.content' => 'Invite guests to take part in a gleeful gift exchange!',


		// Article - Tips for Hosting an Ugly Sweater Party
		'page.holiday-tips.article.sweater-party.title' => 'Tips for Hosting an Ugly Sweater Party',

		'page.holiday-tips.article.sweater-party.intro-1' => 'Ugly holiday sweater parties are great for any crowd, especially one with a great sense of humor! You don’t have to break the bank on an ugly vintage sweater either: as these parties have become more popular, retailers have begun selling new “ugly sweaters” for just the occasion! Pressed for time? Sift through grandma’s closet! You may find the perfect light-up reindeer turtleneck!',
		'page.holiday-tips.article.sweater-party.intro-2' => 'The invites have been sent out… now what? We’ve got some ideas.',

		'page.holiday-tips.article.sweater-party.subtitle-1' => 'Set up a photobooth',
		'page.holiday-tips.article.sweater-party.content-1' => 'Capture all of the hilarious outfits with a designated photo booth. This can be as simple as a selfie stick, some rather tacky wrapping paper taped on your wall and a few props. If you’re feeling crafty, there are some truly outrageous ideas you can check out on Pinterest. At the end of the night, gather all the best shots and share!',

		'page.holiday-tips.article.sweater-party.subtitle-2' => 'Vote for the best!',
		'page.holiday-tips.article.sweater-party.content-2-1' => 'One of the best parts of hosting an ugly sweater contest is the moment someone new walks through the door. Every party has its own gems, those few who truly go above and beyond their calling, and who deserve recognition. Once everyone has arrived, distribute voting cards for the best costumes. Once the votes are tallied, gather everyone and present the winners with some tacky prizes and bragging rights!',
		'page.holiday-tips.article.sweater-party.content-2-2' => 'Here are a few ideas for your voting cards:',
		'page.holiday-tips.article.sweater-party.content-2-3' => '
			<li>Best DIY</li>
			<li>Ugliest sweater</li>
			<li>Most shameful</li>
			<li>Most original</li>
		',

		'page.holiday-tips.article.sweater-party.subtitle-3' => 'Serve food worthy of an ugly sweater Party',
		'page.holiday-tips.article.sweater-party.content-3-1' => 'Poke fun at the tackiness the holidays can sometimes take and don’t forget to serve fruitcake. For sugary treats, have a cookie decoration station with gingerbread, or a place to ice truly outrageous cupcakes. Decorate a sheet cake with an ugly sweater design or have a local bakery bake up a creation just for the occasion!',
		'page.holiday-tips.article.sweater-party.content-3-2' => '<strong>Bonus tip:</strong> Ugly sweater parties are a great place to play games that are a little on the silly side. Now’s the time to play all of those wacky party games you’ve seen online!',

		'page.holiday-tips.article.sweater-party.urlID' => 'tips-hosting-ugly-sweater-party',


		// Article - Gingerbread Spice Latte
		'page.holiday-tips.article.gingerbread.title' => 'Gingerbread Spice Latte',

		'page.holiday-tips.article.gingerbread.intro' => 'We gave this seasonal sweet the drink it deserves.',
		'page.holiday-tips.article.gingerbread.ingredients' => '
			<li><a href="http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee" target="_blank" title="1 Van Houtte® Holiday Blend K-Cup® pod" class="tracking-ga" data-url-ga="van-houtte-holiday-blend-k-cup-pod">1 Van Houtte® Holiday Blend K-Cup<sup>®</sup> pod</a></li>
			<li>1/3 teaspoon (2 ml) ground ginger</li>
			<li>1/4 teaspoon (1.5 ml) ground cinnamon</li>
			<li>Pinch of ground nutmeg</li>
			<li>Pinch of ground cloves</li>
			<li>1/4 tablespoon (4 ml) vanilla extract</li>
			<li>Sugar (to taste)</li>
			<li>1 cup (250 ml) milk</li>
			<li>Whipped cream</li>
		',
		'page.holiday-tips.article.gingerbread.preparation' => '
			<li>Brew 120 ml (4 oz.) of Van Houtte<sup>®</sup> Holiday Blend coffee directly into a 415 ml (14 oz.) cup.</li>
			<li>Add all the spices and the vanilla.</li>
			<li>Stir and sweeten to taste.</li>
			<li>Froth the milk and add to coffee.</li>
			<li>Top with whipped cream.</li>
		',

		'page.holiday-tips.article.gingerbread.urlID' => 'gingerbread-spice-latte',


		// Article - Cappuccino Pudding 
		'page.holiday-tips.article.cappucino.title' => 'Cappuccino Pudding',

		'page.holiday-tips.article.cappucino.intro' => 'A truly fun recipe to make, using mostly pantry staples.',
		'page.holiday-tips.article.cappucino.ingredients' => '
			<li>1 french vanilla flavoured coffee K-Cup<sup>®</sup> pod, brewed at 6-ounce setting</li>
			<li>1 envelope gelatin</li>
			<li>½ cup sweetened condensed milk</li>
			<li>½ cup ice cubes (about 4)</li>
			<li>Whipped cream</li>
		',
		'page.holiday-tips.article.cappucino.preparation' => '
			<li>Sprinkle gelatin over coffee, whisk together and allow to bloom 2 minutes.</li>
			<li>Transfer to blender, add sweetened condensed milk and blend on low speed for 2 minutes or until gelatin is completely dissolved.</li>
			<li>Add ice cubes, 1 at a time, blending after each until ice is melted.</li>
			<li>Pour into 4 parfait glasses.</li>
			<li>Refrigerate 2 hours or until set.</li>
			<li>Top with whipped cream.</li>
		',
		'page.holiday-tips.article.cappucino.portion' => 'Makes 4 servings',
		'page.holiday-tips.article.cappucino.suggestion.title' => 'SUGGESTED K-CUP<sup>®</sup> POD',
		'page.holiday-tips.article.cappucino.suggestion.content-1' => '<a href="http://www.keurig.ca/Beverages/Coffee/French-Vanilla-Coffee/p/french-vanilla-coffee-van-houtte-k-cup" target="_blank" title="Van Houtte® French Vanilla" class="tracking-ga" data-url-ga="van-houtte-french-vanilla">Van Houtte<sup>®</sup> French Vanilla</a>',
		'page.holiday-tips.article.cappucino.suggestion.content-2' => 'Perfection, in balance. A harmony of rich, creamy coffee and delicate vanilla.',
		'page.holiday-tips.article.cappucino.tip.content' => 'Open up a K-Cup<sup>®</sup> pod and sprinkle some coffee grounds over the top of each pudding. ',

		'page.holiday-tips.article.cappucino.urlID' => 'cappucino-pudding',


		// Article - Candy Cane Latte
		'page.holiday-tips.article.candy-cane.title' => 'Candy Cane Latte',

		'page.holiday-tips.article.candy-cane.intro' => 'Toast to friends of all ages with this sugary holiday treat!',
		'page.holiday-tips.article.candy-cane.ingredients' => '
			<li><a href="http://www.keurig.ca/Beverages/Christmas-Blend/p/christmas-blend-timothys-coffee" target="_blank" title="1 Timothy’s® Christmas Blend K-Cup® pod" class="tracking-ga" data-url-ga="timothys-christmas-blend-k-cup-pod">1 Timothy’s<sup>®</sup> Christmas Blend K-Cup<sup>®</sup> pod</a></li>
			<li>120 ml (4 oz.) milk</li>
			<li>1 crushed candy cane</li>
			<li>Whipped cream</li>
		',
		'page.holiday-tips.article.candy-cane.preparation' => '
			<li>Place the crushed candy cane in a 415 ml (14 oz.) cup, and reserve a few pieces.</li>
			<li>Brew 175 ml (6 oz.) of Timothy’s<sup>®</sup> Holiday Blend coffee directly into the cup with the crushed candy cane.</li>
			<li>Stir well to make sure the candy cane has melted.</li>
			<li>Froth the milk and add to the coffee</li>
			<li>Top with whipped cream.</li>
			<li>Garnish with crushed candy cane.</li>
		',

		'page.holiday-tips.article.candy-cane.urlID' => 'candy-cane-latte',


		// Article - Poached Pears 
		'page.holiday-tips.article.poached-pears.title' => 'Poached Pears',

		'page.holiday-tips.article.poached-pears.intro' => 'A very elegant dessert you can make in just a few minutes.',
		'page.holiday-tips.article.poached-pears.portion' => 'Makes 4 servings.',
		'page.holiday-tips.article.poached-pears.ingredients' => '
			<li>1 Colombian coffee K-Cup<sup>®</sup> pod, brewed at 8-ounce setting</li>
			<li>2 cups water</li>
			<li>⅓ cup brown sugar</li>
			<li>4 Bosc pears, peeled, halved and cored</li>
		',
		'page.holiday-tips.article.poached-pears.content-1' => 'Combine coffee, water and brown sugar in large skillet. Add pears, bring liquid to a boil, then lower heat and simmer 8 minutes. Carefully flip pears over using rubber spatula and poach another 10 minutes or until pears are tender and liquid has formed a syrup. To serve, drizzle syrup over pears.',
		'page.holiday-tips.article.poached-pears.suggestion.title' => 'SUGGESTED K-CUP<sup>®</sup> POD',
		'page.holiday-tips.article.poached-pears.suggestion.content-1' => '<a href="http://www.keurig.ca/Beverages/Coffee/Colombia-Coffee/p/colombia-coffee-barista-prima-k-cup" target="_blank" title="Barista Prima Coffeehouse® Colombia Coffee" class="tracking-ga" data-url-ga="barista-prima-coffeehouse-colombia-coffee">Barista Prima Coffeehouse<sup>®</sup> Colombia Coffee</a>',
		'page.holiday-tips.article.poached-pears.suggestion.content-2' => 'The magic of the Andes right in your cup. Highly-prized Arabica beans accented by bold fruit notes and a distinctive hint of walnut.',
		'page.holiday-tips.article.poached-pears.tip.content' => 'Raspberries make a perfect garnish, with their vivid color and restrained sweetness. Serve with ice cream or on its own.',

		'page.holiday-tips.article.poached-pears.urlID' => 'poached-pears',


		// Article - Craft a D.I.Y. gift basket for your guests
		'page.holiday-tips.article.gift-basket.title' => 'Craft a D.I.Y. gift basket for your guests',

		'page.holiday-tips.article.gift-basket.intro' => 'When you’re a serial gifter, crafting personalized gift baskets for party guests to bring home is the way to go. We’ve dreamed up four themed baskets that add something special to your gift-giving. Put a bow on some new classics!',
		'page.holiday-tips.article.gift-basket.goodies-title' => 'Goodies we suggest :',

		'page.holiday-tips.article.gift-basket.subtitle-1' => 'Home for the holidays',
		'page.holiday-tips.article.gift-basket.content-1' => 'Our “home for the holidays” colourful basket is filled to the brim with all of those little accents that remind you of the holiday season. Make it personal with a small token and note from your family’s traditions, or a home-cooked treat! ',
		'page.holiday-tips.article.gift-basket.goodies-1' => '
			<li><a href="http://www.keurig.ca/Beverages/Hot-Apple-Cider-Mix/p/apple-cider-green-mountain-k-cup" target="_blank" title="Green MountainTM Hot Apple Cider Mix" class="tracking-ga" data-url-ga="green-mountain-hot-apple-cider-mix">Green Mountain<sup>TM</sup> Hot Apple Cider Mix</a></li>
			<li>Flavoured popcorn</li>
			<li>Peppermint-scented hand soap</li>
			<li>Cinnamon candles</li>
			<li>Snowflake cookie cutters</li>
		',

		'page.holiday-tips.article.gift-basket.subtitle-2' => 'Sugar and spice ',
		'page.holiday-tips.article.gift-basket.content-2' => 'No matter how you celebrate the holidays, we’re sure baking is on your list. Our “Sugar & spice” candy jar has all of the accessories and treats you’ll need for holiday cooking! Attach a family recipe with its needed ingredients in a mason jar so the giftee can get a little taste of your holiday baking! ',
		'page.holiday-tips.article.gift-basket.goodies-2' => '
			<li>Custom-engraved rolling pin</li>
			<li>Cookies decorating kit</li>
			<li><a href="http://www.keurig.ca/Beverages/Hot-Cocoa/Hot-Chocolate-Mix/p/hot-chocolate-mix-laura-secord-k-cup" target="_blank" title="Laura Secord® Hot Chocolate Mix" class="tracking-ga" data-url-ga="laura-secord-hot-chocolate-mix">Laura Secord<sup>®</sup> Hot Chocolate Mix</a></li>
			<li>Pure vanilla extract</li>
			<li>Festive tea towels </li>
		',

		'page.holiday-tips.article.gift-basket.subtitle-3' => 'Warm and fuzzy ',
		'page.holiday-tips.article.gift-basket.content-3' => 'There’s no denying that the holidays are hectic, which is where our “Warm and fuzzy” tin bucket comes in. Not only can all of these things be used throughout the year, but they are also the perfect little package for those who forget to treat themselves. Incorporate a favourite film, or a mug that references an inside joke to personalize.',
		'page.holiday-tips.article.gift-basket.goodies-3' => '
			<li>Warm and soft throw</li>
			<li>Cozy slippers</li>
			<li>Candied walnuts</li>
			<li>Lavender essential oils</li>
			<li><a href="http://www.keurig.ca/Beverages/Christmas-Blend/p/christmas-blend-timothys-coffee" target="_blank" title="Timohy’s® Christmas Blend" class="tracking-ga" data-url-ga="timohys-christmas-blend">Timohy’s<sup>®</sup> Christmas Blend</a></li>
			<li><a href="http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee" target="_blank" title="Van Houtte® Holiday Blend" class="tracking-ga" data-url-ga="van-houtte-holiday-blend">Van Houtte<sup>®</sup> Holiday Blend</a></li>
		',

		'page.holiday-tips.article.gift-basket.subtitle-4' => 'Roasting on an open fire',
		'page.holiday-tips.article.gift-basket.content-4' => 'The winter months are perfect for spending nights around a crackling fire. Our “roasting on an open fire” canvas tote is packed with all the items you need for those days when it’s cold outside. Personalize by slipping in a favourite novel that you think your friend would enjoy too! ',
		'page.holiday-tips.article.gift-basket.goodies-4' => '
			<li>Decorative fire starter</li>
			<li>Smores kit</li>
			<li>Cozy plaid scarf</li>
			<li>Cinnamon sticks </li>
			<li><a href="http://www.keurig.ca/Accessories/White-ceramic-travel-mug/p/holiday-travel-mug" target="_blank" title="White Ceramic Travel Mug" class="tracking-ga" data-url-ga="white-ceramic-travel-mug">White Ceramic Travel Mug</a></li>
			<li>Waxed canvas fire carrier </li>
		',

		'page.holiday-tips.article.gift-basket.urlID' => 'craft-diy-gift-basket',


		// Article - Elf’s Latte
		'page.holiday-tips.article.elf-latte.title' => 'Elf’s Latte',

		'page.holiday-tips.article.elf-latte.intro' => 'Keep festivities sweet with this simple yet savory latte!',
		'page.holiday-tips.article.elf-latte.ingredients' => '
			<li><a href="http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee" target="_blank" title="1 Van Houtte® Holiday Blend K-Cup® pod" class="tracking-ga" data-url-ga="van-houtte-holiday-blend-k-cup-pod">1 Van Houtte<sup>®</sup> Holiday Blend K-Cup<sup>®</sup> pod</a></li>
			<li>120 ml (4 oz.) skim milk</li>
			<li>30 ml (1 oz.) Irish cream liqueur</li>
			<li>Whipped cream</li>
		',
		'page.holiday-tips.article.elf-latte.preparation' => '
			<li>Brew 175-235 ml (6-8 oz.) of Van Houtte® Holiday Blend coffee directly into a 415 ml (14 oz.) mug.</li>
			<li>While the coffee is brewing, warm the milk using a milk frother.</li>
			<li>When coffee is ready, add 120 ml (4 oz.) of warm milk to coffee.</li>
			<li>Add the Irish cream liqueur.<sup>*</sup></li>
			<li>Top with whipped cream.</li>
		',
		'page.holiday-tips.article.elf-latte.content' => '<sup>*</sup>Please drink responsibly',

		'page.holiday-tips.article.elf-latte.urlID' => 'elfs-latte',


		// Article - Wow your crowd with a stunning table presentation
		'page.holiday-tips.article.table-presentation.title' => 'Wow your crowd with a stunning table presentation',

		'page.holiday-tips.article.table-presentation.intro' => 'Dinner parties are a great way to gather loved ones during the holiday season, but they can get pretty overwhelming. While we’re not all Martha’s, there are many ways to bring a unique elegance to your table setting. Find tips and tricks below to jazz up your table presentation!',

		'page.holiday-tips.article.table-presentation.subtitle-1' => 'Invite nature indoors',
		'page.holiday-tips.article.table-presentation.content-1' => 'While we’re bombarded with imitation trees and wreaths in the stores, we sometimes forget the simplicity of utilizing the flora that surrounds us. One way to breathe life into your table is adorning it with natural elements: embellish your place cards with a sprig of rosemary, mistletoe or a spruce twig, for instance. Another quick trick is filling large glass vases with pine cones and battery-operated copper string lights.',

		'page.holiday-tips.article.table-presentation.subtitle-2' => 'Welcome home-grown charm',
		'page.holiday-tips.article.table-presentation.content-2' => 'Incorporate quirky and cool pieces into your setting to set your table apart from the rest. Use vintage Christmas decorations as accent pieces, placing mismatched retro decorative plates at each seat or dusting off your mother’s old holiday tableware. Just be sure to not overdo it, as it may come off as tacky.',

		'page.holiday-tips.article.table-presentation.subtitle-3' => 'Bring heart to the table',
		'page.holiday-tips.article.table-presentation.content-3-1' => 'While a rousing game of Yankee Swap may cloud your judgement, don’t lose sight of the reason why you’ve invited all of these people to your home! When everyone is seated together enjoying their meal, remember to toast your guests and speak to what you’re thankful for. If it’s a mix of guests from different parts of your life, slip a few unique conversation starters on cardstock underneath their cutlery. If you come from a family that has an  extraordinary holiday tradition, incorporate it into your dinner to get your guests talking about favourites from their home. ',
		'page.holiday-tips.article.table-presentation.content-3-2' => '<strong>Remember:</strong> By keeping it simple, and focusing your energy on certain pieces that will truly stand out, you’ll be able to actually ENJOY the company without having to rush all of those final touches throughout the night! ',

		'page.holiday-tips.article.table-presentation.urlID' => 'wow-crowd-stunning-table-presentation',


		// Article - Wild mushroom coffee risotto
		'page.holiday-tips.article.mushroom-risotto.title' => 'Wild mushroom coffee risotto',

		'page.holiday-tips.article.mushroom-risotto.intro' => 'Enhance this creamy and cheesy risotto with wild mushrooms and a subtle coffee flavour and give a new twist to this comfort food favourite! Serve as a main dish with a vegetables or salad or as an appetizer for a fancy dinner at home. Remember: the key to a perfect risotto is patience and lots of stirring.',
		'page.holiday-tips.article.mushroom-risotto.portion' => 'Makes 4 serving.',
		'page.holiday-tips.article.mushroom-risotto.ingredients' => '
			<li>8 cups (2 litres) veal stock (or chicken stock)</li>
			<li>1 shallot, finely chopped</li>
			<li>2 tablespoons (30 ml) butter</li>
			<li>2 tablespoons (30 ml) olive oil</li>
			<li>350 g wild mushrooms (whole chanterelles and sliced café mushrooms)</li>
			<li>2 cups (500 ml) Arborio rice</li>
			<li>1 tablespoon (15 ml) full-bodied, Bold & Woodsy coffee (<a href="http://www.keurig.ca/Boissons/Caf%C3%A9/M%C3%A9lange-fran%C3%A7ais/p/french-roast-coffee-van-houtte-k-cup" target="_blank" title="Van Houtte® French Roast Dark Roast" class="tracking-ga" data-url-ga="van-houtte-french-roast-dark-roast">Van Houtte<sup>®</sup> French Roast Dark Roast</a>), finely ground</li>
			<li>1 cup (250 ml) white wine</li>
			<li>Water, as needed</li>
			<li>½ cup (125 ml) grated parmesan cheese</li>
			<li>Salt and pepper to taste</li>
		',
		'page.holiday-tips.article.mushroom-risotto.preparation' => '
			<li>In a small saucepan, bring the veal stock to a boil over high heat.</li>
			<li>In a large skillet, sweat the shallot in butter and olive oil over medium heat. Add the mushrooms and cook for 2 to 3 minutes.</li>
			<li>Add the Arborio rice and Van Houtte<sup>®</sup> ground coffee. Stir.</li>
			<li>Pour in the white wine, increase heat to medium-high and bring to a boil while stirring.</li>
			<li>Stir in veal stock to the rice mixture, a ladleful at a time (about ½ cup [125 ml]). Bring to a boil and reduce after each ladleful of stock, stirring constantly.</li>
			<li>When there is no more stock to add, taste the rice. If it is cooked (tender but still slightly crunchy), remove the pan from the heat. Otherwise, continue to add water in the same manner as the veal stock until cooked.</li>
			<li>Once removed from heat, add the parmesan and salt and pepper to taste. Mix.</li>
			<li>Serve immediately.</li>
		',

		'page.holiday-tips.article.mushroom-risotto.urlID' => 'wild-mushroom-coffee-risotto',



	// ******************************************************
	// Party Planner
	'page.party-planner.title' => 'Party Planner',
	'page.party-planner.url' => 'party-planner',

	'page.party-planner.subtitle' => 'The holidays are made of magical moments. Spend more time enjoying your guests’ company and less time planning your party. Thanks to this quick quiz, you’ll be the host with the most in a flash!',
	'page.party-planner.articles.title' => 'Step up your party with these enchanting ideas!',

		'page.party-planner.event.title' => 'What kind of event are you planning?',
		'page.party-planner.event.question-1' => 'Holiday dinner',
		'page.party-planner.event.question-1.clean-url' => 'holiday-dinner',

		'page.party-planner.event.question-2' => 'Cozy brunch',
		'page.party-planner.event.question-2.clean-url' => 'cozy-brunch',

		'page.party-planner.event.question-3' => 'Celebratory cocktail',
		'page.party-planner.event.question-3.clean-url' => 'celebratory-cocktail',

		'page.party-planner.guest.title' => 'How many guests are you expecting?',
		'page.party-planner.guest.question-1' => '1-5',
		'page.party-planner.guest.question-1.clean-url' => '1-5',

		'page.party-planner.guest.question-2' => '5-10',
		'page.party-planner.guest.question-2.clean-url' => '5-10',

		'page.party-planner.guest.question-3' => '10+',
		'page.party-planner.guest.question-3.clean-url' => '10-more',

		'page.party-planner.mood.title' => 'What mood are you going for?',
		'page.party-planner.mood.question-1' => 'Heartwarming',
		'page.party-planner.mood.question-1.clean-url' => 'heartwarming',

		'page.party-planner.mood.question-2' => 'Eccentric',
		'page.party-planner.mood.question-2.clean-url' => 'eccentric',

		'page.party-planner.mood.question-3' => 'Elegant',
		'page.party-planner.mood.question-3.clean-url' => 'elegant',

		'page.party-planner.submit.title' => 'Plan my party',
		'page.party-planner.result.title' => 'Get the party started!',

		'page.party-planner.button.buy' => 'Shop',

		// Brewer
			// K200
			'page.party-planner.result.brewer-1.title' => 'Keurig<sup>®</sup> 2.0 K200',
			'page.party-planner.result.brewer-1.subtitle' => 'Brewing system',
			'page.party-planner.result.brewer-1.url' => 'http://www.keurig.ca/brewers/home/c/home101?q=200%3Arelevance&page=0&layoutStatus=grid&show=Page&text=200&categoryCode=home101&terms=&selectedFacets=',
			'page.party-planner.result.brewer-1.url-clean' => 'keurig-2-0-k200-brewing-system',

			// K400
			'page.party-planner.result.brewer-2.title' => 'Keurig<sup>®</sup> 2.0 K400',
			'page.party-planner.result.brewer-2.subtitle' => 'Brewing system',
			'page.party-planner.result.brewer-2.url' => 'http://www.keurig.ca/brewers/home/c/home101?q=400%3Arelevance&page=0&layoutStatus=grid&show=Page&text=400&categoryCode=home101&terms=&selectedFacets=',
			'page.party-planner.result.brewer-2.url-clean' => 'keurig-2-0-k400-brewing-system',

			// K500
			'page.party-planner.result.brewer-3.title' => 'Keurig<sup>®</sup> 2.0 K500',
			'page.party-planner.result.brewer-3.subtitle' => 'Brewing system',
			'page.party-planner.result.brewer-3.url' => 'http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K500-Brewing-System/p/Keurig-2-0-K500-Brewing-System',
			'page.party-planner.result.brewer-3.url-clean' => 'keurig-2-0-k500-brewing-system',

			// KOLD
			'page.party-planner.result.brewer-4.title' => 'Keurig<sup>®</sup> KOLD<sup>TM</sup>',
			'page.party-planner.result.brewer-4.subtitle' => 'Drinkmaker',
			'page.party-planner.result.brewer-4.url' => 'http://www.keurig.ca/Drinkmaker/Keurig%C2%AE-KOLD%E2%84%A2-Drinkmaker/p/Keurig-KOLD-G360-drinkmaker',
			'page.party-planner.result.brewer-4.url-clean' => 'keurig-kold-drinkmaker',

		// Coffee
			// Coffee Results HOT(5)
			'page.party-planner.result.coffee-1-1.title' => 'All Time Favourites Variety Box',
			'page.party-planner.result.coffee-1-1.subtitle' => '&nbsp;',
			'page.party-planner.result.coffee-1-1.url' => 'http://www.keurig.ca/Beverages/Holiday-Blend/p/c/Variety-Box---All-Time-Favourite-Coffees/p/all-time-favourite-variety-keurig-k-cup',
			'page.party-planner.result.coffee-1-1.url-clean' => 'all-time-favourites-variety-box',

			'page.party-planner.result.coffee-1-2.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-1-2.subtitle' => 'Holiday Blend',
			'page.party-planner.result.coffee-1-2.url' => 'http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee',
			'page.party-planner.result.coffee-1-2.url-clean' => 'van-houtte-holiday-blend',
			
			'page.party-planner.result.coffee-1-3.title' => ' Timothy’s<sup>®</sup>',
			'page.party-planner.result.coffee-1-3.subtitle' => 'Christmas Blend',
			'page.party-planner.result.coffee-1-3.url' => 'http://www.keurig.ca/Beverages/Christmas-Blend/p/christmas-blend-timothys-coffee',
			'page.party-planner.result.coffee-1-3.url-clean' => ' timothys-christmas-blend',

			// Coffee Results HOT (5-10)
			'page.party-planner.result.coffee-2-1.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-2-1.subtitle' => 'Colombian',
			'page.party-planner.result.coffee-2-1.url' => 'http://www.keurig.ca/Beverages/Coffee/Colombian-Coffee/p/colombian-medium-coffee-VH-k-carafe',
			'page.party-planner.result.coffee-2-1.url-clean' => 'van-houtte-colombian',
			
			'page.party-planner.result.coffee-2-2.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-2-2.subtitle' => 'Holiday Blend',
			'page.party-planner.result.coffee-2-2.url' => 'http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee',
			'page.party-planner.result.coffee-2-2.url-clean' => 'van-houtte-holiday-blend',
			
			'page.party-planner.result.coffee-2-3.title' => 'Timothy’s<sup>®</sup>',
			'page.party-planner.result.coffee-2-3.subtitle' => 'Christmas Blend',
			'page.party-planner.result.coffee-2-3.url' => 'http://www.keurig.ca/Beverages/Christmas-Blend/p/christmas-blend-timothys-coffee',
			'page.party-planner.result.coffee-2-3.url-clean' => 'timothys-christmas-blend',
			
			'page.party-planner.result.coffee-2-4.title' => 'Laura Secord<sup>®</sup>',
			'page.party-planner.result.coffee-2-4.subtitle' => 'Hot Chocolate Mix',
			'page.party-planner.result.coffee-2-4.url' => 'http://www.keurig.ca/Boissons/Chocolat-Chaud/M%C3%A9lange-%C3%A0-chocolat-chaud/p/hot-chocolate-mix-laura-secord-k-cup',
			'page.party-planner.result.coffee-2-4.url-clean' => 'laura-secord-hot-chocolate-mix',

			// Coffee Results HOT(10+)
			'page.party-planner.result.coffee-3-1.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-3-1.subtitle' => 'French Roast',
			'page.party-planner.result.coffee-3-1.url' => 'http://www.keurig.ca/Beverages/French-Roast-Coffee/p/french-roast-coffee-van-houtte-k-carafe',
			'page.party-planner.result.coffee-3-1.url-clean' => 'van-houtte-french-roast',

			'page.party-planner.result.coffee-3-2.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-3-2.subtitle' => 'Holiday Blend',
			'page.party-planner.result.coffee-3-2.url' => 'http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee',
			'page.party-planner.result.coffee-3-2.url-clean' => 'van-houtte-holiday-blend',

			'page.party-planner.result.coffee-3-3.title' => 'Timothy’s<sup>®</sup>',
			'page.party-planner.result.coffee-3-3.subtitle' => 'Christmas Blend',
			'page.party-planner.result.coffee-3-3.url' => 'http://www.keurig.ca/Beverages/Christmas-Blend/p/christmas-blend-timothys-coffee',
			'page.party-planner.result.coffee-3-3.url-clean' => 'timothys-christmas-blend',

			'page.party-planner.result.coffee-3-4.title' => 'Laura Secord<sup>®</sup>',
			'page.party-planner.result.coffee-3-4.subtitle' => 'Hot Chocolate Mix',
			'page.party-planner.result.coffee-3-4.url' => 'http://www.keurig.ca/Boissons/Chocolat-Chaud/M%C3%A9lange-%C3%A0-chocolat-chaud/p/hot-chocolate-mix-laura-secord-k-cup',
			'page.party-planner.result.coffee-3-4.url-clean' => 'vaura-secord-timothys-christmas-blend',

			'page.party-planner.result.coffee-3-5.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-3-5.subtitle' => 'Cappuccino - Specialty Collection',
			'page.party-planner.result.coffee-3-5.url' => 'http://www.keurig.ca/Beverages/Specialty/Cappuccino---Specialty-Collection-/p/specialty-cappuccino-coffee-van-houtte-k-cup',
			'page.party-planner.result.coffee-3-5.url-clean' => 'van-houtte-cappuccino-specialty-collection',


			// Drink Results KOLD (<5)
			'page.party-planner.result.coffee-4-1.title' => 'Seraphine<sup>TM</sup> Persian Lime',
			'page.party-planner.result.coffee-4-1.url' => 'http://www.keurig.ca/Kold-Beverages/Persian-Lime-Carbonated-Water/p/persian-lime-carbonated-water-seraphine-kold-pod',
			'page.party-planner.result.coffee-4-1.url-clean' => 'seraphine-persian-lime',

			'page.party-planner.result.coffee-4-2.title' => 'Red Barn<sup>TM</sup> Cane Cola',
			'page.party-planner.result.coffee-4-2.url' => 'http://www.keurig.ca/Kold-Beverages/Cane-Cola-Craft-Soda/p/cane-cola-craft-soda-red-barn-kold-pod',
			'page.party-planner.result.coffee-4-2.url-clean' => 'red-barn-cane-cola',

			// Drink Results KOLD (5-10)
			'page.party-planner.result.coffee-5-1.title' => 'Seraphine<sup>TM</sup> Passion Fruit',
			'page.party-planner.result.coffee-5-1.url' => 'http://www.keurig.ca/Kold-Beverages/Passion-Fruit-Carbonated-Water/p/passion-fruit-carbonated-water-seraphine-kold-pod',
			'page.party-planner.result.coffee-5-1.url-clean' => 'seraphine-passion-fruit',

			'page.party-planner.result.coffee-5-2.title' => 'Coca Cola<sup>®</sup>',
			'page.party-planner.result.coffee-5-2.url' => 'http://www.keurig.ca/Kold-Beverages/Coca-Cola%C2%AE/p/coca-cola-kold-pod',
			'page.party-planner.result.coffee-5-2.url-clean' => 'coca-cola',

			'page.party-planner.result.coffee-5-3.title' => 'Red Barn<sup>TM</sup> Cane Cola',
			'page.party-planner.result.coffee-5-3.url' => 'http://www.keurig.ca/Kold-Beverages/Cane-Cola-Craft-Soda/p/cane-cola-craft-soda-red-barn-kold-pod',
			'page.party-planner.result.coffee-5-3.url-clean' => 'red-barn-cane-cola',

			// Drink Results KOLD (10+)
			'page.party-planner.result.coffee-6-1.title' => 'Seraphine<sup>TM</sup> Persian Lime',
			'page.party-planner.result.coffee-6-1.url' => 'http://www.keurig.ca/Kold-Beverages/Persian-Lime-Carbonated-Water/p/persian-lime-carbonated-water-seraphine-kold-pod',
			'page.party-planner.result.coffee-6-1.url-clean' => 'seraphine-persian-lime',

			'page.party-planner.result.coffee-6-2.title' => 'Coca Cola<sup>®</sup>',
			'page.party-planner.result.coffee-6-2.url' => 'http://www.keurig.ca/Kold-Beverages/Coca-Cola%C2%AE/p/coca-cola-kold-pod',
			'page.party-planner.result.coffee-6-2.url-clean' => 'coca-cola',

			'page.party-planner.result.coffee-6-3.title' => 'Red Barn<sup>TM</sup> Cane Cola',
			'page.party-planner.result.coffee-6-3.url' => 'http://www.keurig.ca/Kold-Beverages/Cane-Cola-Craft-Soda/p/cane-cola-craft-soda-red-barn-kold-pod',
			'page.party-planner.result.coffee-6-3.url-clean' => 'red-barn-cane-cola',

			'page.party-planner.result.coffee-6-4.title' => 'Waterful<sup>TM</sup> Blissful berry',
			'page.party-planner.result.coffee-6-4.url' => 'http://www.keurig.ca/Kold-Beverages/Waterful%E2%84%A2-Blissful-Berry/p/blissful-berry-waterful-kold-pod',
			'page.party-planner.result.coffee-6-4.url-clean' => 'waterful-blissful-berry',


		// Accessory
			'page.party-planner.result.accessory-1.title' => 'White Ceramic Travel mug',
			'page.party-planner.result.accessory-1.subtitle' => '&nbsp;',
			'page.party-planner.result.accessory-1.url' => 'http://www.keurig.ca/Accessories/White-ceramic-travel-mug/p/holiday-travel-mug',
			'page.party-planner.result.accessory-1.url-clean' => 'white-ceramic-travel-mug',

			'page.party-planner.result.accessory-2.title' => 'Keurig<sup>TM</sup>',
			'page.party-planner.result.accessory-2.subtitle' => '2.0 Carousel',
			'page.party-planner.result.accessory-2.url' => 'http://www.keurig.ca/Accessories/Keurig%E2%84%A2-2-0-Carousel/p/keurig-2-0-carousel',
			'page.party-planner.result.accessory-2.url-clean' => 'keurig-2-0-carousel',

			'page.party-planner.result.accessory-3.title' => 'Keurig<sup>®</sup> Collage<sup>TM</sup>',
			'page.party-planner.result.accessory-3.subtitle' => 'Storage Nouveau White',
			'page.party-planner.result.accessory-3.url' => 'http://www.keurig.ca/Accessories/Keurig%C2%AE-Collage%E2%84%A2-Storage-Nouveau-White/p/keurig-collage-storage-unit-nouveau-white',
			'page.party-planner.result.accessory-3.url-clean' => 'keurig-collage-storage-nouveau-white',

			'page.party-planner.result.accessory-4.title' => 'Keurig<sup>TM</sup> 2.0',
			'page.party-planner.result.accessory-4.subtitle' => 'Thermal Carafe',
			'page.party-planner.result.accessory-4.url' => 'http://www.keurig.ca/Accessories/Keurig%E2%84%A2-2-0-Thermal-Carafe/p/Keurig-2-0-Thermal-Carafe',
			'page.party-planner.result.accessory-4.url-clean' => 'keurig-2-0-thermal-carafe',

			'page.party-planner.result.accessory-5.title' => 'Keurig<sup>®</sup> Kold<sup>TM</sup>',
			'page.party-planner.result.accessory-5.subtitle' => 'Glacier Glass Set',
			'page.party-planner.result.accessory-5.url' => 'http://www.keurig.ca/Kold-Accessories/KEURIG%C2%AE-KOLD%E2%84%A2-Glacier-Glass-Set%3A-Small/p/kold-glacier-glass-set-small',
			'page.party-planner.result.accessory-5.url-clean' => 'keurig-kold-glacier-glass-set',

			'page.party-planner.result.accessory-6.title' => 'Keurig<sup>®</sup> Kold<sup>TM</sup>',
			'page.party-planner.result.accessory-6.subtitle' => 'Wire Carousel Pod Storage',
			'page.party-planner.result.accessory-6.url' => 'http://www.keurig.ca/Kold-Accessories/KEURIG%C2%AE-KOLD%E2%84%A2-Wire-Carousel-Pod-Storage/p/wire-carousel-pod-storage-keurig-kold',
			'page.party-planner.result.accessory-6.url-clean' => 'keurig-kold-wire-carousel-pod-storage',

			'page.party-planner.result.accessory-7.title' => 'Keurig<sup>®</sup> Kold<sup>TM</sup>',
			'page.party-planner.result.accessory-7.subtitle' => 'Turnstyle Pod Storage',
			'page.party-planner.result.accessory-7.url' => 'http://www.keurig.ca/Kold-Accessories/KEURIG%C2%AE-KOLD%E2%84%A2-Turnstyle-Pod-Storage/p/turnstyle-pod-storage-keurig-kold',
			'page.party-planner.result.accessory-7.url-clean' => 'keurig-kold-turnstyle-pod-storage',


		// Message
		'page.party-planner.result.message-1' => 'Planning a heartfelt evening with your close ones? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket, a sumptuous risotto and the colourful and compact Keurig<sup>®</sup> K200 brewing system!',
		'page.party-planner.result.message-2' => 'Planning a whimsical evening with your close ones? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest, a sumptuous risotto and the colourful and compact Keurig<sup>®</sup> K200 brewing system!',
		'page.party-planner.result.message-3' => 'Planning a chic evening with your close ones? Keurig<sup>®</sup> has your back. Impress your guests with a stunning table, a sumptuous risotto and the colourful and compact Keurig<sup>®</sup> K200 Brewing System!',
		'page.party-planner.result.message-4' => 'Planning a heartfelt evening with your fun bunch? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket and a sumptuous risotto. When it’s time for dessert, the Keurig<sup>®</sup> K400 brewing system will help you brew a carafe for everyone!',
		'page.party-planner.result.message-5' => 'Planning a whimsical evening with your fun bunch? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest and a sumptuous risotto. When it’s time for dessert, the Keurig<sup>®</sup> K400 brewing system will help you brew a carafe for everyone!',
		'page.party-planner.result.message-6' => 'Planning a chic evening with family and friends? Keurig<sup>®</sup> has your back. Impress your guests with a stunning table and a sumptuous risotto. When it’s time for dessert, the Keurig<sup>®</sup> K400 brewing system will help you brew a carafe for everyone!',
		'page.party-planner.result.message-7' => 'Planning a heartfelt evening with your extended family? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket and a sumptuous risotto. When it’s time for dessert, the Keurig<sup>®</sup> K500 brewing system will help you brew carafes for everyone!',
		'page.party-planner.result.message-8' => 'Planning a whimsical evening with your extended family? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest and a sumptuous risotto. When it’s time for dessert, the Keurig<sup>®</sup> K500 brewing system will help you brew carafes for everyone!',
		'page.party-planner.result.message-9' => 'Planning a chic evening with your extended family? Keurig<sup>®</sup> has your back. Impress your guests with a stunning table and a sumptuous risotto. When it’s time for dessert, the Keurig<sup>®</sup> K500 brewing system will help you brew carafes for everyone!',
		'page.party-planner.result.message-10' => 'Planning a heartfelt brunch with your close ones? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket, perfect poached pears and the colourful and compact Keurig<sup>®</sup> K200 brewing system!',
		'page.party-planner.result.message-11' => 'Planning a whimsical brunch with your close ones? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest, perfect poached pears and the colourful and compact Keurig<sup>®</sup> K200 brewing system!',
		'page.party-planner.result.message-12' => 'Planning a chic brunch with your close ones? Keurig<sup>®</sup> has your back. Spice things up with a stunning table, perfect poached pears and the colourful and compact Keurig<sup>®</sup> K200 brewing system!',
		'page.party-planner.result.message-13' => 'Planning a heartfelt brunch with your fun bunch? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket and perfect poached pears. When it’s time for coffee, the Keurig<sup>®</sup> K400 brewing system will help you brew a carafe for everyone!',
		'page.party-planner.result.message-14' => 'Planning a whimsical brunch with your fun bunch? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest and perfect poached pears. When it’s time for coffee, the Keurig<sup>®</sup> K400 brewing System will help you brew a carafe for everyone!',
		'page.party-planner.result.message-15' => 'Planning a chic brunch with your family and friends? Keurig<sup>®</sup> has your back. Impress your guests with a stunning table and perfect poached pears. When it’s time for coffee, the Keurig<sup>®</sup> K400 brewing system will help you brew a carafe for everyone!',
		'page.party-planner.result.message-16' => 'Planning a heartfelt brunch with your extended family? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket and perfect poached pears. When it’s time for coffee, the Keurig<sup>®</sup> K500 brewing system will help you brew carafes for everyone!',
		'page.party-planner.result.message-17' => 'Planning a whimsical brunch with your extended family? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest and perfect poached pears. When it’s time for coffee, the Keurig<sup>®</sup> K500 brewing system will help you brew carafes for everyone!',
		'page.party-planner.result.message-18' => 'Planning a chic brunch with your extended family? Keurig<sup>®</sup> has your back. Impress your guests with a stunning table and perfect poached pears. When it’s time for coffee, the Keurig<sup>®</sup> K500 brewing system will help you brew carafes for everyone!',
		'page.party-planner.result.message-19' => 'Planning a lovely cocktail party? Keurig<sup>®</sup> has your back. Wow your guests with a personalized gift basket and the perfect cappuccino pudding. When it’s time for cocktails, the KOLD<sup>TM</sup> drinkmaker will help you make your favourite mixers!',
		'page.party-planner.result.message-20' => 'Planning a crazy cocktail party? Keurig<sup>®</sup> has your back. Spice things up with a kooky sweater contest and the perfect cappuccino pudding. When it’s time for cocktails, the KOLD<sup>TM</sup> drinkmaker will help you make your favourite mixers!',
		'page.party-planner.result.message-21' => 'Planning a chic cocktail party? Keurig<sup>®</sup> has your back. Impress your guests with a stunning table and the perfect cappuccino pudding. When it’s time for cocktails, the KOLD<sup>TM</sup> drinkmaker will help you make your favourite mixers!',


	// ******************************************************
	// Gift Guide
	'page.gift-guide.title' => 'Gift Guide',
	'page.gift-guide.url' => 'gift-guide',

		'page.gift-guide.subtitle' => 'Finding the perfect gifts for your loved ones has never been easier.  We\'ve curated the ideal bundles, fit for every budget and special someone on your list! Start sharing the joy with Keurig<sup>®</sup>.',

		// Generic
		'page.gift-guide.button.discover' => 'Discover',
		'page.gift-guide.button.buy' => 'Get this gift',
		'page.gift-guide.gift' => 'Gifts',

		// Category titles
		'page.gift-guide.categorytitle.50less' => '$50 or less',
		'page.gift-guide.categorytitle.50to150' => '$50 to $150',
		'page.gift-guide.categorytitle.50amdmore' => '$150 and more',

		// Package 1
		'page.gift-guide.package.pack1.title' => 'For the busy friend',
		'page.gift-guide.package.pack1.subtitle' => 'The perfect way to savour holiday flavours on the go. ',
		'page.gift-guide.package.pack1.price' => '$40',
		'page.gift-guide.package.pack1.content' => '
			<li>Limited Edition Winter Travel Mug</li>
			<li>1 box of Timothy’s<sup>®</sup> Christmas Blend K-Cup<sup>®</sup> pods</li>
			<li>1 box of Van Houtte<sup>®</sup> Holiday Blend K-Cup<sup>®</sup> pods</li>
		',
		'page.gift-guide.package.pack1.url' => 'http://www.keurig.ca/holiday/c/busy-bundle/c/busy101',
		'page.gift-guide.package.pack1.url-clean' => 'for-the-busy-friend',

		// Package 2
		'page.gift-guide.package.pack2.title' => 'For the sweet tooth',
		'page.gift-guide.package.pack2.subtitle' => 'Paired with these delectable K-Cup<sup>®</sup> pods, our carousel is the gift that keeps on giving.',
		'page.gift-guide.package.pack2.price' => '$41',
		'page.gift-guide.package.pack2.content' => '
			<li>Keurig<sup>®</sup> Neo<sup>TM</sup> Carousel</li>
			<li>1 Box of Apple Cider Mix K-Cup<sup>®</sup> pods</li>
		',
		'page.gift-guide.package.pack2.url' => 'http://www.keurig.ca/holiday/c/sweet-tooth/c/sweet101',
		'page.gift-guide.package.pack2.url-clean' => 'for-the-sweet-tooth',

		// Package 3
		'page.gift-guide.package.pack3.title' => 'For the winter aficionado',
		'page.gift-guide.package.pack3.subtitle' => 'Brew up a storm with this beautiful bundle.',
		'page.gift-guide.package.pack3.price' => '$188.92',
		'page.gift-guide.package.pack3.rebate' => '$129.99',
		'page.gift-guide.package.pack3.content' => '
			<li>Keurig<sup>®</sup> K200 Brewing System</li>
			<li>Keurig<sup>TM</sup> Carousel</li>
			<li>1 Box of Van Houtte<sup>®</sup> Colombian Coffee K-Cup<sup>®</sup> pods</li>
			<li>1 box of Timothy’s<sup>®</sup> Breakfast Blend K-Cup<sup>®</sup> pods</li>
		',
		'page.gift-guide.package.pack3.url' => 'http://www.keurig.ca/holiday/c/winter-aficionado/c/winter101',
		'page.gift-guide.package.pack3.legal' => 'Offer is available from December 1<sup>st</sup>, 2015 at 12:00 a.m. (EST) to December 25<sup>th</sup>, 2015 at 11:59 p.m. (EST), or until supplies last. Valid exclusively online at Keurig.ca. When one (1) K-Cup® Carousel, one (1) Keurig® K200 Brewing System, one (1) box of 24-count Timothy’s Breakfast Blend K-Cup® pods, and one (1) box of 24-count Van Houtte Colombian K-Cup® pods are added to cart, the cart total will automatically be reduced to the price of $129.99. The regular price of this bundle is $188.92. Maximum of five (5) of each brewing system can be added to each order. This promotion does not apply towards previously purchased merchandise. Offer subject to change or cancellation without prior notice. Shipping surcharges may be applied for locations outside of metropolitan areas. Cannot be combined with any other offer, coupon, or promotion.',
		'page.gift-guide.package.pack3.url-clean' => 'for-the-winter-aficionado',

		// Package 4
		'page.gift-guide.package.pack4.title' => 'For the holiday enthusiast',
		'page.gift-guide.package.pack4.subtitle' => 'A winning combination for the most wonderful time of the year!',
		'page.gift-guide.package.pack4.price' => '$88',
		'page.gift-guide.package.pack4.content' => '
			<li>1 box of Starbucks<sup>®</sup> Pike Place<sup>®</sup> Roast K-Cup<sup>®</sup> pods</li>
			<li>1 box of Folgers Gourmet Selections<sup>®</sup> Classic Roast<sup>®</sup> K-Cup<sup>®</sup> pods</li>
			<li>1 box of Barista Prima Coffeehouse<sup>®</sup> Italian Roast K-Cup<sup>®</sup> pods</li>
			<li>1 box of Timothy’s<sup>®</sup> Christmas Blend K-Cup<sup>®</sup> pods</li>
			<li>1 box of Van Houtte<sup>®</sup> Holiday Blend K-Cup<sup>®</sup> pods</li>
			<li>1 box of Laura Secord<sup>®</sup> Hot Chocolate Mix K-Cup<sup>®</sup> pods</li>
		',
		'page.gift-guide.package.pack4.url' => 'http://www.keurig.ca/holiday/c/holiday-enthusiast/c/enthusiast101',
		'page.gift-guide.package.pack4.legal' => 'Offer is available from December 1<sup>st</sup>, 2015 at 5:00 p.m. (EST) to December<sup>th</sup> 25th, 2015 at 11:59 p.m. (EST), or until supplies last. Valid exclusively online at Keurig.ca. When a minimum of 5 boxes of K-Cup®, K-Carafe<sup>TM</sup> or Rivo® pods are added to cart, only the 6th and least expensive box will be discounted at its full price. Kold™ pods are excluded from this promotion. Promo code HOLIDAYPROMO must be applied at checkout. Only one (1) promo code can be used per order. This promotion does not apply towards previously purchased merchandise. Offer subject to change or cancellation without prior notice. Shipping surcharges may be applied for locations outside of metropolitan areas. Cannot be combined with any other offer, coupon, or promotion.',
		'page.gift-guide.package.pack4.url-clean' => 'for-the-holiday-enthusiast',

		// Package 5
		'page.gift-guide.package.pack5.title' => 'For the homemaker extraordinaire',
		'page.gift-guide.package.pack5.subtitle' => 'This great gift basket is all about gratitude. ',
		'page.gift-guide.package.pack5.price' => '$ 197',
		'page.gift-guide.package.pack5.content' => '
			<li>Keurig<sup>®</sup> K400 brewing system</li>
			<li>1 All Time Favourites Variety Box K-Cup<sup>®</sup> pods</li>
			<li>Limited Edition Winter Travel Mug</li>
		',
		'page.gift-guide.package.pack5.url' => 'http://www.keurig.ca/holiday/c/homemaker-extraordinaire/c/homemaker101',
		'page.gift-guide.package.pack5.url-clean' => 'for-the-homemaker-extraordinaire',

		// Package 6
		'page.gift-guide.package.pack6.title' => 'For the early adopter',
		'page.gift-guide.package.pack6.subtitle' => 'This cool thirst-aid station is the slickest present ever.',
		'page.gift-guide.package.pack6.price' => '$ 431',
		'page.gift-guide.package.pack6.content' => '
			<li>Keurig<sup>®</sup> KOLD<sup>TM</sup> drinkmaker</li>
			<li>Keurig<sup>®</sup> KOLD<sup>TM</sup> Glacier Glass Set </li>
			<li>1 box of Flynn\'s<sup>TM</sup> Soda Shop Root Beer KOLD<sup>TM</sup> pods</li>
			<li>1 box of Coca-Cola<sup>®</sup> KOLD<sup>TM</sup> pods </li>
		',
		'page.gift-guide.package.pack6.url' => 'http://www.keurig.ca/holiday/c/early-adopter/c/early101',
		'page.gift-guide.package.pack6.url-clean' => 'for-the-early-adopter',




	// ******************************************************
	// Flavours of the season
	'page.flavours-season.title' => 'Flavours of the season',
	'page.flavours-season.url' => 'holiday-flavours',
	
		'page.flavours-season.subtitle' => 'Start brewing some holiday cheer! For a limited time, discover our selection of holiday blends and fill your cup with good times.',

		// Generic
		'page.flavours-season.button.readrecipe' => 'Read recipe',
		'page.flavours-season.button.buy-online' => 'Shop online',
		'page.flavours-season.button.buy' => 'Shop',
			
		// Featured
		'page.flavours-season.featured1.title' => 'Van Houtte<sup>®</sup> Holiday Blend',
		'page.flavours-season.featured1.content' => 'This is the new holiday must! A rich and flavourful blend, with woodsy notes and subtle acidity. Holiday Blend will brighten your frosty mornings! Available both online and in <a href="http://www.keurig.ca/find-a-store" target="_blank" title="stores" class="tracking-ga" data-url-ga="stores">stores</a>.',
		'page.flavours-season.featured1.shop.url' => 'http://www.keurig.ca/Beverages/Holiday-Blend/p/holiday-blend-van-houtte-coffee',
		'page.flavours-season.featured1.article.url' => 'gingerbread-spice-latte',
		'page.flavours-season.featured1.article.url-clean' => 'van-houtte-holiday-blend',

		'page.flavours-season.featured2.title' => 'Timothy’s<sup>®</sup> Christmas Blend',
		'page.flavours-season.featured2.content' => 'Timothy’s<sup>®</sup> Christmas Blend is here, just in time for the holidays! Classical blend of the finest beans, this medium roast coffee has plenty of spirit. You’ll enjoy its rich, smooth, satisfying taste in every cup. Available both online and in <a href="http://www.keurig.ca/find-a-store" target="_blank" title="stores" class="tracking-ga" data-url-ga="stores">stores</a>.  ',
		'page.flavours-season.featured2.shop.url' => 'http://www.keurig.ca/Beverages/Christmas-Blend/p/christmas-blend-timothys-coffee',
		'page.flavours-season.featured2.article.url' => 'candy-cane-latte',
		'page.flavours-season.featured2.article.url-clean' => 'timothys-christmas-blend',

		// Suggestions
		'page.flavours-season.sugg.title' => 'Best K-Cup<sup>®</sup> pods of 2015',
		'page.flavours-season.sugg.subtitle' => 'To top it all off, we’ve compiled this year’s most-beloved K-Cup<sup>®</sup> pods. Enjoy!',

		'page.flavours-season.sugg1.img-width' => '169',
		'page.flavours-season.sugg1.img-height' => '168',
		'page.flavours-season.sugg1.title' => 'Van Houtte<sup>®</sup>',
		'page.flavours-season.sugg1.content' => 'Colombian',
		'page.flavours-season.sugg1.url' => 'http://www.keurig.ca/Beverages/Coffee/Colombian-Coffee/p/colombian-medium-coffee-VH-k-cup',
		'page.flavours-season.sugg1.url-clean' => 'van-houtte-colombian',

		'page.flavours-season.sugg2.img-width' => '169',
		'page.flavours-season.sugg2.img-height' => '168',
		'page.flavours-season.sugg2.title' => 'Barista Prima Coffeehouse<sup>®</sup>',
		'page.flavours-season.sugg2.content' => 'Italian Roast',
		'page.flavours-season.sugg2.url' => 'http://www.keurig.ca/Beverages/Coffee/Italian-Roast-Coffee/p/italian-roast-coffee-barista-prima-k-cup',
		'page.flavours-season.sugg2.url-clean' => 'barista-prima-coffeehouse-italian-roast',

		'page.flavours-season.sugg3.img-width' => '173',
		'page.flavours-season.sugg3.img-height' => '173',
		'page.flavours-season.sugg3.title' => 'Timothy’s<sup>®</sup>',
		'page.flavours-season.sugg3.content' => 'Breakfast Blend',
		'page.flavours-season.sugg3.url' => 'http://www.keurig.ca/Beverages/Coffee/Breakfast-Blend-Coffee/p/english-breakfast-coffee-timothys-k-cup',
		'page.flavours-season.sugg3.url-clean' => 'timothys-breakfast-blend',

		'page.flavours-season.sugg4.img-width' => '172',
		'page.flavours-season.sugg4.img-height' => '172',
		'page.flavours-season.sugg4.title' => 'Donut House Collection<sup>®</sup>',
		'page.flavours-season.sugg4.content' => 'Donut House® Coffee',
		'page.flavours-season.sugg4.url' => 'http://www.keurig.ca/Beverages/Coffee/Donut-House%C2%AE-Coffee/p/donut-house-coffee-DHC-k-cup',
		'page.flavours-season.sugg4.url-clean' => 'donut-house-collection-coffee',

		'page.flavours-season.sugg5.img-width' => '167',
		'page.flavours-season.sugg5.img-height' => '167',
		'page.flavours-season.sugg5.title' => 'Van Houtte<sup>®</sup>',
		'page.flavours-season.sugg5.content' => 'Original House Blend',
		'page.flavours-season.sugg5.url' => 'http://www.keurig.ca/Beverages/Coffee/Original-House-Blend-Coffee/p/original-house-blend-coffee-VH-k-cup',
		'page.flavours-season.sugg5.url-clean' => 'van-houtte-original-house-blend',

		'page.flavours-season.sugg6.img-width' => '167',
		'page.flavours-season.sugg6.img-height' => '167',
		'page.flavours-season.sugg6.title' => 'Green Mountain Coffee<sup>®</sup>',
		'page.flavours-season.sugg6.content' => 'Dark Magic<sup>®</sup>',
		'page.flavours-season.sugg6.url' => 'http://www.keurig.ca/Beverages/Coffee/Dark-Magic%C2%AE-Extra-Bold-Coffee/p/dark-magic-coffee-GMC-k-cup',
		'page.flavours-season.sugg6.url-clean' => 'green-mountain-coffee-dark-magic',

		'page.flavours-season.sugg7.img-width' => '167',
		'page.flavours-season.sugg7.img-height' => '167',
		'page.flavours-season.sugg7.title' => 'Van Houtte<sup>®</sup>',
		'page.flavours-season.sugg7.content' => 'Vanilla Hazelnut',
		'page.flavours-season.sugg7.url' => 'http://www.keurig.ca/Beverages/Coffee/Vanilla-Hazelnut-Coffee/p/vanilla-hazelnut-coffee-VH-k-cup',
		'page.flavours-season.sugg7.url-clean' => 'green-mountain-coffee-vanilla-hazelnut',

		'page.flavours-season.sugg8.img-width' => '169',
		'page.flavours-season.sugg8.img-height' => '169',
		'page.flavours-season.sugg8.title' => 'Laura Secord<sup>®</sup>',
		'page.flavours-season.sugg8.content' => 'Hot Chocolate Mix',
		'page.flavours-season.sugg8.url' => 'http://www.keurig.ca/Beverages/Hot-Cocoa/Hot-Chocolate-Mix/p/hot-chocolate-mix-laura-secord-k-cup',
		'page.flavours-season.sugg8.url-clean' => 'laura-secord-hot-chocolate-mix',

		'page.flavours-season.sugg9.img-width' => '167',
		'page.flavours-season.sugg9.img-height' => '167',
		'page.flavours-season.sugg9.title' => 'Starbucks<sup>®</sup>',
		'page.flavours-season.sugg9.content' => 'Pike Place<sup>®</sup> Roast',
		'page.flavours-season.sugg9.url' => 'http://www.keurig.ca/Beverages/Coffee/Pike-Place%C2%AE-Roast-Coffee/p/pike-place-roast-coffee-starbucks-k-cup',
		'page.flavours-season.sugg9.url-clean' => 'starbucks-pike-place-roast',

		'page.flavours-season.sugg10.img-width' => '172',
		'page.flavours-season.sugg10.img-height' => '172',
		'page.flavours-season.sugg10.title' => 'The Original Donut Shop<sup>TM</sup>',
		'page.flavours-season.sugg10.content' => 'The Original Donut Shop<sup>®</sup> Regular',
		'page.flavours-season.sugg10.url' => 'http://www.keurig.ca/Beverages/Coffee/The-Original-Donut-Shop%E2%84%A2-Coffee-Regular/p/original-donut-shop-coffee-ODS-k-cup',
		'page.flavours-season.sugg10.url-clean' => 'the-original-donut-shop-regular',


	// ******************************************************
	// Buttons
	'btn.close' => 'Back to Holiday Planner',
	'btn.back' => 'Back',


	// ******************************************************
	// Shares
	'share.title' => 'Share the tips',
	'share.email.title' => 'Spread the Holiday Magic!',
	'share.email.message' => 'I%20was%20browsing%20through%20the%20Keurig%C2%AE%20Holiday%20Planner%20and%20this%20cool%20article%20made%20me%20think%20of%20you.%20%0A%0A',
	'share.email.message-end' => '%0A%0AHappy%20Holidays!%0A%0A',


	// ******************************************************
	// Footer
	'footer.facebook-share' => 'Spread the magic',
	'footer.language' => 'Français',
	'footer.language.abr' => 'FR',
	'footer.go-to-keurig' => 'Go to Keurig.ca',
	'footer.go-to-keurig.url' => 'http://www.keurig.ca/',

];

