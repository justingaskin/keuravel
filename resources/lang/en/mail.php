<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the email tamplate page. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	'language' => 'en',
	'altLocale' => 'fr',
	'subject' => 'Important - Last Steps to get Your Promo Code',
	'banner.alt' => 'Last Steps to get Your Promo Code',
	'link.shop1' => 'Shop<br>Brewers',
	'link.shop2' => 'Shop<br>K-CUP<sup>®</sup> PODS',
	'question' => 'Got a question?',
	'print' => 'Print out the completed form.',
	'infos' => 'Your request will be processed within the next 2 weeks and we’ll send you a promo code by email.',
	'contact' => 'Contact one of our dedicated customer service representative at',
	'legal1.line1' => '$35 offer<br>Offer is available from October 1st, 2015 at 9:00 a.m. (EDT) to November 15th, 2015 at 5:00 p.m. (EDT), or until supplies last. Valid exclusively online at Keurig.ca.',
	'legal1.line2' => 'Coupon is valid for a total of two (2) free boxes of K-Cup<sup>®</sup> pods, as follows: one (1) 30-count All-Time Favourites Variety Box (value of $21.49) and your choice of any one (1) box of K-Cup<sup>®</sup> pods (minimum value of $16.99). Free shipping is included. Only one (1) coupon code can be used per order. Unique discount code must be applied at checkout. This promotion does not apply towards previously purchased merchandise. Offer subject to change or cancellation without prior notice. Shipping surcharges may be applied for locations outside of metropolitan areas. Cannot be combined with any other offer, coupon, or promotion.',
	'legal2.line1' => '$70 offer<br>Offer is available from October 1st, 2015 at 9:00 a.m. (EDT) to November 15th, 2015 at 5:00 p.m. (EDT), or until supplies last. Valid exclusively online at Keurig.ca.',
	'legal2.line2' => 'Coupon is valid for a total of four (4) free boxes of K-Cup<sup>®</sup> pods, as follows: one (1) free 30-count All-Time Favourites Variety Box (value of $21.49) and your choice of any three (3) boxes of K-Cup<sup>®</sup> pods (minimum value of $50.97). Free shipping is included. Only one (1) coupon code can be used per order. Unique discount code must be applied at checkout. This promotion does not apply towards previously purchased merchandise. Offer subject to change or cancellation without prior notice. Shipping surcharges may be applied for locations outside of metropolitan areas. Cannot be combined with any other offer, coupon, or promotion.',



];
