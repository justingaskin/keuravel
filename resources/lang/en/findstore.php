<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the Find a store page. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	'brewing' => 'K-Cup® Brewing Systems',
	'infos' => 'You can find Keurig<sup>®</sup> Brewing Systems, portion packs and accessories at thousands of retail locations across the Canada. Please check with the stores in your local area to learn about the specific brewer models and varieties that they carry.',
	'nationalstores' => 'National and local retail stores',
	'link.ares' => 'https://www.arescuisine.com/en/',
	'link.aviva' => 'http://www.aviva.ca/',
	'link.batteries' => 'http://www.batteriesincluded.ca/',
	'link.bedbath' => 'http://www.bedbathandbeyond.ca/',
	'link.staples' => 'http://www.staples.ca/en/',
	'link.canadiantire' => 'http://www.canadiantire.ca/en.html',
	'link.centrerasoir' => 'http://www.centredurasoir.com/en/accueil.php',
	'link.corbeil' => 'https://www.corbeilelectro.com/en',
	'link.costco' => 'http://www.costco.ca/?storeId=10302&catalogId=11201&langId=-24',
	'link.despres' => 'http://www.despreslaporte.com/1-1-home.html?DIVISIONID=1',
	'link.futureshop' => 'http://www.futureshop.ca/en-ca/home.aspx?path=f9af28b84da40a62feed69e743160ef6en99',
	'link.homeoutfitters' => 'http://www.homeoutfitters.com/en/index.html',
	'link.hudson' => 'http://www.thebay.com/',
	'link.lasoupiere' => 'http://lasoupiere.com/en/home/',
	'link.korvette' => 'http://www.korvette.ca/default.asp?lan=en',
	'link.linenchest' => 'http://www.linenchest.com/',
	'link.londondrugs' => 'http://www.londondrugs.com/',
	'link.personaledge' => 'http://www.personaledge.com/on/accueil.php',
	'link.rona' => 'http://www.rona.ca/en',
	'link.sears' => 'http://www.sears.ca/homepage',
	'link.stokes' => 'http://www.stokesstores.com/en/',
	'link.vinpassion' => 'https://vinetpassion.com/en/',
	'link.walmart' => 'http://www.walmart.ca/en/'
];

