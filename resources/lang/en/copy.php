<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the global page. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	'language' => 'en',
	'altLocale' => 'fr',
	'title' => 'Your Keurig® Gift',
//HOMEPAGE
	//HEADER
	'header.menu.link1' => 'How it works',
	'header.menu.link2' => 'Help',
	'header.menu.link3' => 'Legal',
	'header.menu.link4' => 'Visit Keurig.ca',
	'header.purchaseinstore' => 'Purchased in store?',
	//BANNER
	'banner.left.title' => '<span>BUY</span> A BREWER',
	'banner.left.text' => 'And get up to<br><span><sup>$</sup>70</span> worth of K-Cup<sup>®</sup> pods<sup>*</sup>',
	'banner.left.btn' => 'LEARN MORE',

	'banner.right.title' => '<span>REDEEM</span> YOUR OFFER',
	'banner.right.text' => 'Purchased a brewer between October 1<sup>st</sup> and November 15<sup>th</sup>, 2015?',
	'banner.right.btn' => 'GET YOUR PODS',

	'banner.copy' => "*Certain conditions apply. Only on select brewing system. Offer ends on November 15<sup>th</sup>, 2015.",
	'banner.dogear' => "<strong>24h</strong>left to buy a brewer in stores or online|<strong>Only :days days </strong>remaining to buy a brewer in stores or online",
	'banner.dogear.end' => "<strong>This offer has now ended.</strong>",
	//STEP
	'step' => "Step",
	'step1.text' => "Discover the right offer for you",
	'step2.text' => "Choose your path to free K-Cup<sup>®</sup> pods",
	//OFFER
	'offer.btn' => "Buy",
	'offer.link' => "Buy in store",
	//OFFER1
	'offer1.title' => "A sweet deal for savvy drinkers",
	'offer1.info' => "Get<strong><sup>$</sup>35</strong><span>worth of K-Cup<sup>®</sup> pods</span>",
	'offer1.text' => "with the purchase of a Keurig<sup>®</sup><br>K200 or K300 brewing system.",
	'offer1.desc1' => "Colourful and Compact",
	'offer1.desc2' => "Plenty of possibilities",
	//OFFER2
	'offer2.title' => "A serious offer for coffee lovers",
	'offer2.info' => "Get<strong><sup>$</sup>70</strong><span>worth of K-Cup<sup>®</sup> pods</span>",
	'offer2.text' => "with the purchase of a Keurig<sup>®</sup><br>K400 or K500 brewing system.",
	'offer2.desc1' => "Style with more features ",
	'offer2.desc2' => "The cream of the crop",
	//COMPARE
	'compare' => "COMPARE BREWERS",
	//JOURNEY1
	'journey1.yellow' => "Love to<br>shop online?",
	'journey1.block1' => "Purchase a select brewing system at Keurig.ca",
	'journey1.block2' => "Receive your promo code by email",
	'journey1.block3' => "Use your promo code to order your free Variety Box and free K-Cup<sup>®</sup> pods at Keurig.ca",
	'journey.ship' => "EVERYTHING SHIPS FOR FREE!",
	//JOURNEY2
	'journey2.yellow' => "More of a store shopper?",
	'journey2.block1' => "Purchase a brewing system from one of these <a href=':url'>retailers</a>",
	'journey2.block2' => "Redeem your offer <a href=':url?caribou-2.0=step1-form'>here</a> before 26/02/16",
	'journey2.block3' => "Receive your promo code by email",
	'journey2.block4' => "Use your promo code to order your free Variety Box and free K-Cup<sup>®</sup> pods at Keurig.ca",
//PAGE FORM
	//BANNER
	'formbanner.title1' => "Purchased in store with this promo?",
	'formbanner.title2' => "GET YOUR FREE K-CUP<sup>®</sup> PODS! ",
	//JOURNEY
	'form.journey.block1' => "Purchase a brewer from a retailer between October 1<sup>st</sup> and November 15<sup>th</sup>, 2015",
	'form.journey.block2' => "Fill out the form below",
	'form.journey.block3' => "Mail the completed form, a copy of your receipt and your bar code before February 26<sup>th</sup>, 2016",
	'form.journey.block4' => "Receive your promo code by email",
	'form.journey.block5' => "Use your promo code at Keurig.ca",
	'form.journey.ship' => "Your order ships for FREE",
	//FORMULAIRE
	'form.title' => "Fill out this form<br>to get your promo code",
	'form.error' => 'An error occured. Check the form for invalid fields',
	'form.choose' => 'Choose...',
	'form.required' => "Required fields",
	'form.field.firstname' => "First name",
	'form.field.lastname' => "Last name",
	'form.field.email' => "Email",
	'form.field.address1' => "Address line 1",
	'form.field.address2' => "Address line 2",
	'form.field.city' => "City",
	'form.field.province' => "Province",
	'form.field.postalcode' => "Postal Code",
	'form.field.phone' => "Phone number",
	'form.field.model' => "Model Purchased",
		'form.field.model.opt1' => "Keurig® 2.0 K200 Brewing System",
		'form.field.model.opt2' => "Keurig® 2.0 K300 Brewing System",
		'form.field.model.opt3' => "Keurig® 2.0 K400 Brewing System",
		'form.field.model.opt4' => "Keurig® 2.0 K500 Brewing System",
	'form.field.date' => "Purchase date",
	'form.field.dateformat' => '(dd/mm/yyyy)',
	'form.field.from' => "Purchased from",
	'form.hudsonsbay' => "HUDSON’S BAY",
	'form.other' => "OTHER",
	'form.field.specify' => "Specify",
	'form.submit' => "Submit",
	'form.legal.offer' => "This offer is only applicable on brewers purchased between October 1<sup>st</sup>, 2015 and November 15<sup>th</sup>, 2015.",
	'form.checkbox1' => "I authorize Keurig<sup>®</sup> to contact me by phone, email and mail to offer me additional promotions and benefits.",
	'form.checkbox2' => "I agree on all <a target='_blank' href=':url'>terms & conditions</a> of this offer",
	//BLOCKS
	'form.block1.text' => "You have questions, we have answers.",
	'form.block1.link' => 'http://www.keurig.ca/welcome-offer',
	'form.blocks.learnmore' => "Learn more",
	'form.block2.title' => "Want to get in touch with us?",
	'form.block2.text' => "Reach out to a customer support representative.",
	'form.block2.link' => 'tel:+1-800-361-5628',
	'form.blocks.phone' => "1-800-361-5628",
//PAGE THANKS
	//CONTENT
	'thanks.h1' => "You’re one step away from getting your promo code",
	'thanks.receive' => "You will now receive the completed form by email.",
	'thanks.print' => "Print out the completed form.",
	'thanks.cut' => "Cut out the bar code on your brewer’s box. ",
	'thanks.copy' => "Make a copy of your receipt.",
	'thanks.send' => "Send everything by mail along to",
	'thanks.address' => "C.P 2010, Longueuil, Québec, J4K 5J9 before February 26<sup>th</sup>, 2016",
	'thanks.infos' => "We will process your request within 2 weeks and will get back to you with a valid promo code at Keurig.ca through email, if you meet the eligibility criteria.",
	'thanks.value' => 'Depending on the brewing system you purchased, a promo code for a $35 or $70 worth of K-Cup® pods will be sent your way.',
	'thanks.box35' => 'The $35 promo code entitles you to one free Variety Box and one free box of 24 K-Cup® pods of your choice.',
	'thanks.box75' => 'The $70 promo code entitles you to one free Variety Box and three free boxes of 24 K-Cup® pods of your choice.',
	//POPUP
	'popup.print' => "Print your completed form",
	'popup.cut' => "Cut-out the bar code on the box",
	'popup.copy' => "Make a copy of your receipt",
	'popup.send' => "Mail everything at C.P 2010, Longueuil, Québec, J4K 5J9",
	'popup.infos' => "Your request will be processed within the next 2 weeks and we’ll send you a promo code by email",
	//FOOTER
	'footer.help' => "Help",
	'footer.contact' => "Contact us",
	'footer.customer' => "Customer Service 1 800 361 5628",
	'footer.customer2' => "Customer Service <a href='tel:+18003615628'>1 800 361 5628</a>",
	'footer.email' => "Email form",
	'footer.terms' => "Terms & Conditions",
	'footer.copy' => "Copyright Keurig Canada Inc. 2015 All Rights Reserved",
	'footer.privacy' => "Keurig.ca's privacy Policy",
	'footer.privacy.link' => "http://www.keurig.ca/content/privacy-policy",

	// LINKS
	'link.keurig' => "http://www.keurig.ca",
	'link.mail' => "http://www.keurig.ca/support/email-us-page",
	'link.legal' => "http://www.keurig.ca/terms-conditions",
	'link.retailer' => "http://www.keurig.ca/find-a-store",
	'link.from' => '/fr/deja-achete',
	'province' => [
		'BC' => 'British Columbia',
		'NB' => 'New Brunswick',
		'NL' => 'Newfoundland and Labrador',
		'NS' => 'Nova Scotia',
		'PE' => 'Prince Edward Island',
		'QC' => 'Quebec',
		'NT' => 'Northwest Territories',
	],

	'link.buy.k200' => 'http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Orange-Zest/p/Keurig-2-0-k200-brewer-orange',
	'link.buy.k300' => 'http://www.keurig.ca/Brewers/Home/Keurig%C2%AE-2-0-K300-Brewing-System/p/Keurig-2-0-K300-Brewing-System',
	'link.buy.k400' => 'http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K400-Brewing-System-Vintage-Red/p/Keurig-2-0-K400-Brewing-System-Vintage-Red',
	'link.buy.k500' => 'http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K500-Brewing-System/p/Keurig-2-0-K500-Brewing-System',
// PAGE PROMO END
	'end.title' => "This promotion has now ended",
	'end.text' => "If you purchased a Keurig<sup>®</sup> brewer in store between October 1<sup>st</sup> and November 15<sup>th</sup>, 2015, please
	<a href=':url'>fill out the form</a> and follow the steps to receive your promo code.
	<br />
	<br />Visit <a href='http://www.keurig.ca'>Keurig.ca</a> and susbcribe to the newsletter to discover our current promotions.",
	'end.link-text' => "Shop now",
	'end.link' => "http://www.keurig.ca",
];
