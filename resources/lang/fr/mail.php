<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the email tamplate page. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	'language' => 'fr',
	'altLocale' => 'en',
	'subject' => 'Important - Dernières étapes pour obtenir votre code coupon',
	'banner.alt' => 'Dernières étapes pour obtenir votre code coupon',
	'link.shop1' => "Magasiner<br>les systèmes<br>d'infusion",
	'link.shop2' => 'Magasiner<br>les capsules<br>K-Cup<sup>®</sup>',
	'question' => 'Vous avez une question?',
	'print' => 'Imprimez le formulaire complété.',
	'infos' => 'Nous traiterons votre demande dans un délai de 2 semaines et vous enverrons un code promotionnel par courriel.',
	'contact' => 'Contactez l’un de nos représentants au service à la clientèle au',
	'legal1.line1' => 'Offre 35 $<br>L’offre est disponible du 1er octobre 2015 à 9 heures (HAE) au 15 novembre 2015 à 17 heures (HAE), ou jusqu’à épuisement des stocks. Valable exclusivement en ligne à Keurig.ca.',
	'legal1.line2' => 'Le coupon est valide pour un total de deux (2) boîtes gratuites de capsules K-Cup® comme suit : une (1) boîte Variée vos cafés favoris de 30 capsules (valeur de 21,49 $) et votre choix d’une (1) boîte de capsules K-Cup® (valeur minimum de 16,99 $). La livraison gratuite est incluse. Un (1) seul code promo peut être utilisé par achat. Le code promo unique doit être appliqué à l’achat. Cette offre ne s’applique pas aux achats antérieurs. L’offre peut être modifiée ou annulée sans préavis. Des frais d’expédition supplémentaires peuvent s’appliquer aux emplacements situés à l’extérieur des zones métropolitaines. Ne peut être combinée avec d’autres offres, codes coupons ou promotions.',
	'legal2.line1' => 'Offre 70 $<br>L’offre est disponible du 1er octobre 2015 à 9 heures (HAE) au 15 novembre 2015 à 17 heures (HAE), ou jusqu’à épuisement des stocks. Valide exclusivement en ligne à Keurig.ca.',
	'legal2.line2' => 'Le coupon est valide pour un total de quatre (4) boîtes gratuites de 30 capsules K-Cup® comme suit : une (1) boîte Variée vos cafés favoris de 30 capsules (valeur de 21,49 $) et votre choix de trois (3) boîtes de capsules K-Cup® (valeur minimum de 50,97 $). La livraison gratuite est incluse. Un seul (1) code promo peut être utilisé par achat. Le code promo unique doit être appliqué à l’achat. Cette offre ne s’applique pas aux achats antérieurs. L’offre peut être modifiée ou annulée sans préavis. Des frais d’expédition supplémentaires peuvent s’appliquer aux emplacements situés à l’extérieur des zones métropolitaines. Ne peut être combinée avec d’autres offres, codes coupons ou promotions.',



];
