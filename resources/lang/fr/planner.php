<?php return
[
	'site.url' => 'guide-des-fetes',

	// ******************************************************
	// Home
	'home.title' => 'Guide des fêtes',
	'home.welcome' => 'Infusez de la magie des Fêtes avec Keurig<sup>®</sup> <br /> et profitez de chaque petit moment.',
	'home.welcome.meta' => 'Infusez de la magie des Fêtes avec Keurig® et profitez de chaque petit moment.',

	'home.loading' => 'Chargement en cours',


	// ******************************************************
	// Holidays Tips
	'page.holiday-tips.title' => 'Astuces des fêtes',
	'page.holiday-tips.url' => 'conseils-recettes',

		// Generic
		'page.holiday-tips.subtitle' => 'Notre assortiment de trucs et d’astuces des Fêtes vous permettra d’apprécier chaque minute de la saison. Des recettes des plus décadentes aux activités amusantes et festives, Keurig<sup>®</sup> vous offre le meilleur des Fêtes.',
		'page.holiday-tips.quicktips.title' => 'Astuce des fêtes',
		'page.holiday-tips.ingredients.title' => 'Ingrédients',
		'page.holiday-tips.preparation.title' => 'Préparation',
		'page.holiday-tips.tip.title' => 'Astuce',

		// Quick tips
		'page.holiday-tips.quicktips1.content' => 'Pensez aux amateurs de thé! Faites le plein de <a href="http://www.keurig.ca/beverages/tea/c/tea101" target="_blank" title="capsules Keurig K-Cup® variées" class="tracking-ga" data-url-ga="keurig-k-cup-pods">capsules Keurig K-Cup<sup>®</sup> variées</a>.',
		'page.holiday-tips.quicktips2.content' => 'Gardez les enfants occupés en organisant une compétition de bonhommes de neige! Pensez au <a href="http://www.keurig.ca/recherche/?text=+chocolat+chaud" target="_blank" title="chocolat chaud" class="tracking-ga" data-url-ga="hot-chocolate">chocolat chaud</a>.',
		'page.holiday-tips.quicktips3.content' => 'Achetez votre dinde plus tôt que tard!',
		'page.holiday-tips.quicktips4.content' => 'Installez un espace photo des Fêtes pour capturer tous ces moments inoubliables!',
		'page.holiday-tips.quicktips5.content' => '<a href="http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee" target="_blank" title="K-Cup® Keurig® mélange des Fêtes" class="tracking-ga" data-url-ga="keurig-holiday-blends-k-cup-pods">K-Cup® Keurig<sup>®</sup> mélange des Fêtes</a> pour célébrer la saison avec vos proches!',
		'page.holiday-tips.quicktips6.content' => 'Les biscuits au pain d’épice font plaisir à tout le monde!',
		'page.holiday-tips.quicktips7.content' => 'Conviez vos invités à un échange de cadeaux coloré!',


		// Article - Tips for Hosting an Ugly Sweater Party
		'page.holiday-tips.article.sweater-party.title' => 'Organisez une fête de chandails de Noël rigolos',

		'page.holiday-tips.article.sweater-party.intro-1' => 'Les fêtes de chandails rigolos sont amusantes pour tout le monde, particulièrement ceux qui ont un bon sens de l’humour! Vous n’avez pas à casser la tirelire pour le trouver : la popularité croissante de ces soirées a encouragé la vente de nouveaux « chandails rigolos » pour l’occasion! Pressé? Jetez un œil dans le garde-robe de grand-maman! Vous pourriez trouver le parfait col roulé de rennes au nez clignotant!',
		'page.holiday-tips.article.sweater-party.intro-2' => 'Les invitations sont envoyées.. .et maintenant? Nous avons quelques idées.',

		'page.holiday-tips.article.sweater-party.subtitle-1' => 'Installez un espace photo',
		'page.holiday-tips.article.sweater-party.content-1' => 'Capturez tous les ensembles hilarants avec un espace photo. Cela peut être aussi simple qu’avec un bâton pour égo-portraits, du papier d’emballage cadeau et quelques accessoires. Si vous vous sentez bricoleur, plein d’idées folles vous attendent sur Pinterest. À la fin de la nuit, réunissez toutes vos meilleures photos et partagez-les!',

		'page.holiday-tips.article.sweater-party.subtitle-2' => 'Votez pour le meilleur!',
		'page.holiday-tips.article.sweater-party.content-2-1' => 'Le meilleur dans une fête de chandails rigolos est le moment où un nouvel invité arrive. Chaque fête a ses vedettes, ces individus qui se distinguent par leur originalité et créativité, et qui méritent votre reconnaissance. Une fois que tout le monde est arrivé, distribuez des cartes de vote pour les meilleurs costumes. Une fois que les votes sont comptés, réunissez tout le monde et décernez des prix aux gagnants!',
		'page.holiday-tips.article.sweater-party.content-2-2' => 'Voici quelques idées pour vos cartes de vote :',
		'page.holiday-tips.article.sweater-party.content-2-3' => '
			<li>Meilleur bricolage</li>
			<li>Chandail le plus rigolo</li>
			<li>Chandail le plus honteux</li>
			<li>Ensemble le plus original</li>
		',

		'page.holiday-tips.article.sweater-party.subtitle-3' => 'Servez de la nourriture digne de votre événement',
		'page.holiday-tips.article.sweater-party.content-3-1' => 'Profitez de l’aspect des fêtes pour servir un gâteau aux fruits! Pour les gâteries sucrées, organisez une station de décoration pour les gâteaux au pain d’épice, ou un endroit où glacer de délicieux petits gâteaux. Décorez un gâteau avec un design de chandail rigolo ou demandez à une pâtisserie locale de vous aider à créer quelque chose d’unique pour l’occasion!',
		'page.holiday-tips.article.sweater-party.content-3-2' => '<strong>Bonus :</strong> Les fêtes de chandails rigolos sont idéales pour jouer à des jeux loufoques. C’est le moment de jouer à tous les jeux délirants que vous avez vus en ligne!',

		'page.holiday-tips.article.sweater-party.urlID' => 'astuces-fete-chandals-noel-rigolos',


		// Article - Gingerbread Spice Latte
		'page.holiday-tips.article.gingerbread.title' => 'Latte au pain d’épices',

		'page.holiday-tips.article.gingerbread.intro' => 'Goûtez à cette gâterie gourmande du temps des Fêtes.',
		'page.holiday-tips.article.gingerbread.ingredients' => '
			<li><a href="http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee" target="_blank" title="1 capsule K-Cup® Van Houtte® mélange des fêtes" class="tracking-ga" data-url-ga="van-houtte-holiday-blend-k-cup-pod">1 capsule K-Cup<sup>®</sup> Van Houtte® mélange des fêtes</a></li>
			<li>1/3 de cuillère à thé (2 ml) de gingembre moulu</li>
			<li>1/4 de cuillère à thé (1,5 ml)  de cannelle moulue</li>
			<li>1 pincée de muscade moulue</li>
			<li>1 pincée de clou de girofle moulu</li>
			<li>1/4 de cuillère à table (4 ml) d’extrait de vanille</li>
			<li>Sucre</li>
			<li>1 tasse (250 ml) de lait</li>
			<li>Crème fouettée</li>
		',
		'page.holiday-tips.article.gingerbread.preparation' => '
			<li>Infuser 120 ml (4 oz) de café Van Houtte<sup>®</sup> mélange des fêtes dans une tasse de 415 ml (14 oz).</li>
			<li>Ajouter toutes les épices et la vanille.</li>
			<li>Brasser et sucrer au goût.</li>
			<li>Mousser le lait, puis l’ajouter au café.</li>
			<li>Garnir de crème fouettée.</li>
		',

		'page.holiday-tips.article.gingerbread.urlID' => 'latte-pain-epices',


		// Article - Cappuccino Pudding
		'page.holiday-tips.article.cappucino.title' => 'Pudding au cappuccino',

		'page.holiday-tips.article.cappucino.intro' => 'Une recette amusante à essayer, en utilisant les aliments déjà présents dans votre garde-manger.',
		'page.holiday-tips.article.cappucino.ingredients' => '
			<li>Une capsule K-Cup<sup>®</sup> aromatisée à la vanille française, infusée au réglage pour 6 oz</li>
			<li>1 enveloppe de gélatine</li>
			<li>½ tasse de lait concentré sucré</li>
			<li>½ tasse de glaçons (environ 4)</li>
			<li>Crème fouettée</li>
		',
		'page.holiday-tips.article.cappucino.preparation' => '
			<li>Saupoudrer la gélatine sur le café, fouetter et laisser fleurir 2 minutes.</li>
			<li>Transférer dans un mélangeur, ajouter le lait condensé sucré et mélanger à basse vitesse 2 minutes, ou jusqu’à ce que la gélatine soit complètement dissoute.</li>
			<li>Ajouter les cubes de glace, 1 à la fois, mélangeant après chaque glaçon jusqu’à ce qu’il soit fondu.</li>
			<li>Verser dans 4 verres à parfait.</li>
			<li>Réfrigérer 2 heures ou jusqu’à ce que le mélange fige.</li>
			<li>Garnir de crème fouettée.</li>
		',
		'page.holiday-tips.article.cappucino.portion' => 'Donne 4 portions.',
		'page.holiday-tips.article.cappucino.suggestion.title' => 'CAPSULE K-CUP<sup>®</sup> SUGGÉRÉE',
		'page.holiday-tips.article.cappucino.suggestion.content-1' => '<a href="http://www.keurig.ca/Boissons/Caf%C3%A9/Vanille-fran%C3%A7aise/p/french-vanilla-coffee-van-houtte-k-cup" target="_blank" title="Vanille française de Van Houtte®" class="tracking-ga" data-url-ga="van-houtte-french-vanilla">Vanille française de Van Houtte<sup>®</sup></a>',
		'page.holiday-tips.article.cappucino.suggestion.content-2' => 'La perfection, parfaitement équilibrée. Une harmonie de café riche et crémeux et de vanille délicate.',
		'page.holiday-tips.article.cappucino.tip.content' => 'Ouvrez une capsule K-Cup® et saupoudrez un peu de café moulu sur chaque pudding.',

		'page.holiday-tips.article.cappucino.urlID' => 'pudding-cappucino',


		// Article - Candy Cane Latte
		'page.holiday-tips.article.candy-cane.title' => 'Latte à la canne de bonbon',

		'page.holiday-tips.article.candy-cane.intro' => 'Trinquez avec les petits, comme les grands, avec ce délice des Fêtes!',
		'page.holiday-tips.article.candy-cane.ingredients' => '
			<li><a href="http://www.keurig.ca/Boissons/M%C3%A9lange-de-No%C3%ABl/p/christmas-blend-timothys-coffee" target="_blank" title="1 capsule K-Cup® Timothy’s® mélange de Noël" class="tracking-ga" data-url-ga="timothys-christmas-blend-k-cup-pod">1 capsule K-Cup<sup>®</sup> Timothy’s<sup>®</sup> mélange de Noël</a></li>
			<li>120 ml (4 oz) de lait</li>
			<li>1 canne de bonbon concassée</li>
			<li>Crème fouettée</li>
		',
		'page.holiday-tips.article.candy-cane.preparation' => '
			<li>Placer la canne de bonbon concassée dans le fond d’une tasse de 415 ml (14 oz), et réserver quelques morceaux.</li>
			<li>Infuser 175 ml (6 oz) de café Timothy’s<sup>®</sup> mélange de Noël directement sur la canne de bonbon concassée.</li>
			<li>Bien mélanger, afin que la canne de bonbon soit fondue.</li>
			<li>Mousser le lait, puis l’ajouter au café.</li>
			<li>Ajouter la crème fouettée.</li>
			<li>Décorer avec quelques morceaux de canne de bonbon concassée.</li>
		',

		'page.holiday-tips.article.candy-cane.urlID' => 'latte-canne-bonbon',


		// Article - Poached Pears
		'page.holiday-tips.article.poached-pears.title' => 'Poires pochées',

		'page.holiday-tips.article.poached-pears.intro' => 'Un dessert très élégant qui se concocte en quelques minutes.',
		'page.holiday-tips.article.poached-pears.portion' => 'Donne 4 portions',
		'page.holiday-tips.article.poached-pears.ingredients' => '
			<li>1 capsule K-Cup<sup>®</sup> de café colombien infusé au réglage pour 6 oz</li>
			<li>2 tasses d’eau</li>
			<li>⅓ de sucre brun</li>
			<li>4 poires Bosc pelées, coupées en deux et évidées</li>
		',
		'page.holiday-tips.article.poached-pears.content-1' => 'Combiner le café, l’eau et le sucre brun dans une grande poêle.  Ajouter les poires, porter à ébullition puis baisser la température et laisser mijoter 8 minutes. Retourner doucement les poires en utilisant une spatule de caoutchouc et pocher encore 10 minutes ou jusqu’à ce que les poires soient tendres et que le liquide devienne sirupeux. Pour servir, napper sur les poires.',
		'page.holiday-tips.article.poached-pears.suggestion.title' => 'CAPSULE K-CUP<sup>®</sup> SUGGÉRÉE',
		'page.holiday-tips.article.poached-pears.suggestion.content-1' => '<a href="http://www.keurig.ca/Boissons/Caf%C3%A9/Colombie/p/colombia-coffee-barista-prima-k-cup" target="_blank" title="Café Colombie de Barista Prima Coffeehouse®" class="tracking-ga" data-url-ga="barista-prima-coffeehouse-colombia-coffee">Café Colombie de Barista Prima Coffeehouse<sup>®</sup></a>',
		'page.holiday-tips.article.poached-pears.suggestion.content-2' => 'La magie des Andes dans votre tasse. Des grains de café Arabica accentués par des notes fruitées et une touche distinctive de noix. ',
		'page.holiday-tips.article.poached-pears.tip.content' => 'Les framboises sont une garniture parfaite avec leur couleur vive et leur douceur discrète. Servez avec ou sans crème glacée.',

		'page.holiday-tips.article.poached-pears.urlID' => 'poires-pochees',


		// Article - Craft a D.I.Y. gift basket for your guests
		'page.holiday-tips.article.gift-basket.title' => 'Fabriquez un panier de cadeaux pour vos invités',

		'page.holiday-tips.article.gift-basket.intro' => 'Quand on aime offrir, c’est toujours une bonne idée de fabriquer des paniers de cadeaux personnalisés pour vos invités. Voici quatre paniers à thème qui ajouteront une touche unique à vos événements. Emballez de nouveaux classiques!',
		'page.holiday-tips.article.gift-basket.goodies-title' => 'Gâteries suggérées :',

		'page.holiday-tips.article.gift-basket.subtitle-1' => 'Les fêtes à la maison',
		'page.holiday-tips.article.gift-basket.content-1' => 'Notre panier « les Fêtes à la maison » est rempli de toutes sortes de petits objets qui rappellent le temps des Fêtes. Personnalisez-le avec un petit cadeau accompagné d’un mot ou alors une gâterie faite maison!',
		'page.holiday-tips.article.gift-basket.goodies-1' => '
			<li><a href="http://www.keurig.ca/Boissons/M%C3%A9lange-%C3%A0-cidre-de-pomme-chaud/p/apple-cider-green-mountain-k-cup" target="_blank" title="Green MountainTM Mélange à cidre de pomme chaud" class="tracking-ga" data-url-ga="green-mountain-hot-apple-cider-mix">Green Mountain<sup>TM</sup> Mélange à cidre de pomme chaud</a></li>
			<li>Maïs soufflé aromatisé</li>
			<li>Savon à main à la menthe</li>
			<li>Chandelles à la cannelle</li>
			<li>Moules à biscuits en forme de flocons</li>
		',

		'page.holiday-tips.article.gift-basket.subtitle-2' => 'Sucre et épices',
		'page.holiday-tips.article.gift-basket.content-2' => 'Peu importe comment vous célébrez les Fêtes, les pâtisseries sont certainement sur la liste. Notre bonbonnière « sucre et épices » est composée de tous les accessoires et les gâteries dont vous aurez besoin pour votre cuisine des Fêtes! Ajoutez une recette de famille avec les ingrédients nécessaires dans un pot mason. Ainsi, la personne pourra goûter à l’une de vos recettes du temps des Fêtes!',
		'page.holiday-tips.article.gift-basket.goodies-2' => '
			<li>Rouleau de pâtisserie gravé</li>
			<li>Ensemble de décoration de biscuits</li>
			<li><a href="http://www.keurig.ca/Boissons/Chocolat-Chaud/M%C3%A9lange-%C3%A0-chocolat-chaud/p/hot-chocolate-mix-laura-secord-k-cup" target="_blank" title="Laura Secord® Mélange à chocolat chaud" class="tracking-ga" data-url-ga="laura-secord-hot-chocolate-mix">Laura Secord<sup>®</sup> Mélange à chocolat chaud</a></li>
			<li>Extrait de vanille pure</li>
			<li>Naperons festifs</li>
		',

		'page.holiday-tips.article.gift-basket.subtitle-3' => 'Doux et chaleureux',
		'page.holiday-tips.article.gift-basket.content-3' => 'Il va sans dire que le temps des Fêtes est bien mouvementé, et voici d’où vient notre petit seau « doux et chaleureux ». Non seulement tout son contenu peut être utilisé durant l’année, mais c’est aussi le parfait petit paquet pour ceux qui oublient de se faire plaisir. Incorporez un film préféré ou une tasse qui fait référence à une blague pour personnaliser votre cadeau.',
		'page.holiday-tips.article.gift-basket.goodies-3' => '
			<li>Couverture douillette</li>
			<li>Pantoufles confortables</li>
			<li>Noix confites</li>
			<li>Huiles essentielles à la lavande</li>
			<li><a href="http://www.keurig.ca/Boissons/M%C3%A9lange-de-No%C3%ABl/p/christmas-blend-timothys-coffee" target="_blank" title="Timothy’s® Mélange de Noël" class="tracking-ga" data-url-ga="timohys-christmas-blend">Timothy’s<sup>®</sup> Mélange de Noël</a></li>
			<li><a href="http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee" target="_blank" title="Van Houtte® Mélanges des fêtes" class="tracking-ga" data-url-ga="van-houtte-holiday-blend">Van Houtte<sup>®</sup> Mélanges des fêtes</a></li>
		',

		'page.holiday-tips.article.gift-basket.subtitle-4' => 'Grillades au feu de bois',
		'page.holiday-tips.article.gift-basket.content-4' => 'Les mois d’hiver sont parfaits pour passer les nuits autour d’un feu crépitant. Notre fourre-tout en canevas est rempli de tout ce dont vous aurez besoin quand il fait froid dehors. Personnalisez-le en y glissant un bon roman que votre ami appréciera!',
		'page.holiday-tips.article.gift-basket.goodies-4' => '
			<li>Allume-feu décoratif</li>
			<li>Ensemble pour « smores »</li>
			<li>Écharpe carreautée douillette</li>
			<li>Bâtonnets de cannelle</li>
			<li><a href="http://www.keurig.ca/Accessoires/Tasse-de-voyage-en-c%C3%A9ramique-blanche/p/holiday-travel-mug" target="_blank" title="Tasse de voyage en céramique blanche" class="tracking-ga" data-url-ga="white-ceramic-travel-mug">Tasse de voyage en céramique blanche</a></li>
			<li>Porte-buches en canevas</li>
		',

		'page.holiday-tips.article.gift-basket.urlID' => 'fabriquez-panier-cadeaux',


		// Article - Elf’s Latte
		'page.holiday-tips.article.elf-latte.title' => 'Latte du lutin',

		'page.holiday-tips.article.elf-latte.intro' => 'Sucrez vos festivités avec ce latte simple et savoureux!',
		'page.holiday-tips.article.elf-latte.ingredients' => '
			<li><a href="" target="_blank" title="1 capsule K-Cup® Van Houtte® mélange des fêtes" class="tracking-ga" data-url-ga="van-houtte-holiday-blend-k-cup-pod">1 capsule K-Cup<sup>®</sup> Van Houtte<sup>®</sup> mélange des fêtes</a></li>
			<li>120 ml (4 oz) de lait écrémé</li>
			<li>30 ml (1 oz) de liqueur de crème irlandaise</li>
			<li>Crème fouettée</li>
		',
		'page.holiday-tips.article.elf-latte.preparation' => '
			<li>Infuser 175-235 ml (6-8 oz) de café Van Houtte<sup>®</sup> mélange des fêtes, dans une tasse de 415 ml (14 oz.).</li>
			<li>Pendant l’infusion du café, réchauffer le lait dans un mousseur à lait.</li>
			<li>Lorsque le café est prêt, y ajouter 120 ml (4 oz) de lait chaud.</li>
			<li>Ajouter la liqueur de crème irlandaise.<sup>*</sup></li>
			<li>Garnir de crème fouettée.</li>
		',
		'page.holiday-tips.article.elf-latte.content' => '<sup>*</sup>Buvez de façon responsable',

		'page.holiday-tips.article.elf-latte.urlID' => 'latte-lutin',


		// Article - Wow your crowd with a stunning table presentation
		'page.holiday-tips.article.table-presentation.title' => 'Impressionnez vos invités avec une table éblouissante',

		'page.holiday-tips.article.table-presentation.intro' => 'Les repas sont parfaits pour réunir vos proches durant le temps des Fêtes, mais ils peuvent être assez difficiles à planifier. Tout le monde ne peut pas être Martha Stewart, mais il existe de nombreuses façons d’amener une élégance unique à votre table sans vous casser la tête. Découvrez des trucs et astuces plus bas pour créer une belle table, tout en simplicité!',

		'page.holiday-tips.article.table-presentation.subtitle-1' => 'Invitez la nature chez vous',
		'page.holiday-tips.article.table-presentation.content-1' => 'Alors que nous sommes bombardés d’arbres et de couronnes synthétiques dans les magasins, il nous arrive parfois d’oublier à quel point il serait plus simple d’utiliser ce que nous avons dans notre jardin. Une façon d’ajouter de la vie à votre table est de la décorer avec des éléments naturels. Embellissez vos marque-places avec un brin de romarin , d’houx ou d’épices, par exemple. Une autre petite astuce est de remplir de grands vases de verre avec des pommes de pin et un jeu de lumières de Noël.',

		'page.holiday-tips.article.table-presentation.subtitle-2' => 'Accueillez le charme fait maison',
		'page.holiday-tips.article.table-presentation.content-2' => 'Incorporez des articles excentriques et intéressants àvos décorations pour rehausser votre table des Fêtes. Vous pouvez utiliser des décorations de Noël de style vintage comme accents, par exemple en plaçant des assiettes décoratives rétro à chaque place, ou en utilisant les couverts des Fêtes de votre mère. Assurez-vous de ne pas trop en faire, pour que le résultat demeure de bon goût. ',

		'page.holiday-tips.article.table-presentation.subtitle-3' => 'Amenez le coeur à table',
		'page.holiday-tips.article.table-presentation.content-3-1' => 'Bien qu’un jeu d’échange de cadeaux peut nuire à votre jugement, n’oubliez pas la raison pour laquelle vous avez invité tout le monde chez vous! Lorsque tous sont assis ensemble et savourent leur repas, rappelez-vous de lever un verre pour les remercier et de parler de ce dont vous êtes reconnaissant. S’il s’agit d’un mélange d’invités de différentes parties de votre vie, glissez quelques informations sur les convives pour débuter une conversation sous leurs ustensiles. Si vous venez d’une famille avec une grande tradition des Fêtes, soulignez-les durant le repas et encouragez vos invités à partager sur leurs traditions préférées. ',
		'page.holiday-tips.article.table-presentation.content-3-2' => '<strong>N’oubliez pas :</strong> en gardant les choses simples et en vous concentrant sur certains articles qui se démarqueront du lot, vous pourrez vraiment profiter de vos invités sans avoir à vous dépêcherpour poser la touche finale!',

		'page.holiday-tips.article.table-presentation.urlID' => 'impressionnez-invites-table-eblouissante',


		// Article - Wild mushroom coffee risotto
		'page.holiday-tips.article.mushroom-risotto.title' => 'Risotto champignons sauvage et café',

		'page.holiday-tips.article.mushroom-risotto.intro' => 'Intensifiez ce risotto crémeux au fromage et aux champignons sauvages avec une saveur subtile de café et créez une nouvelle variante de ce plat favori réconfortant. Servez comme plat principal avec des légumes ou une salade, ou alors en entrée pour un repas chic à la maison. N’oubliez pas : la clé d’un risotto réussi est la patience et beaucoup de brassage.',
		'page.holiday-tips.article.mushroom-risotto.portion' => 'Donne 4 portions.',
		'page.holiday-tips.article.mushroom-risotto.ingredients' => '
			<li>8 tasses (2 litres) de bouillon de veau (ou de poulet)</li>
			<li>1 échalote, émincée</li>
			<li>2 c. à soupe (30 ml) de beurre</li>
			<li>2 c. à soupe (30 ml) d’huile d’olive</li>
			<li>350g de champignons sauvages (chanterelles entières et champignons café en coupés en tranches)</li>
			<li>2 tasses (500 ml) de riz Arborio</li>
			<li>1 c. à soupe (15 ml) de <a href="http://www.keurig.ca/Boissons/Caf%C3%A9/M%C3%A9lange-fran%C3%A7ais/p/french-roast-coffee-van-houtte-k-cup" target="_blank" title="Mélange français Van Houtte®" class="tracking-ga" data-url-ga="van-houtte-french-roast-dark-roast">Mélange français Van Houtte<sup>®</sup></a>, finement moulu</li>
			<li>1 tasse (250 ml) de vin blanc</li>
			<li>Eau, au besoin</li>
			<li>½ tasse (125 ml) de parmesan râpé</li>
			<li>Sel et poivre au goût</li>
		',
		'page.holiday-tips.article.mushroom-risotto.preparation' => '
			<li>Dans une petite casserole, porter le bouillon de veau à ébullition </li>
			<li>Dans une grande poêle, faire revenir l’échalote dans du beurre et de l’huile d’olive sur un feu moyen. Ajouter les champignons et faire cuire pendant 2 à 3 minutes.</li>
			<li>Ajouter le riz Arborio et le café moulu Van Houtte<sup>®</sup>. Brasser.</li>
			<li>Verser le vin blanc, augmenter le feu à moyen élevé et porter à ébullition tout en brassant.</li>
			<li>Ajouter le bouillon de boeuf en brassant, une louche à la fois (environ ½ tasse, ou 125 ml). Porter à ébullition et réduire après chaque louche de bouillon, en brassant constamment.</li>
			<li>Quand il n’y a plus de stock à ajouter, goûter le riz . S’il est cuit (tendre, mais encore un peu croquant), retirer la casserole du feu. Sinon, continuer à ajouter de l’eau de la même manière que le bouillon de veau jusqu’à cuisson complète.</li>
			<li>Une fois retiré du feu, ajouter parmesan, sel et poivre au goût. Mélanger.</li>
			<li>Servir immédiatement</li>
		',

		'page.holiday-tips.article.mushroom-risotto.urlID' => 'risotto-champignons-sauvage-cafe',



	// ******************************************************
	// Party Planner
	'page.party-planner.title' => 'Planificateur des fêtes',
	'page.party-planner.url' => 'planificateur',

	'page.party-planner.subtitle' => 'La magie des Fêtes vous tient à cœur? Passez plus de temps à profiter de vos invités et moins de temps à organiser vos soirées. Avec ce petit quiz, recevoir devient un véritable jeu d’enfant! ',
	'page.party-planner.articles.title' => 'Agrémentez votre fête avec ces idées inspirantes!',

		'page.party-planner.event.title' => 'Quel genre d’évènement organisez-vous?',
		'page.party-planner.event.question-1' => 'Souper festif',
		'page.party-planner.event.question-2' => 'Brunch chaleureux ',
		'page.party-planner.event.question-3' => 'Cocktail des Fêtes',

		'page.party-planner.guest.title' => 'Combien d’invités attendez-vous?',
		'page.party-planner.guest.question-1' => '1-5',
		'page.party-planner.guest.question-2' => '5-10',
		'page.party-planner.guest.question-3' => '10+',

		'page.party-planner.mood.title' => 'Quel genre d’ambiance désirez-vous créer?',
		'page.party-planner.mood.question-1' => 'Réconfortante',
		'page.party-planner.mood.question-2' => 'Excentrique ',
		'page.party-planner.mood.question-3' => 'Élégante',

		'page.party-planner.submit.title' => 'Que la fête commence',
		'page.party-planner.result.title' => 'Que la fête commence!',

		'page.party-planner.button.buy' => 'Magasinez',

		// Brewer
			// K200
			'page.party-planner.result.brewer-1.title' => 'Keurig<sup>®</sup> 2.0 K200',
			'page.party-planner.result.brewer-1.subtitle' => 'Système d’infusion',
			'page.party-planner.result.brewer-1.url' => 'http://www.keurig.ca/brewers/home/c/home101?q=200%3Arelevance&page=0&layoutStatus=grid&show=Page&text=200&categoryCode=home101&terms=&selectedFacets=',

			// K400
			'page.party-planner.result.brewer-2.title' => 'Keurig<sup>®</sup> 2.0 K400',
			'page.party-planner.result.brewer-2.subtitle' => 'Système d’infusion',
			'page.party-planner.result.brewer-2.url' => 'http://www.keurig.ca/brewers/home/c/home101?q=400%3Arelevance&page=0&layoutStatus=grid&show=Page&text=400&categoryCode=home101&terms=&selectedFacets=',

			// K500
			'page.party-planner.result.brewer-3.title' => 'Keurig<sup>®</sup> 2.0 K500',
			'page.party-planner.result.brewer-3.subtitle' => 'Système d’infusion',
			'page.party-planner.result.brewer-3.url' => 'http://www.keurig.ca/Systemes/Syst%C3%A8me-d%27infusion-Keurig%C2%AE-2-0-K500/p/Keurig-2-0-K500-Brewing-System',

			// KOLD
			'page.party-planner.result.brewer-4.title' => 'Keurig<sup>®</sup> KOLD',
			'page.party-planner.result.brewer-4.subtitle' => 'Système de boissons',
			'page.party-planner.result.brewer-4.url' => 'http://www.keurig.ca/Drinkmaker/Syst%C3%A8me-de-boissons-Keurig%C2%AE-KOLD%3Csup%3EMC%3C-sup%3E/p/Keurig-KOLD-G360-drinkmaker',

		// Coffee
			// Coffee Results HOT(5)
			'page.party-planner.result.coffee-1-1.title' => 'Boîte variée vos cafés favoris',
			'page.party-planner.result.coffee-1-1.subtitle' => '&nbsp;',
			'page.party-planner.result.coffee-1-1.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/c/Bo%C3%AEte-vari%C3%A9e---Vos-Caf%C3%A9s-Favoris-/p/all-time-favourite-variety-keurig-k-cup',

			'page.party-planner.result.coffee-1-2.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-1-2.subtitle' => 'Mélange des Fêtes',
			'page.party-planner.result.coffee-1-2.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee',

			'page.party-planner.result.coffee-1-3.title' => 'Timothy’s<sup>®</sup>',
			'page.party-planner.result.coffee-1-3.subtitle' => 'Mélange de Noël',
			'page.party-planner.result.coffee-1-3.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-de-No%C3%ABl/p/christmas-blend-timothys-coffee',

			// Coffee Results HOT (5-10)
			'page.party-planner.result.coffee-2-1.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-2-1.subtitle' => 'Colombien',
			'page.party-planner.result.coffee-2-1.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Colombien/p/colombian-medium-coffee-VH-k-carafe',

			'page.party-planner.result.coffee-2-2.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-2-2.subtitle' => 'Mélange des Fêtes',
			'page.party-planner.result.coffee-2-2.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee',

			'page.party-planner.result.coffee-2-3.title' => 'Timothy’s<sup>®</sup>',
			'page.party-planner.result.coffee-2-3.subtitle' => 'Mélange de Noël',
			'page.party-planner.result.coffee-2-3.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-de-No%C3%ABl/p/christmas-blend-timothys-coffee',

			'page.party-planner.result.coffee-2-4.title' => 'Laura Secord<sup>®</sup>',
			'page.party-planner.result.coffee-2-4.subtitle' => 'Mélange au chocolat chaud',
			'page.party-planner.result.coffee-2-4.url' => 'http://www.keurig.ca/Boissons/Chocolat-Chaud/M%C3%A9lange-%C3%A0-chocolat-chaud/p/hot-chocolate-mix-laura-secord-k-cup',

			// Coffee Results HOT(10+)
			'page.party-planner.result.coffee-3-1.title' => 'Van houtte<sup>®</sup>',
			'page.party-planner.result.coffee-3-1.subtitle' => 'Mélange français',
			'page.party-planner.result.coffee-3-1.url' => 'http://www.keurig.ca/Boissons/French-Roast-Coffee/p/french-roast-coffee-van-houtte-k-carafe',

			'page.party-planner.result.coffee-3-2.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-3-2.subtitle' => 'Mélange des Fêtes',
			'page.party-planner.result.coffee-3-2.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee',

			'page.party-planner.result.coffee-3-3.title' => 'Timothy’s<sup>®</sup>',
			'page.party-planner.result.coffee-3-3.subtitle' => 'Mélange de Noël',
			'page.party-planner.result.coffee-3-3.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-de-No%C3%ABl/p/christmas-blend-timothys-coffee',

			'page.party-planner.result.coffee-3-4.title' => 'Laura Secord<sup>®</sup>',
			'page.party-planner.result.coffee-3-4.subtitle' => 'Mélange au chocolat chaud ',
			'page.party-planner.result.coffee-3-4.url' => 'http://www.keurig.ca/Boissons/Chocolat-Chaud/M%C3%A9lange-%C3%A0-chocolat-chaud/p/hot-chocolate-mix-laura-secord-k-cup',

			'page.party-planner.result.coffee-3-5.title' => 'Van Houtte<sup>®</sup>',
			'page.party-planner.result.coffee-3-5.subtitle' => 'Cappuccino - Collection de Spécialité',
			'page.party-planner.result.coffee-3-5.url' => 'http://www.keurig.ca/Boissons/Sp%C3%A9cialit%C3%A9/Cappuccino---Collection-de-Sp%C3%A9cialit%C3%A9-/p/specialty-cappuccino-coffee-van-houtte-k-cup',


			// Drink Results KOLD (<5)
			'page.party-planner.result.coffee-4-1.title' => 'Seraphine<sup>MC</sup> Lime de Perse',
			'page.party-planner.result.coffee-4-1.url' => 'http://www.keurig.ca/Kold-Beverages/Eau-gaz%C3%A9fi%C3%A9e---lime-de-perse/p/persian-lime-carbonated-water-seraphine-kold-pod',

			'page.party-planner.result.coffee-4-2.title' => 'Red Barn<sup>MC</sup> Cola au sucre de canne',
			'page.party-planner.result.coffee-4-2.url' => 'http://www.keurig.ca/Kold-Beverages/Soda-artisanal---sucre-de-canne/p/cane-cola-craft-soda-red-barn-kold-pod',

			// Drink Results KOLD (5-10)
			'page.party-planner.result.coffee-5-1.title' => 'Seraphine<sup>MC</sup> Fruit de la passion',
			'page.party-planner.result.coffee-5-1.url' => 'http://www.keurig.ca/Kold-Beverages/Eau-gaz%C3%A9fi%C3%A9e---fruit-de-la-passion/p/passion-fruit-carbonated-water-seraphine-kold-pod',

			'page.party-planner.result.coffee-5-2.title' => 'Coca Cola<sup>®</sup>',
			'page.party-planner.result.coffee-5-2.url' => 'http://www.keurig.ca/Kold-Beverages/Coca-Cola%3Csup%3EMD%3C-sup%3E/p/coca-cola-kold-pod',

			'page.party-planner.result.coffee-5-3.title' => 'Red Barn<sup>MC</sup> Cola au sucre de canne',
			'page.party-planner.result.coffee-5-3.url' => 'http://www.keurig.ca/Kold-Beverages/Soda-artisanal---sucre-de-canne/p/cane-cola-craft-soda-red-barn-kold-pod',

			// Drink Results KOLD (10+)
			'page.party-planner.result.coffee-6-1.title' => 'Seraphine<sup>MC</sup> Lime de Perse',
			'page.party-planner.result.coffee-6-1.url' => 'http://www.keurig.ca/Kold-Beverages/Eau-gaz%C3%A9fi%C3%A9e---lime-de-perse/p/persian-lime-carbonated-water-seraphine-kold-pod',

			'page.party-planner.result.coffee-6-2.title' => 'Coca Cola<sup>®</sup>',
			'page.party-planner.result.coffee-6-2.url' => 'http://www.keurig.ca/Kold-Beverages/Coca-Cola%3Csup%3EMD%3C-sup%3E/p/coca-cola-kold-pod',

			'page.party-planner.result.coffee-6-3.title' => 'Red Barn<sup>MC</sup> Cola au sucre de canne',
			'page.party-planner.result.coffee-6-3.url' => 'http://www.keurig.ca/Kold-Beverages/Soda-artisanal---sucre-de-canne/p/cane-cola-craft-soda-red-barn-kold-pod',

			'page.party-planner.result.coffee-6-4.title' => 'Waterful<sup>MC</sup> Baies divines',
			'page.party-planner.result.coffee-6-4.url' => 'http://www.keurig.ca/Kold-Beverages/Waterful-Baies-divines/p/blissful-berry-waterful-kold-pod',


		// Accessory
			'page.party-planner.result.accessory-1.title' => 'Tasse de voyage en céramique blanche',
			'page.party-planner.result.accessory-1.subtitle' => '&nbsp;',
			'page.party-planner.result.accessory-1.url' => 'http://www.keurig.ca/Accessoires/Tasse-de-voyage-en-c%C3%A9ramique-blanche/p/holiday-travel-mug',

			'page.party-planner.result.accessory-2.title' => 'Keurig<sup>MC</sup> 2.0',
			'page.party-planner.result.accessory-2.subtitle' => 'Carrousel',
			'page.party-planner.result.accessory-2.url' => 'http://www.keurig.ca/Accessoires/Carrousel-Keurig%3Csup%3EMC%3C-sup%3E-2-0/p/keurig-2-0-carousel',

			'page.party-planner.result.accessory-3.title' => 'Keurig<sup>®</sup>',
			'page.party-planner.result.accessory-3.subtitle' => 'Collage<sup>MC</sup> unité de rangement nouveau blanc',
			'page.party-planner.result.accessory-3.url' => 'http://www.keurig.ca/Accessoires/Keurig%C2%AE-Collage%3Csup%3EMC%3C-sup%3E-unit%C3%A9-de-rangement-nouveau-blanc/p/keurig-collage-storage-unit-nouveau-white',

			'page.party-planner.result.accessory-4.title' => 'Keurig<sup>MC</sup> 2.0',
			'page.party-planner.result.accessory-4.subtitle' => 'Carafe isolante',
			'page.party-planner.result.accessory-4.url' => 'http://www.keurig.ca/Accessoires/Carafe-isolante-Keurig%3Csup%3EMC%3C-sup%3E-2-0/p/Keurig-2-0-Thermal-Carafe',

			'page.party-planner.result.accessory-5.title' => 'Keurig<sup>®</sup> Kold<sup>MC</sup>',
			'page.party-planner.result.accessory-5.subtitle' => 'Ensemble de verres glacier ',
			'page.party-planner.result.accessory-5.url' => 'http://www.keurig.ca/Kold-Accessories/KEURIG%C2%AE-KOLD%3Csup%3EMC%3C-sup%3E-Ensemble-de-verres-glacier-%3A-petit/p/kold-glacier-glass-set-small',

			'page.party-planner.result.accessory-6.title' => 'Keurig<sup>®</sup> Kold<sup>MC</sup>',
			'page.party-planner.result.accessory-6.subtitle' => 'Rangement pour capsules Carrousel inox',
			'page.party-planner.result.accessory-6.url' => 'http://www.keurig.ca/Kold-Accessories/KEURIG%C2%AE-KOLD%3Csup%3EMC%3C-sup%3E-Rangement-pour-capsules-Carrousel-inox/p/wire-carousel-pod-storage-keurig-kold',

			'page.party-planner.result.accessory-7.title' => 'Keurig<sup>®</sup> Kold<sup>MC</sup>',
			'page.party-planner.result.accessory-7.subtitle' => 'Rangement pour capsules Turnstyle',
			'page.party-planner.result.accessory-7.url' => 'http://www.keurig.ca/Kold-Accessories/KEURIG%C2%AE-KOLD%3Csup%3EMC%3C-sup%3E-Rangement-pour-capsules-Turnstyle/p/turnstyle-pod-storage-keurig-kold',


		// Message
		'page.party-planner.result.message-1' => 'Vous planifiez un souper réconfortant avec vos proches? Keurig<sup>®</sup> est là pour vous. Imprésionnez vos invités avec un panier cadeau personnalisé, un somptueux risotto et un système d’infusion Keurig<sup>®</sup> K200 coloré et compact!',
		'page.party-planner.result.message-2' => 'Vous planifiez un souper extravagant avec vos proches? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisie avec un concours de chandails farfelus, un somptueux risotto et un système d’infusion Keurig<sup>®</sup> K200 coloré et compact!',
		'page.party-planner.result.message-3' => 'Vous planifiez un souper chic avec vos proches? Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante, un somptueux risotto et un système d’infusion Keurig<sup>®</sup> K200 coloré et compact!',
		'page.party-planner.result.message-4' => 'Vous planifiez un souper réconfortant avec votre entourage? Keurig<sup>®</sup> est là pour vous. Impressionnez vos invités avec un panier cadeau personnalisé et un somptueux risotto. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-5' => 'Vous planifiez un souper extravagant avec votre entourage? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisie avec un concours de chandails farfelus et un somptueux risotto. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-6' => 'Vous planifiez un souper chic avec  votre entourage? Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante et un somptueux risotto.  À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-7' => 'Vous planifiez un souper réconfortant avec votre famille élargie? Keurig<sup>®</sup> est là pour vous. Impressionnez vos invités avec un panier cadeau personnalisé et un somptueux risotto. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K500 vous aidera à infuser des carafes pour tout le monde!',
		'page.party-planner.result.message-8' => 'Vous planifiez un souper extravagant avec votre famille élargie? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisie avec un concours de chandails farfelus et un somptueux risotto. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K500 vous aidera à infuser des carafes pour tout le monde!',
		'page.party-planner.result.message-9' => 'Vous planifiez un souper chic avec  votre famille élargie? Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante et un somptueux risotto.  À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-10' => 'Vous planifiez un brunch réconfortant avec vos proches? Keurig<sup>®</sup> est là pour vous. Impressionnez vos invités avec un panier cadeau personnalisé, des poires parfaitement pochées et un système d’infusion Keurig<sup>®</sup> K200 coloré et compact!',
		'page.party-planner.result.message-11' => 'Vous planifiez un brunch extravagant avec vos proches? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisie avec un concours de chandails farfelus, des poires parfaitement pochées et un système d’infusion Keurig<sup>®</sup> K200 coloré et compact!',
		'page.party-planner.result.message-12' => 'Vous planifiez un brunch chic avec vos proches? Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante, des poires parfaitement pochées et un système d’infusion Keurig<sup>®</sup> K200 coloré et compact!',
		'page.party-planner.result.message-13' => 'Vous planifiez un brunch réconfortant avec votre entourage? Keurig<sup>®</sup> est là pour vous. Impressionnez vos invités avec un panier cadeau personnalisé et des poires parfaitement pochées. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-14' => 'Vous planifiez un brunch extravagant avec votre entourage? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisieavec un concours de chandails farfelus et des poires parfaitement pochées. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-15' => 'Vous planifiez un brunch chic avec  votre entourage? Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante et des poires parfaitement pochées.  À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-16' => 'Vous planifiez un brunch réconfortant avec votre famille élargie? Keurig<sup>®</sup> est là pour vous. Impressionnez vos invités avec un panier cadeau personnalisé et des poires parfaitement pochées. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K500 vous aidera à infuser des carafes pour tout le monde!',
		'page.party-planner.result.message-17' => 'Vous planifiez un brunch extravagant avec votre famille élargie? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisie avec un concours de chandails farfelus et des poires parfaitement pochées. À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K500 vous aidera à infuser des carafes pour tout le monde!',
		'page.party-planner.result.message-18' => 'Vous planifiez un brunch chic avec  votre famille élargie? Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante et des poires parfaitement pochées.  À l’heure du dessert, le système d’infusion Keurig<sup>®</sup> K400 vous aidera à infuser une carafe pour tout le monde!',
		'page.party-planner.result.message-19' => 'Vous planifiez un charmant cocktail? Keurig<sup>®</sup> est là pour vous. Impressionnez vos invités avec un panier cadeau personnalisé et le parfait pudding au cappuccino. À l’heure de l’apéro, le système de boissons KOLD<sup>MC</sup> vous aidera derrière le bar!',
		'page.party-planner.result.message-20' => 'Vous planifiez un cocktail extravagant? Keurig<sup>®</sup> est là pour vous. Ajoutez une touche de fantaisie avec un concours de chandails farfelus et le parfait pudding au cappuccino. À l’heure de l’apéro, le système de boissons KOLD<sup>MC</sup> vous aidera derrière le bar!',
		'page.party-planner.result.message-21' => 'Vous planifiez un cocktail chic?  Keurig<sup>®</sup> est là pour vous. Charmez vos invités avec une table renversante et le parfait pudding au cappuccino.  À l’heure de l’apéro, le système de boissons KOLD<sup>MC</sup> vous aidera derrière le bar!',


	// ******************************************************
	// Gift Guide
	'page.gift-guide.title' => 'Guide cadeaux',
	'page.gift-guide.url' => 'guide-cadeaux',

		'page.gift-guide.subtitle' => 'Trouver les cadeaux parfaits pour vos proches n’a jamais été aussi facile. Conçus pour tous les budgets, nos assortiments cadeaux simplifieront votre magasinage des Fêtes. Répandez de la joie avec Keurig<sup>®</sup>!',

		// Generic
		'page.gift-guide.button.discover' => 'Découvrez',
		'page.gift-guide.button.buy' => 'Magasinez ce cadeau',
		'page.gift-guide.gift' => 'Cadeaux',

		// Category titles
		'page.gift-guide.categorytitle.50less' => '50 $ ou moins',
		'page.gift-guide.categorytitle.50to150' => '50 $ à 150 $',
		'page.gift-guide.categorytitle.50amdmore' => '150 $ et plus',

		// Package 1
		'page.gift-guide.package.pack1.title' => 'Pour les grands voyageurs ',
		'page.gift-guide.package.pack1.subtitle' => 'La meilleure façon de savourer les saveurs des fêtes où que vous soyez.',
		'page.gift-guide.package.pack1.price' => '40 $',
		'page.gift-guide.package.pack1.content' => '
			<li>Tasse de voyage hivernale en édition limitée</li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Timothy’s<sup>®</sup> Mélange de Noël</li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Van Houtte<sup>®</sup> Mélange des Fêtes</li>
		',
		'page.gift-guide.package.pack1.url' => 'http://www.keurig.ca/holiday/c/grands-voyageurs/c/busy101',

		// Package 2
		'page.gift-guide.package.pack2.title' => 'Pour les dents sucrées',
		'page.gift-guide.package.pack2.subtitle' => 'Agencé avec ces délicieuses capsules K-Cup<sup>®</sup>, notre carrousel est un cadeau perpétuel! ',
		'page.gift-guide.package.pack2.price' => '41 $',
		'page.gift-guide.package.pack2.content' => '
			<li>Keurig<sup>®</sup> Néo<sup>MC</sup> carrousel</li>
			<li>1 Boîte de capsules K-Cup<sup>®</sup> de Mélange à cidre de pomme chaud</li>
		',
		'page.gift-guide.package.pack2.url' => 'http://www.keurig.ca/holiday/c/dents-sucrees/c/sweet101',

		// Package 3
		'page.gift-guide.package.pack3.title' => 'Pour les amoureux de l’hiver',
		'page.gift-guide.package.pack3.subtitle' => 'Infusez une avalanche de saveurs avec ces cadeaux chaleureux.',
		'page.gift-guide.package.pack3.price' => '188.92 $',
		'page.gift-guide.package.pack3.rebate' => '$129.99',
		'page.gift-guide.package.pack3.content' => '
			<li>Système d’infusion Keurig<sup>®</sup> K200</li>
			<li>Keurig<sup>TM</sup> Carrousel </li>
			<li>Café Colombien Van Houtte<sup>®</sup></li>
			<li>Mélange du matin Timothy’s<sup>®</sup></li>
		',
		'page.gift-guide.package.pack3.url' => 'http://www.keurig.ca/holiday/c/amoureux-hiver/c/winter101',
		'page.gift-guide.package.pack3.legal' => 'L’offre est disponible du 1<sup>er</sup> décembre 2015 à 0h (HNE) jusqu’au 25 décembre 2015 à 11h59 (HNE), ou jusqu’à épuisement des stocks. Valide uniquement en ligne à Keurig.ca. Lorsqu’un (1) Carrousel K-Cup®, un (1) système d’infusion Keurig® K200, une (1) boîte de capsules K-Cup® Van Houtte Colombien et une (1) boîte de capsules K-Cup® Timothy’s Mélange du petit déjeuner sont ajoutés au panier, le total du panier sera automatiquement réduit au prix de 129,99 $. Le prix régulier de cet ensemble est de 188,92 $. Un maximum de cinq (5) de chaque système d’infusion peut être ajouté à chaque commande. Cette promotion ne s’applique pas aux achats antérieurs. L’offre peut être modifiée ou annulée sans préavis. Des frais d’expédition supplémentaires peuvent s’appliquer aux emplacements situés à l’extérieur des zones métropolitaines. Ne peut être combinée avec d’autres offres, coupons ou promotions.',

		// Package 4
		'page.gift-guide.package.pack4.title' => 'Pour les amoureux des Fêtes',
		'page.gift-guide.package.pack4.subtitle' => 'Une combinaison gagnante pour souligner le plus beau moment de l’année.',
		'page.gift-guide.package.pack4.price' => '88 $',
		'page.gift-guide.package.pack4.content' => '
			<li>Une boîte de capsules K-Cup<sup>®</sup> Starbucks<sup>®</sup> Torréfaction Pike Place<sup>®</sup> </li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Folgers Gourmet Selections<sup>®</sup> Torréfaction traditionnelle<sup>®</sup></li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Barista Prima Coffeehouse<sup>®</sup> Mélange italien</li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Timothy’s<sup>®</sup> Mélange de Noël</li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Van Houtte<sup>®</sup> Mélange des Fêtes</li>
			<li>Une boîte de capsules K-Cup<sup>®</sup> Laura Secord<sup>®</sup> Mélange à chocolat chaud</li>
		',
		'page.gift-guide.package.pack4.url' => 'http://www.keurig.ca/holiday/c/amoureux-des-fetes/c/enthusiast101',
		'page.gift-guide.package.pack4.legal' => 'L’offre est disponible du 1<sup>er</sup> décembre 2015 à 17 h (HAE) jusqu’au 25 décembre 2015 à 23 h 59 (HAE), ou jusqu’à épuisement des stocks. Valide uniquement en ligne à Keurig.ca. Lorsqu’un minimum de 5 boîtes de capsules K-Cup®, K-Carafe<sup>MC</sup> ou Rivo® sont ajoutées au panier, seulement la 6e boîte la moins chère sera escomptée de son prix régulier. Les capsules Kold<sup>MC</sup> sont exclues de cette promotion. Le code de promotion PROMODESFETES doit être appliqué au moment de passer la commande. Seulement un (1) code de promotion ne peut être utilisé par commande. Cette promotion ne s’applique pas aux achats antérieurs. L’offre peut être modifiée ou annulée sans préavis. Des frais d’expédition supplémentaires peuvent s’appliquer aux emplacements situés à l’extérieur des zones métropolitaines. Ne peut être combinée avec d’autres offres, coupons ou promotions.',

		// Package 5
		'page.gift-guide.package.pack5.title' => 'Pour les hôtes extraordinaires',
		'page.gift-guide.package.pack5.subtitle' => 'L’ensemble cadeau par excellence pour remercier votre hôte.',
		'page.gift-guide.package.pack5.price' => '197 $',
		'page.gift-guide.package.pack5.content' => '
			<li>Système d’infusion Keurig<sup>®</sup> K400</li>
			<li>1 Boîte variée de capsules K-Cup<sup>®</sup> vos cafés favoris</li>
			<li>Tasse de voyage hivernale en édition limitée</li>
		',
		'page.gift-guide.package.pack5.url' => 'http://www.keurig.ca/holiday/c/hotes-extraoridinaire/c/homemaker101',

		// Package 6
		'page.gift-guide.package.pack6.title' => 'Pour les accros techno ',
		'page.gift-guide.package.pack6.subtitle' => 'Parfait pour satisfaire votre soif, ce cadeau est plus branché que jamais.',
		'page.gift-guide.package.pack6.price' => '431 $',
		'page.gift-guide.package.pack6.content' => '
			<li>Système de boissons Keurig<sup>®</sup> KOLD<sup>MC</sup></li>
			<li>Keurig<sup>®</sup> KOLD<sup>MC</sup> Ensemble de verres glacier</li>
			<li>Une boîte de capsules KOLD<sup>MC</sup> Flynn\'s<sup>MC</sup> Soda Shop Racinette</li>
			<li>Une boîte de capsules KOLD<sup>MC</sup> Coca-Cola<sup>®</sup></li>
		',
		'page.gift-guide.package.pack6.url' => 'http://www.keurig.ca/holiday/c/accros-techno/c/early101',



	// ******************************************************
	// Flavours of the season
	'page.flavours-season.title' => 'Saveurs de la saison',
	'page.flavours-season.url' => 'saveurs',

		'page.flavours-season.subtitle' => 'Infusez de la magie des fêtes! Découvrez pour une durée limitée notre sélection de mélanges des fêtes et remplissez votre tasse de beaux moments.',

		// Generic
		'page.flavours-season.button.readrecipe' => 'Lire la recette',
		'page.flavours-season.button.buy-online' => 'Magasinez en ligne',
		'page.flavours-season.button.buy' => 'Magasinez',

		// Featured
		'page.flavours-season.featured1.title' => 'Van Houtte<sup>®</sup> Mélange des fêtes',
		'page.flavours-season.featured1.content' => 'Le nouvel incontournable du temps des Fêtes! Une saveur riche, des notes boisées légèrement piquantes, le tout relevé par une touche d’acidité vivifiante, le Mélange des Fêtes illuminera vos matins hivernaux. Disponible en ligne et en <a href="http://www.keurig.ca/trouver-detaillant" target="_blank" title="magasin" class="tracking-ga" data-url-ga="stores">magasin</a>.',
		'page.flavours-season.featured1.shop.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-des-f%C3%AAtes/p/holiday-blend-van-houtte-coffee',
		'page.flavours-season.featured1.article.url' => 'latte-pain-epices',

		'page.flavours-season.featured2.title' => 'Timothy’s<sup>®</sup> Mélange de Noël',
		'page.flavours-season.featured2.content' => 'Comme le temps des Fêtes, le Mélange de Noël de Timothy’s® ne se présente qu’une fois par an! Un mélange classique des grains les plus fins, ce café velouté déborde d’arômes festifs et dévoile une saveur riche et douce intensément satisfaisante. Disponible en ligne et en <a href="http://www.keurig.ca/trouver-detaillant" target="_blank" title="magasin" class="tracking-ga" data-url-ga="stores">magasin</a>. ',
		'page.flavours-season.featured2.shop.url' => 'http://www.keurig.ca/Boissons/M%C3%A9lange-de-No%C3%ABl/p/christmas-blend-timothys-coffee',
		'page.flavours-season.featured2.article.url' => 'latte-canne-bonbon',

		// Suggestions
		'page.flavours-season.sugg.title' => 'Meilleures capsules K-Cup<sup>®</sup> de 2015',
		'page.flavours-season.sugg.subtitle' => 'Pour couronner le tout, nous avons compilé les capsules K-Cup<sup>®</sup> les plus aimées cette année. À vous de savourer!',

		'page.flavours-season.sugg1.img-width' => '167',
		'page.flavours-season.sugg1.img-height' => '167',
		'page.flavours-season.sugg1.title' => 'Van Houtte<sup>®</sup>',
		'page.flavours-season.sugg1.content' => 'Colombien',
		'page.flavours-season.sugg1.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Caf%C3%A9-Colombien/p/colombian-medium-coffee-VH-k-cup',

		'page.flavours-season.sugg2.img-width' => '167',
		'page.flavours-season.sugg2.img-height' => '167',
		'page.flavours-season.sugg2.title' => 'Barista Prima Coffeehouse<sup>®</sup>',
		'page.flavours-season.sugg2.content' => 'Mélange italien',
		'page.flavours-season.sugg2.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/M%C3%A9lange-italien/p/italian-roast-coffee-barista-prima-k-cup',

		'page.flavours-season.sugg3.img-width' => '172',
		'page.flavours-season.sugg3.img-height' => '172',
		'page.flavours-season.sugg3.title' => 'Timothy’s<sup>®</sup>',
		'page.flavours-season.sugg3.content' => 'Mélange du petit déjeuner',
		'page.flavours-season.sugg3.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/M%C3%A9lange-du-petit-d%C3%A9jeuner/p/english-breakfast-coffee-timothys-k-cup',

		'page.flavours-season.sugg4.img-width' => '172',
		'page.flavours-season.sugg4.img-height' => '172',
		'page.flavours-season.sugg4.title' => 'Donut House Collection<sup>®</sup>',
		'page.flavours-season.sugg4.content' => 'Maison du beigne',
		'page.flavours-season.sugg4.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Caf%C3%A9-Donut-House%C2%AE/p/donut-house-coffee-DHC-k-cup',

		'page.flavours-season.sugg5.img-width' => '169',
		'page.flavours-season.sugg5.img-height' => '169',
		'page.flavours-season.sugg5.title' => 'Van Houtte<sup>®</sup>',
		'page.flavours-season.sugg5.content' => 'Mélange de la maison',
		'page.flavours-season.sugg5.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/M%C3%A9lange-de-la-maison/p/original-house-blend-coffee-VH-k-cup',

		'page.flavours-season.sugg6.img-width' => '169',
		'page.flavours-season.sugg6.img-height' => '169',
		'page.flavours-season.sugg6.title' => 'Green Mountain Coffee<sup>®</sup>',
		'page.flavours-season.sugg6.content' => 'Magie Noire',
		'page.flavours-season.sugg6.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Magie-noire-tr%C3%A8s-intense/p/dark-magic-coffee-GMC-k-cup',

		'page.flavours-season.sugg7.img-width' => '167',
		'page.flavours-season.sugg7.img-height' => '167',
		'page.flavours-season.sugg7.title' => 'Van Houtte<sup>®</sup>',
		'page.flavours-season.sugg7.content' => 'Vanille noisette',
		'page.flavours-season.sugg7.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Vanille-noisette/p/vanilla-hazelnut-coffee-VH-k-cup',

		'page.flavours-season.sugg8.img-width' => '169',
		'page.flavours-season.sugg8.img-height' => '169',
		'page.flavours-season.sugg8.title' => 'Laura Secord<sup>®</sup>',
		'page.flavours-season.sugg8.content' => 'Mélange à chocolat chaud',
		'page.flavours-season.sugg8.url' => 'http://www.keurig.ca/Boissons/Chocolat-Chaud/M%C3%A9lange-%C3%A0-chocolat-chaud/p/hot-chocolate-mix-laura-secord-k-cup',

		'page.flavours-season.sugg9.img-width' => '169',
		'page.flavours-season.sugg9.img-height' => '168',
		'page.flavours-season.sugg9.title' => 'Starbucks<sup>®</sup>',
		'page.flavours-season.sugg9.content' => 'Torréfaction Pike Place<sup>®</sup>',
		'page.flavours-season.sugg9.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Torr%C3%A9faction-Pike-Place%C2%AE/p/pike-place-roast-coffee-starbucks-k-cup',

		'page.flavours-season.sugg10.img-width' => '167',
		'page.flavours-season.sugg10.img-height' => '167',
		'page.flavours-season.sugg10.title' => 'The Original Donut Shop<sup>MC</sup>',
		'page.flavours-season.sugg10.content' => 'Original Donut Shop<sup>MC</sup> Régulier',
		'page.flavours-season.sugg10.url' => 'http://www.keurig.ca/Boissons/Caf%C3%A9/Original-Donut-Shop%3Csup%3EMC%3C-sup%3E-r%C3%A9gulier/p/original-donut-shop-coffee-ODS-k-cup',


	// ******************************************************
	// Buttons
	'btn.close' => 'Retour au Guide des Fêtes',
	'btn.back' => 'Retour',


	// ******************************************************
	// Shares
	'share.title' => 'Partagez les conseils',
	'share.email.title' => 'En route vers la magie des Fêtes!',
	'share.email.message' => 'Bonjour,%0A%0AJe%20parcourais%20le%20Guides%20des%20F%C3%AAtes%20Keurig%C2%AE%20et%20cet%20article%20sympathique%20m%E2%80%99a%20fait%20penser%20%C3%A0%20toi.%0A%0A',
	'share.email.message-end' => '%0A%0AJoyeuses%20F%C3%AAtes!%0A%0A',


	// ******************************************************
	// Footer
	'footer.facebook-share' => 'Partagez la magie',
	'footer.language' => 'English',
	'footer.language.abr' => 'EN',
	'footer.go-to-keurig' => 'Aller sur Keurig.ca',
	'footer.go-to-keurig.url' => 'http://www.keurig.ca/accueil',


];