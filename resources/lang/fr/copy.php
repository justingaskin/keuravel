<?php
return [

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the global page. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	'language' => 'fr',
	'altLocale' => 'en',
	'title' => 'Votre Cadeau Keurig®',
//HOMEPAGE
	//HEADER
	'header.menu.link1' => 'Comment ça fonctionne?',
	'header.menu.link2' => 'Aide',
	'header.menu.link3' => 'Légal',
	'header.menu.link4' => 'Visitez Keurig.ca',
	'header.purchaseinstore' => 'Acheté en magasin?',
	//BANNER
	'banner.left.title' => '<span>ACHETEZ</span> UN SYSTÈME D\'INFUSION',
	'banner.left.text' => 'et obtenez jusqu\'à <br><span>70</span> $ en capsules K-cup<sup>®</sup><sup>*</sup>',
	'banner.left.btn' => 'EN SAVOIR PLUS ',

	'banner.right.title' => '<span>BÉNÉFICIEZ</span> DE CETTE OFFRE',

	'banner.right.text' => 'Avez-vous acheté un système d\'infusion entre le 1<sup>er</sup> octobre et le 15 novembre 2015?',

	'banner.right.btn' => 'OBTENEZ VOS CAPSULES',
	'banner.copy' => "*Certaines conditions s'appliquent. Applicable seulement sur les systèmes d'infusion sélectionnés. Offre valable jusqu’au 15 novembre 2015.",
	'banner.dogear' => "<strong>24h seulement</strong>pour acheter votre système d'infusion en magasin ou en ligne|<strong>:days jours seulement</strong>pour acheter votre système d'infusion en magasin ou en ligne",
	'banner.dogear.end' => "<strong>Cette offre est terminée.</strong>",
	//STEP
	'step' => "Étape",
	'step1.text' => "Choisissez l'offre qui vous convient",
	'step2.text' => "Recevez vos capsules K-Cup<sup>®</sup> gratuites",
	//OFFER
	'offer.btn' => "Achetez",
	'offer.link' => "Achetez en magasin",
	//OFFER1
	'offer1.title' => "Une offre pour les buveurs branchés",
	'offer1.info' => "Obtenez<strong><sup>$</sup>35</strong><span>de capsules K-Cup<sup>®</sup>gratuitement</span>",
	'offer1.text' => "à l'achat d'un système d'infusion Keurig<sup>®</sup><br>K200 ou K300.",
	'offer1.desc1' => "Coloré et compact",
	'offer1.desc2' => "Un monde de possibilités",
	//OFFER2
	'offer2.title' => "Une offre pour les amateurs de café",
	'offer2.info' => "Obtenez<strong><sup>$</sup>70</strong><span>de capsules K-Cup<sup>®</sup>gratuitement</span>",
	'offer2.text' => "à l'achat d'un système d'infusion Keurig<sup>®</sup><br>K400 ou K500.",
	'offer2.desc1' => "Plus de style, plus d'options",
	'offer2.desc2' => "La crème de la crème",
	//COMPARE
	'compare' => "Comparez les systèmes d'infusion",
	//JOURNEY1
	'journey1.yellow' => "Adepte des achats en ligne?",
	'journey1.block1' => "Achetez un système d'infusion sélectionné à Keurig.ca",
	'journey1.block2' => "Recevez votre code promotionnel par courriel",
	'journey1.block3' => "Utilisez votre code promotionnel pour commander votre Boîte variée et vos capsules K-Cup<sup>®</sup> gratuites à Keurig.ca",
	'journey.ship' => "TOUT EST LIVRÉ GRATUITEMENT!",
	//JOURNEY2
	'journey2.yellow' => "Vous préférez faire vos achats en magasin?",
	'journey2.block1' => "Achetez un système d'infusion chez l'un de ces <a href=':url'>détaillants</a>",
	'journey2.block2' => "Réclamez votre offre <a href=':url?caribou-2.0=step1-form'>ici</a> avant le 26/02/16",
	'journey2.block3' => "Recevez votre code promotionnel par courriel",
	'journey2.block4' => "Utilisez votre code promotionnel pour commander votre Boîte variée et vos capsules K-Cup<sup>®</sup> gratuites à Keurig.ca",
//PAGE FORM
	//BANNER
	'formbanner.title1' => "Acheté en magasin avec cette promotion?",
	'formbanner.title2' => "OBTENEZ VOS CAPSULES K-CUP<sup>®</sup> GRATUITES!",
	//JOURNEY
	'form.journey.block1' => "Achetez un système d'infusion chez un détaillant entre le 1<sup>er</sup> octobre et le 15 novembre 2015",
	'form.journey.block2' => "Complétez le formulaire ci-dessous",
	'form.journey.block3' => "Postez le formulaire complété, une copie de votre facture et le code barre sur la boîte de votre système d'infusion avant le 26 février 2016",
	'form.journey.block4' => "Recevez votre code promotionnel par courriel",
	'form.journey.block5' => "Utilisez votre code promotionnel sur Keurig.ca",
	'form.journey.ship' => "Votre commande vous est livrée gratuitement",
	//FORMULAIRE
	'form.error' => 'Une erreur s\'est produite. Veuillez vérifier le formulaire.',
	'form.title' => "Remplissez ce formulaire pour obtenir votre code promotionnel",
	'form.choose' => 'Choisir...',
	'form.required' => "Champs obligatoires",
	'form.field.firstname' => "Prénom",
	'form.field.lastname' => "Nom",
	'form.field.email' => "Courriel",
	'form.field.address1' => "Adresse ligne 1",
	'form.field.address2' => "Adresse ligne 2",
	'form.field.city' => "Ville",
	'form.field.province' => "Province",
	'form.field.postalcode' => "Code postal",
	'form.field.phone' => "Numéro de téléphone",
	'form.field.model' => "Modèle acheté",
		'form.field.model.opt1' => "Système d'infusion Keurig® 2.0 K200",
		'form.field.model.opt2' => "Système d'infusion Keurig® 2.0 K300",
		'form.field.model.opt3' => "Système d'infusion Keurig® 2.0 K400",
		'form.field.model.opt4' => "Système d'infusion Keurig® 2.0 K500",
	'form.field.date' => "Date de l'achat",
	'form.field.dateformat' => '(jj/mm/aaaa)',
	'form.field.from' => "Lieu d'achat",
	'form.hudsonsbay' => "LA BAIE D'HUDSON",
	'form.other' => "AUTRE",
	'form.field.specify' => "Précisez",
	'form.submit' => "Soumettre",
	'form.legal.offer' => "Cette offre s'applique uniquement aux systèmes d'infusion achetés entre le 1<sup>er</sup> octobre 2015 et le 15 novembre 2015.",
	'form.checkbox1' => "J'autorise Keurig<sup>®</sup> à me contacter par téléphone, courriel et courrier pour m'offrir des promotions et des bénéfices additionnels.",
	'form.checkbox2' => "J'accepte tous les <a target='_blank' href=':url'>termes et conditions</a> reliés à cette offre",
	//BLOCKS
	'form.block1.text' => "Trouvez les réponses à vos questions.",
	'form.block1.link' => 'http://www.keurig.ca/offre-bienvenue',
	'form.blocks.learnmore' => "En savoir plus",
	'form.block2.title' => "Vous aimeriez nous parler?",
	'form.block2.text' => "N'hésitez pas à contacter notre service à la clientèle",
	'form.block2.link' => 'tel:+1-800-361-5628',
	'form.blocks.phone' => "1-800-361-5628",
//PAGE THANKS
	//CONTENT
	'thanks.h1' => "Plus qu'une étape avant de recevoir votre code promotionnel",
	'thanks.receive' => "Le formulaire complété vous sera envoyé par courriel.",
	'thanks.print' => "Imprimez le formulaire complété.",
	'thanks.cut' => "Découpez le code barre sur la boîte de votre système d'infusion.",
	'thanks.copy' => "Faites une copie de votre facture.",
	'thanks.send' => "Envoyez-le tout par la poste au",
	'thanks.address' => "C.P 2010, Longueuil, Québec, J4K 5J9 avant le 26 février 2016",
	'thanks.infos' => "Nous traiterons votre demande dans un délai de 2 semaines et vous reviendrons avec un code promotionnel valide à Keurig.ca par courriel si vous rencontrez les critères d’admission.",
	'thanks.value' => 'Dépendant du système d\'infusion que vous avez acheté, un code promotionnel  de 35 $ ou de 70 $ en capsules K-Cup® vous sera envoyé.',
	'thanks.box35' => 'Le code promotionnel de 35 $ vous offre une Boîte variée et une boîte gratuite de 24 capsules K-Cup® de votre choix.',
	'thanks.box75' => 'Le code promotionnel de 70 $ vous offre une Boîte variée et trois boîtes gratuites de 24 capsules K-Cup® de votre choix.',
	//POPUP
	'popup.print' => "Imprimez votre formulaire complété",
	'popup.cut' => "Découpez le code barre du produit sur votre boîte",
	'popup.copy' => "Faites une copie de votre reçu ",
	'popup.send' => "Envoyez-nous le tout au<br>C.P 2010, Longueuil, Québec, J4K 5J9",
	'popup.infos' => "Votre demande sera traitée dans un délai de 2 semaines et un code promotionnel vous sera envoyé par courriel",
	//FOOTER
	'footer.help' => "Aide",
	'footer.contact' => "Contactez-nous",
	'footer.customer' => "Service à la clientèle 1 800 361 5628",
	'footer.customer2' => "Service à la clientèle <a href='tel:+18003615628'>1 800 361 5628</a>",
	'footer.email' => "Formulaire courriel",
	'footer.terms' => "Conditions générales",
	'footer.copy' => "Droits d'auteur Keurig Canada Inc. 2015 Tous droits réservés",
	'footer.privacy' => "Politique de confidentialité de Keurig.ca",
	'footer.privacy.link' => "http://www.keurig.ca/politique-confidentialite",

	//LINKS
	'link.keurig' => "http://www.keurig.ca/accueil",
	'link.mail' => "http://www.keurig.ca/service-clientele/courriel-contactez-nous",
	'link.legal' => "http://www.keurig.ca/conditions-utilisation",
	'link.retailer' => "http://www.keurig.ca/trouver-detaillant",

	'province' => [
		'BC' => 'Colombie-Britannique',
		'NB' => 'Nouveau-Brunswick',
		'NL' => 'Terre-Neuve-Et-Labrador',
		'NS' => 'Nouvelle-Écosse',
		'PE' => 'Île-Du-Prince-Édouard',
		'QC' => 'Québec',
		'NT' => 'Territoires-Du-Nord-Ouest',
	],

	'link.buy.k200' => "http://www.keurig.ca/Syst%C3%A8mes-d'infusion/Syst%C3%A8me-d'infusion-Keurig%C2%AE-2-0-K200-Zeste-d'orange/p/Keurig-2-0-k200-brewer-orange",
	'link.buy.k300' => "http://www.keurig.ca/Syst%C3%A8mes-d'infusion/%C3%80-la-maison/Syst%C3%A8me-d'infusion-Keurig%C2%AE-2-0-K300/p/Keurig-2-0-K300-Brewing-System",
	'link.buy.k400' => "http://www.keurig.ca/Syst%C3%A8mes-d'infusion/Syst%C3%A8me-d'infusion-Keurig%C2%AE-2-0-K400-Rouge-classique/p/Keurig-2-0-K400-Brewing-System-Vintage-Red",
	'link.buy.k500' => 'http://www.keurig.ca/Syst%C3%A8mes-d%27infusion/Syst%C3%A8me-d%27infusion-Keurig%C2%AE-2-0-K500/p/Keurig-2-0-K500-Brewing-System',
// PAGE PROMO END
	'end.title' => "Cette promotion est désormais terminée",
	'end.text' => "Si vous avez acheté un système d’infusion Keurig<sup>®</sup> en magasin entre le 1er octobre et le 15 novembre 2015,
	veuillez <a href=':url'>remplir ce formulaire</a> et suivre les étapes afin de recevoir votre code promotionnel.
	<br />
	<br />Visitez <a href='http://www.keurig.ca'>Keurig.ca</a> et abonnez-vous à l’infolettre pour découverir les promotions en cours.",
	'end.link-text' => "Magasinez",
	'end.link' => "http://www.keurig.ca",
];
