<?php

return [

    'required' => 'Le champ :attribute est requis.',

    'custom' =>
    [
        'purchasedate' =>
        [
            'required' => 'La date d\'achat est requise.'
        ]
    ],

    'attributes' =>
    [
        'firstname' => 'prénom',
        'lastname' => 'nom',
        'purchasedate' => 'date d\'achat',
        'postalcode' => 'code postal'
    ],

];