<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the Find a store page. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	'brewing' => "Systèmes d'infusion Keurig<sup>®</sup> K-Cup<sup>®</sup>",
	'infos' => "Vous trouverez les systèmes d’infusion Keurig<sup>®</sup> et les godets K-Cup<sup>®</sup> chez des milliers de détaillants à travers le Canada. Veuillez contacter le détaillant de votre région pour connaître les différents modèles de systèmes d’infusion et la variété de dosettes K-Cup<sup>®</sup> disponibles en magasin.",
	'nationalstores' => 'Détaillants nationaux et locaux',
	'link.ares' => 'https://www.arescuisine.com/fr/',
	'link.aviva' => 'http://www.aviva.ca/',
	'link.batteries' => 'http://www.batteriesincluded.ca/',
	'link.bedbath' => 'http://www.bedbathandbeyond.ca/',
	'link.staples' => 'http://www.staples.ca/fr/',
	'link.canadiantire' => 'http://www.canadiantire.ca/fr.html',
	'link.centrerasoir' => 'http://www.centredurasoir.com/fr/accueil.php',
	'link.corbeil' => 'https://www.corbeilelectro.com/fr',
	'link.costco' => 'http://www.costco.ca/?storeId=10302&catalogId=11201&langId=-25',
	'link.despres' => 'http://www.despreslaporte.com/1-1-accueil.html?DIVISIONID=1',
	'link.futureshop' => 'http://www.futureshop.ca/fr-ca/accueil.aspx?path=f9af28b84da40a62feed69e743160ef6fr99',
	'link.homeoutfitters' => 'http://www.homeoutfitters.com/fr/index.html',
	'link.hudson' => 'http://www.labaie.com/webapp/wcs/stores/servlet/fr/thebay#!/',
	'link.lasoupiere' => 'http://lasoupiere.com/fr/home-fr/',
	'link.korvette' => 'http://www.korvette.ca/default.asp?lan=fr',
	'link.linenchest' => 'http://www.linenchest.com/',
	'link.londondrugs' => 'http://www.londondrugs.com/',
	'link.personaledge' => 'http://www.personaledge.com/on/accueil.php',
	'link.rona' => 'http://www.rona.ca/fr',
	'link.sears' => 'http://www.sears.ca/accueil',
	'link.stokes' => 'http://www.stokesstores.com/fr/',
	'link.vinpassion' => 'https://vinetpassion.com/fr/',
	'link.walmart' => 'http://www.walmart.ca/fr/'
];