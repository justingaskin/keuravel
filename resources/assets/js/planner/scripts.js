/********************************************************
 * Mobile detect
 *******************************************************/
var tabletDevice = false,
	tabletSize = false
	mobileDevice = false,
	clickORtouchend = '',
	mouseenterORtouchend = '',
	mouseleaveORtouchend = '',
	androidBrowser = false,
	blurSupport = false,
	introDone = false,
	eventClicked = false,
	guestClicked = false,
	moodClicked = false,
	nextId = '',
	animationFlavoursSeason = false,
	animationGiftGuide = false,
	animationHolidayTips = false,
	animationPartyPlanner = false,
	hashTag = window.location.hash;

function responsiveDetect() {

	// Detect if real device
	if ($('html').hasClass('desktop')) {
		desktopDevice = true;
		tabletDevice = false;
		mobileDevice = false;
	} else if ($('html').hasClass('tablet')) {
		desktopDevice = false;
		tabletDevice = true;
		mobileDevice = false;
	} else if ($('html').hasClass('mobile')) {
		desktopDevice = false;
		tabletDevice = false;
		mobileDevice = true;
	}

	if ($('html').hasClass('android') && !$('html').hasClass('chrome')) {
		androidBrowser = true;
	}

	// Touchend or click
	if ((mobileDevice || tabletDevice) && !androidBrowser) {
		clickORtouchend = 'click';
		mouseenterORtouchend = 'touchend';
		mouseleaveORtouchend = 'touchend';
	} else {
		clickORtouchend = 'click';
		mouseenterORtouchend = 'mouseenter';
		mouseleaveORtouchend = 'mouseleave';
	}
}

responsiveDetect();


if ($('html').hasClass('cssfilters')) {
	blurSupport = true;
	$('.background-resize.blur-img').remove();
}

$(function(){

	/* ******************************************************
	   Background resize
	   http://jsfiddle.net/gaby/3YLQf/
	****************************************************** */
	if (!mobileDevice) {
		var stretcher = $('.background > img'), // Define image to stretch
			element = stretcher[0],
			currentSize = { width: 0, height: 0 },
			$document = $(document),
			$containter = $('.background')


		function resizeBackground() {
			var w = $document.width(); // Define document width
			var h = $document.height(); // Define document height

			if (currentSize.width != w || currentSize.height != h) {
				stretcher.width(w).height('auto');
				$containter.css('width', '');

				if (h > element.height) {
					stretcher.width('auto').height(h);
					$containter.css('width', element.width); // Hack for Firefox because the parent div doesn't read the width:auto; of the image
				}
				currentSize.width = w;
				currentSize.height = element.width;
			}
		}

		// Trigger the resize when resize
		$(window).resize(function () {
			setTimeout(function(){
				resizeBackground();
			}, 200);
		})

		$(document).ready(function() {
			$(this).trigger('resize');
		});

		// Trigger the resize on start
		$(window).load(function () {
			$(this).trigger('resize');
		});
	}
	

	/* ******************************************************
	   Loading on start
	****************************************************** */
	//$(window).load(function () {
	$(document).ready(function() {
		setTimeout(function(){
			$('.welcome').addClass('animate');
			$('.loading').animate({
				opacity: 0,
			}, 400, function() {
				$(this).hide();
			});

			setTimeout(function(){
				if (hashTag) { // Is there any Hash tag form share ?
					welcomeAnimation();
					showBackground();
					goToArticle(hashTag);

				} else if (section) { // Is there any url for sections ?
					welcomeAnimation();
					showBackground(); // Hide the background (blur)
					goToPage(section);

				} else { // If no hash tag or url, then show the normal site
					
					welcomeAnimation();
					$('.background.blured').removeClass('blured'); // Remove the class for the blur effect

					// If doesn't support blur effect, use the image fallback
					if (!blurSupport) {
						$('.background .blur-img').fadeOut(400);
					}

					introDone = true; // Declare intro is done
				}
			}, 4000);
		}, 3000);
	});


	/* ******************************************************
	   If url have Hash tag or Holiday Tips article url is called
	****************************************************** */
	function goToArticle(articleUrl, backUrl) { // Show the shared article

		hideCurrentPage() // Hide current open page

		$('#'+holidaysTipsUrl).addClass('active'); // Show page Holiday Tips

		$(articleUrl).addClass('active').fadeIn( 400, function() { // Show the category detail and add class active
			var recipeOrActivity = $(this).attr('data-category-qa'),
				recipeOrActivityName = $(this).attr('data-url-ga');

			utag.view({URI:'/holiday-planner/holiday-tips/'+recipeOrActivity+'/'+recipeOrActivityName+'/'});
		});

		$('#'+holidaysTipsUrl+' .articles').hide(); // Fade the category buttons

		showBackButtonNav(); // Show back button and hide close button

		backToTop(0,200); // Animation back to top

		hideBackground(); // Hide the background (blur)

		/* ******************************************************
		   Holidays Tips - Close article
		****************************************************** */
		$(document).on(clickORtouchend, '.close.back', function (e) {
			if (backUrl) {
				e.preventDefault();

				hideCurrentPage() // Hide current open page

				$('#'+holidaysTipsUrl+' .articles-items .active').removeClass('active').fadeOut(400); // Hide the current article

				$('#'+backUrl).addClass('active').fadeIn(400);  // Show the last section before article
				$('#'+holidaysTipsUrl+' .articles').show(); // Fade the category buttons

				hideBackButtonNav(); // Hide back button and show close button
				
				backToTop(0,200); // Animation back to top

				backUrl = "";
			} else {
				hideBackButtonNav(); // Hide back button and show close button
				
				$('#'+holidaysTipsUrl+' .articles-items article.active').fadeOut(400); // Reset the layout and hide the category detail
				$('#'+holidaysTipsUrl+' .articles').fadeIn(400); // Show the category buttons

				backToTop(0,200); // Animation back to top
			}
		});
	}


	/* ******************************************************
	   If url any url (not hash tag)
	****************************************************** */
	function goToPage(pageUrl) { // Show the section from url
		$('.page.active').removeClass('active');

		$('#'+pageUrl).addClass('active'); // Show page Holiday Tips

		backToTop(0,200); // Animation back to top

		hideBackground(); // Hide the background (blur)

		hideBackButtonNav(); // Hide back button and show close button
	}


	/* ******************************************************
	   Start welcome animation
	****************************************************** */
	function welcomeAnimation() { // Show the section from url
		$('.home').removeClass('introduction-active');
		$('.welcome p').addClass('hide');

		$('#points-listing').addClass('animate');

		setTimeout(function(){
			$('#points-listing').removeClass('animate');
		}, 4000);

		setTimeout(function(){
			startAnimatePointsListing();
		}, 6000);
	}


	/* ******************************************************
	   Hide / Show background
	****************************************************** */
	function hideBackground() {
		$('.welcome, .site-footer, #points-listing').css('opacity', '0'); // Hide the main logo
		$('.background').addClass('blured'); // Blur the background

		// If doesn't support blur effect, use the image fallback
		if (!blurSupport) {
			$('.background .blur-img').fadeIn(400);
		}
	}

	function showBackground() {
		$('.welcome, .site-footer, #points-listing').css('opacity', '1'); // Show the main logo
		$('.background').removeClass('blured'); // Unblur the background

		// If doesn't support blur effect, use the image fallback
		if (!blurSupport) {
			$('.background .blur-img').fadeOut(400);
		}
	}


	/* ******************************************************
	   Hide / Show navigation button
	****************************************************** */
	function hideBackButtonNav() {
		$('.close.all').fadeIn(200); // Hide the chose button
		$('.close.back').fadeOut(200); // Show the back button
	}

	function showBackButtonNav() {
		$('.close.all').fadeOut(200); // Hide the chose button
		$('.close.back').fadeIn(200); // Show the back button
	}

	function showCloseButtonNav() {
		$('.close.all').fadeIn(200); // Hide the chose button
	}


	/* ******************************************************
	   Hide current page
	****************************************************** */
	function hideCurrentPage() {
		$('.page.active').removeClass('active'); // Hide all active page, no exception
	}


	/* ******************************************************
	   Animate back to top
	****************************************************** */
	function backToTop(value, speed) {
		$('html, body, .page').animate({
			scrollTop: value
		}, speed);
	}


	/* ******************************************************
	   Trigger animation
	****************************************************** */
	function triggerAnimate(pageId) {

		$(pageId+' .animated').each(function() {
			var animation = $(this).attr('data-animation');
			$(this).addClass(animation+' animation-done');
		});

		$(pageId+' h2.main-title').addClass('animate-svg animation-done');
	}


	/* ******************************************************
	   Holidays Tips - Go to article
	****************************************************** */
	$(document).on(clickORtouchend, '.go-to-article-link', function (e) {
		e.preventDefault();

		var articleUrl = $(this).attr('href'),
			backUrl = $(this).attr('data-url-back'); // Define the article to load

		goToArticle(articleUrl,backUrl);
	});


	/* ******************************************************
	   Over on background element
	****************************************************** */
	$('#points-listing li a').hover(function(e) {
		e.preventDefault();

		var elementBg = $(this).attr('href');

		$(elementBg+'-bg').stop( true, true ).animate({
			opacity: 1,
		}, 400, function() {
			// Animation complete.
		});
	}, function() {
		var elementBg = $(this).attr('href');

		$(elementBg+'-bg').stop( true, true ).animate({
			opacity: 0,
		}, 400, function() {
			// Animation complete.
		});
	});


	/* ******************************************************
	   Random show point on map
	****************************************************** */
	var startAnimPoints = null,
		stopAnimPoints = null,
		stopForever = null;

	// Start time to animate 
	function startAnimatePointsListing() {
		if (!stopForever){
			$('#points-listing').addClass('wave-animate');

			window.clearInterval(startAnimPoints);

			stopAnimPoints = setInterval(stopAnimatePointsListing, 4000);
		}
	}

	// Remove animation and reset the timer
	function stopAnimatePointsListing() {
		if (!stopForever){
			window.clearInterval(stopAnimPoints);
			$('#points-listing').removeClass('wave-animate');

			startAnimPoints = setInterval(startAnimatePointsListing, 4000);
		}
	}

	// On hover, stop the time and the animation
	$('#points-listing li a').hover(function() {
		clearInterval(startAnimPoints);
		clearInterval(stopAnimPoints);

		$('#points-listing').removeClass('wave-animate');

	}, function() {
		if (!stopForever){
			startAnimPoints = setInterval(startAnimatePointsListing, 3000);
		}
	});


	/* ******************************************************
	   Open lightbox
	****************************************************** */
	$(document).on(clickORtouchend, '#points-listing li a', function (e) {
		e.preventDefault();

		var content = $(this).attr('href') // Define the page to load

		$(content).addClass('active'); // Show the page (lightbox)

		if (content == '#'+partyPlannerUrl) {
			if (!animationPartyPlanner) {
				triggerAnimate(content); // Trigger all animation in the page

				animationPartyPlanner = true;
			}
		} else if (content == '#'+giftGuideUrl) {
			if (!animationGiftGuide) {
				triggerAnimate(content); // Trigger all animation in the page

				animationGiftGuide = true;
			}
		} else if (content == '#'+holidaysTipsUrl) {
			if (!animationHolidayTips) {
				triggerAnimate(content); // Trigger all animation in the page

				animationHolidayTips = true;
			}
		} else if (content == '#'+flavoursUrl) {
			if (!animationFlavoursSeason) {
				triggerAnimate(content); // Trigger all animation in the page

				animationFlavoursSeason = true;
			}
		}

		showCloseButtonNav(); // Show close button

		backToTop(0,0); // Animation back to top

		hideBackground(); // Hide the background (blur)

		$('#points-listing, .visual-element').removeClass('animate'); // Remove the animate to make sure the home doesn't animate in double
		$('.welcome').addClass('faster');

		// Stop animation of point on home
		stopForever = true;
		clearInterval(startAnimPoints);
		clearInterval(stopAnimPoints);
		$('#points-listing').removeClass('wave-animate');
	});


	/* ******************************************************
	   Close lightbox
	****************************************************** */
	$(document).on(clickORtouchend, '.close.all', function (e) {
		e.preventDefault();

		hideCurrentPage() // Hide current open page

		showBackground(); // Show the background (blur)
	});

	$(document).on(clickORtouchend, '.close.all, .close.back', function (e) {
		$('.animated.animation-done').each(function() {
			var animation = $(this).attr('data-animation');
			$(this).removeClass('animated animation-done '+animation);
		});
	});


	/* ******************************************************
	   Gift Guide - Open category
	****************************************************** */
	$(document).on(clickORtouchend, '.trigger-gift-category', function (e) {
		e.preventDefault();

		var giftGuideCategoryActive = $(this).attr('href'), // Variable to get the clicked package
			giftGuideCategory = $(this).attr('data-category'), // For GA
			currentItemPositon = $(this).closest('li').position().top; // Variable to identify where the element was vertical placed

		showBackButtonNav(); // Show back button and hide close button

		$(giftGuideCategoryActive).addClass('active').fadeIn( 400, function() { // Show the category detail and add class active
			utag.view({URI:'/holiday-planner/gift-guide/'+giftGuideCategory+'/'});
		});

		$('#'+giftGuideUrl+' .categories').fadeOut(400); // Fade the category buttons

		backToTop(0,200); // Animation back to top


		/* ******************************************************
		   Gift Guide - Close category
		****************************************************** */
		$(document).on(clickORtouchend, '#'+giftGuideUrl+' .close.back', function (e) {
			e.preventDefault();

			hideBackButtonNav(); // Hide back button and show close button
			
			$('#'+giftGuideUrl+' .categories-items .active').fadeOut(400); // Reset the layout and hide the category detail
			$('#'+giftGuideUrl+' .categories').fadeIn(400); // Show the category buttons

			backToTop(0,200); // Animation back to top
		});
	});


	/* ******************************************************
	   Holidays Tips - Open article
	****************************************************** */
	$(document).on(clickORtouchend, '.trigger-holiday-article', function (e) {
		e.preventDefault();

		var holidayArticleActive = $(this).attr('href'), // Variable to get the clicked article
			currentItemPositon = $(this).closest('li').position().top; // Variable to identify where the element was vertical placed

		showBackButtonNav(); // Show back button and hide close button

		$(holidayArticleActive).addClass('active').fadeIn( 400, function() { // Show the category detail and add class active
			var recipeOrActivity = $(this).attr('data-category-qa'),
				recipeOrActivityName = $(this).attr('data-url-ga');

			utag.view({URI:'/holiday-planner/holiday-tips/'+recipeOrActivity+'/'+recipeOrActivityName+'/'});
		});

		$('#'+holidaysTipsUrl+' .articles').fadeOut(400); // Fade the category buttons

		backToTop(0,200); // Animation back to top


		/* ******************************************************
		   Holidays Tips - Close article
		****************************************************** */
		$(document).on(clickORtouchend, '#'+holidaysTipsUrl+' .close.back', function (e) {
			e.preventDefault();

			hideBackButtonNav(); // Hide back button and show close button
			
			$('#'+holidaysTipsUrl+' .articles-items article.active').fadeOut(400); // Reset the layout and hide the category detail
			$('#'+holidaysTipsUrl+' .articles').fadeIn(400); // Show the category buttons

			backToTop(0,200); // Animation back to top
		});
	});


	/* ******************************************************
	   Party Planenr - Reset
	****************************************************** */
	$(document).on(clickORtouchend, 'a[href="#'+partyPlannerUrl+'"]', function (e) {
		e.preventDefault();

		hideBackButtonNav(); // Hide back button and show close button
		
		$('#'+partyPlannerUrl+' #pp-results').hide(); // Hide open result
		$('#'+partyPlannerUrl+' #pp-questions').show(); // Show fresh page

		backToTop(0,200); // Animation back to top
	});


	/* ******************************************************
	   Party Planner - Open suggestions
	****************************************************** */
	$(document).on(clickORtouchend, '#submitPartyPlanner', function (e) {
		e.preventDefault();

		showBackButtonNav(); // Show back button and hide close button

		$('#'+partyPlannerUrl+' #pp-questions').fadeOut(400); // Fade the category buttons
		$('#'+partyPlannerUrl+' #pp-results').fadeIn(400); // Fade the category buttons

		backToTop(0,200); // Animation back to top


		/* ******************************************************
		   Party Planner - Close results
		****************************************************** */
		$(document).on(clickORtouchend, '#'+partyPlannerUrl+' .close.back', function (e) {
			e.preventDefault();

			hideBackButtonNav(); // Hide back button and show close button
			
			$('#'+partyPlannerUrl+' #pp-questions').fadeIn(400); // Fade the category buttons
			$('#'+partyPlannerUrl+' #pp-results').fadeOut(400); // Fade the category buttons

			backToTop(0,0); // Animation back to top
		});
	});


	/* ******************************************************
	   In case of reload, reset all data from Party Planner
	****************************************************** */
	$('#partyPlannerForm input[type="radio"]').prop('checked', false);


	/* ******************************************************
	   Click on label on party planner
	****************************************************** */
	$(document).on(clickORtouchend, '#partyPlannerForm label', function (e) {
		e.preventDefault();
		var forId = $(this).attr("for");

		$(this).closest('ul').find('label.active').removeClass('active');
		$(this).addClass('active');
		$('#'+forId).click();
	});


	/* ******************************************************
	   Show Party Planner step by step
	****************************************************** */
	$(document).on(clickORtouchend, '#partyPlannerForm .event label', function (e) {
		e.preventDefault();

		if (!eventClicked) {
			$('#partyPlannerForm .guest').slideDown(400);

			$('html, body, .page').animate({
				scrollTop: $('#partyPlannerForm .guest').position().top
			}, 400);
		}
		
		eventClicked = true;
	});

	$(document).on(clickORtouchend, '#partyPlannerForm .guest label', function (e) {
		e.preventDefault();

		if (!guestClicked) {
			$('#partyPlannerForm .mood').slideDown(400);

			$('html, body, .page').animate({
				scrollTop: $('#partyPlannerForm .mood').position().top
			}, 400);
		}
		
		guestClicked = true;
	});

	$(document).on(clickORtouchend, '#partyPlannerForm .mood label', function (e) {
		e.preventDefault();

		if (!moodClicked) {
			$('#partyPlannerForm .wrapper-submit').slideDown(400);

			$('html, body, .page').animate({
				scrollTop: $('#partyPlannerForm .wrapper-submit').position().top
			}, 400);
		}
		
		moodClicked = true;
	});
	
});


