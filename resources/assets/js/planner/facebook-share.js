$(document).on(clickORtouchend, '.fb-feed', function (event) {
    event.preventDefault();
    fbShare.postFeed($(this).data('feed'));
});

function FacebookShare (appID)
{
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
          appId: appID,
          version: 'v2.3' // or v2.0, v2.1, v2.0
        });

        FB.getLoginStatus(updateStatusCallback);
    });

    function updateStatusCallback()
    {
        //
    }

    this.postFeed = function(data)
    {
        FB.ui(
         {
          method: 'feed',
          link: data.href,
          picture: data.picture,
          name: data.name,
          caption: data.caption,
          description: data.description
        }, function(response){});
    }
}