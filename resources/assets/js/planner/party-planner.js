

var answers = [false,false,false];

var results = {
	brewer: null,
	coffee: null,
	accessory: null,
	recipe: null,
	activity: null,
	message: null
};

var answersTitle = {
	kind: null,
	guest: null,
	mood: null
};

/*
   if ( answer.attr('data-brewer') )
   {
	   results.brewer = answer.attr('data-brewer');
   }
*/

$('#submitPartyPlanner').on(clickORtouchend, function(e) {
	e.preventDefault();

	var answers = [
		$('#partyPlannerForm [name=event]:checked').val(),
		$('#partyPlannerForm [name=guest]:checked').val(),
		$('#partyPlannerForm [name=mood]:checked').val()
	];

	var answersTitle = {
		kind: $('#partyPlannerForm [name=event]:checked').attr('data-url-ga'),
		guest: $('#partyPlannerForm [name=guest]:checked').attr('data-url-ga'),
		mood :$('#partyPlannerForm [name=mood]:checked').attr('data-url-ga')
	};

	utag.view({URI:'/holiday-planner/party-planner/'+answersTitle.kind+'/'+answersTitle.guest+'/'+answersTitle.mood});

	// Holiday, 5, heartwarming
	if (answers == "1,1,1") {
		var results = {
			brewer: '1',
			coffee: '1',
			accessory: '1',
			recipe: '1',
			activity: '1',
			message: '1'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 5, Exentric
	if (answers == "1,1,2") {
		var results = {
			brewer: '1',
			coffee: '1',
			accessory: '2',
			recipe: '1',
			activity: '2',
			message: '2'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 5, Elegant
	if (answers == "1,1,3") {
		var results = {
			brewer: '1',
			coffee: '1',
			accessory: '3',
			recipe: '1',
			activity: '3',
			message: '3'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 10, Hearthwarming
	if (answers == "1,2,1") {
		var results = {
			brewer: '2',
			coffee: '2',
			accessory: '4',
			recipe: '1',
			activity: '1',
			message: '4'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 10, Eccentric
	if (answers == "1,2,2") {
		var results = {
			brewer: '2',
			coffee: '2',
			accessory: '4',
			recipe: '1',
			activity: '2',
			message: '5'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 10, Elegant
	if (answers == "1,2,3") {
		var results = {
			brewer: '2',
			coffee: '2',
			accessory: '4',
			recipe: '1',
			activity: '3',
			message: '6'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 10+, Heartwarming
	if (answers == "1,3,1") {
		var results = {
			brewer: '3',
			coffee: '3',
			accessory: '4',
			recipe: '1',
			activity: '1',
			message: '7'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 10+, Eccentric
	if (answers == "1,3,2") {
		var results = {
			brewer: '3',
			coffee: '3',
			accessory: '4',
			recipe: '1',
			activity: '2',
			message: '8'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Holiday, 10+, Elegant
	if (answers == "1,3,3") {
		var results = {
			brewer: '3',
			coffee: '3',
			accessory: '4',
			recipe: '1',
			activity: '3',
			message: '9'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 5, Heartwarming
	if (answers == "2,1,1") {
		var results = {
			brewer: '1',
			coffee: '1',
			accessory: '1',
			recipe: '2',
			activity: '1',
			message: '10'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 5, Eccentric
	if (answers == "2,1,2") {
		var results = {
			brewer: '1',
			coffee: '1',
			accessory: '2',
			recipe: '2',
			activity: '2',
			message: '11'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 5, Elegant
	if (answers == "2,1,3") {
		var results = {
			brewer: '1',
			coffee: '1',
			accessory: '3',
			recipe: '2',
			activity: '3',
			message: '12'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 10, Heartwarming
	if (answers == "2,2,1") {
		var results = {
			brewer: '2',
			coffee: '2',
			accessory: '4',
			recipe: '2',
			activity: '1',
			message: '13'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 10, Eccentric
	if (answers == "2,2,2") {
		var results = {
			brewer: '2',
			coffee: '2',
			accessory: '4',
			recipe: '2',
			activity: '2',
			message: '14'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 10, Elegant
	if (answers == "2,2,3") {
		var results = {
			brewer: '2',
			coffee: '2',
			accessory: '4',
			recipe: '2',
			activity: '3',
			message: '15'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 10+, Heartwarming
	if (answers == "2,3,1") {
		var results = {
			brewer: '3',
			coffee: '3',
			accessory: '4',
			recipe: '2',
			activity: '1',
			message: '16'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 10+, Eccentric
	if (answers == "2,3,2") {
		var results = {
			brewer: '3',
			coffee: '3',
			accessory: '4',
			recipe: '2',
			activity: '2',
			message: '17'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cosy brunch, 10+, Elegant
	if (answers == "2,3,3") {
		var results = {
			brewer: '3',
			coffee: '3',
			accessory: '4',
			recipe: '2',
			activity: '3',
			message: '18'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 5, Heartwarming
	if (answers == "3,1,1") {
		var results = {
			brewer: '4',
			coffee: '4',
			accessory: '5',
			recipe: '3',
			activity: '1',
			message: '19'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 5, Eccentric
	if (answers == "3,1,2") {
		var results = {
			brewer: '4',
			coffee: '4',
			accessory: '5',
			recipe: '3',
			activity: '2',
			message: '20'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 5, Elegant
	if (answers == "3,1,3") {
		var results = {
			brewer: '4',
			coffee: '4',
			accessory: '5',
			recipe: '3',
			activity: '3',
			message: '21'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 10, Heartwarming
	if (answers == "3,2,1") {
		var results = {
			brewer: '4',
			coffee: '5',
			accessory: '6',
			recipe: '3',
			activity: '1',
			message: '19'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 10, Eccentric
	if (answers == "3,2,2") {
		var results = {
			brewer: '4',
			coffee: '5',
			accessory: '6',
			recipe: '3',
			activity: '2',
			message: '20'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 10, Elegant
	if (answers == "3,2,3") {
		var results = {
			brewer: '4',
			coffee: '5',
			accessory: '6',
			recipe: '3',
			activity: '3',
			message: '21'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 10+, Heartwarming
	if (answers == "3,3,1") {
		var results = {
			brewer: '4',
			coffee: '6',
			accessory: '7',
			recipe: '3',
			activity: '1',
			message: '19'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 10+, Eccentric
	if (answers == "3,3,2") {
		var results = {
			brewer: '4',
			coffee: '6',
			accessory: '7',
			recipe: '3',
			activity: '2',
			message: '20'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};

	// Cocktail, 10+, Elegant
	if (answers == "3,3,3") {
		var results = {
			brewer: '4',
			coffee: '6',
			accessory: '7',
			recipe: '3',
			activity: '3',
			message: '21'
		};

		placePartyPlannerContent(results.brewer, results.coffee, results.accessory, results.recipe, results.activity, results.message);
	};
});


function placePartyPlannerContent(brewer, coffee, accessory, recipe, activity, message) {
	$('.placeholder').empty();

	if (message) {
		var messageID = $('#pp-hidden .message-'+message).html();
		$('.placeholder.message').append(messageID);
	}

	if (brewer) {
		var brewerID = $('#pp-hidden .brewer-'+brewer).html();
		$('.placeholder.brewer').append(brewerID);
	}

	if (coffee) {
		var coffeeID = $('#pp-hidden .coffee-'+coffee).html();
		$('.placeholder.coffee').append(coffeeID);
	}

	if (accessory) {
		var accessoryID = $('#pp-hidden .accessory-'+accessory).html();
		$('.placeholder.accessory').append(accessoryID);
	}

	if (recipe) {
		var recipeID = $('#pp-hidden .recipe-'+recipe).html();
		$('.placeholder.recipe').append(recipeID);
	}

	if (activity) {
		var activityID = $('#pp-hidden .activity-'+activity).html();
		$('.placeholder.activity').append(activityID);
	}
}
