
$(function(){

	// Click on Flavours of the seasons
	$(document).on(clickORtouchend, "#points-listing .flavours-season a", function () {
		utag.view({URI:'/holiday-planner/flavours-of-the-season/'});
	});

	// Click on Holiday Tips
	$(document).on(clickORtouchend, "#points-listing .holidays-tips a", function () {
		utag.view({URI:'/holiday-planner/holiday-tips/'});
	});

	// Click on Party planner
	$(document).on(clickORtouchend, "#points-listing .party-planner a", function () {
		utag.view({URI:'/holiday-planner/party-planner/'});
	});

	// Click on Gift guide
	$(document).on(clickORtouchend, "#points-listing .gift-guide a", function () {
		utag.view({URI:'/holiday-planner/gift-guide/'});
	});

	// On close back
	$(document).on(clickORtouchend, ".close.back", function () {
		utag.link({ga_category:'holiday-planner', ga_actions: 'back', ga_label: utag_data.URI});
	});

	// On close all
	$(document).on(clickORtouchend, ".close.all", function () {
		utag.link({ga_category:'holiday-planner', ga_actions: 'x-buton', ga_label: utag_data.URI});
	});

	// Party Planner / Flavours external link
	$(document).on(clickORtouchend, ".flavours-season a.btn, .flavours-season .wrapper-featured img, .flavours-season .wrapper-suggestions img", function () {
		var nameProductEnglish = $(this).attr('data-url-ga'); // Get product name
		utag.link({ga_category:'holiday-planner', ga_actions: nameProductEnglish, ga_label: 'shop'});
	});

	// Gift Guide
	$(document).on(clickORtouchend, ".gift-guide .categories-items article a.btn", function () {
		var nameBundleEnglish = $(this).attr('data-url-ga'); // Get bundle name
		utag.link({ga_category:'holiday-planner', ga_actions: nameBundleEnglish, ga_label: 'shop'});
	});

	// Click on share
	$(document).on(clickORtouchend, ".share a", function () {
		var shareType = $(this).attr('data-type-ga'); // Get data if social network OR email
		utag.link({ga_category:'holiday-planner', ga_actions: 'share', ga_label: shareType});
	});

	// Click on home share
	$(document).on(clickORtouchend, "#site-footer .share-button a", function () {
		utag.link({ga_category:'holiday-planner', ga_actions: 'home-share', ga_label: 'home-share'});
	});

	// Click generic outside link to Keurig
	$(document).on(clickORtouchend, ".tracking-ga", function () {
		var nameLinkEnglish = $(this).attr('data-url-ga'); // Get data of external link
		utag.link({ga_category:'holiday-planner', ga_actions: nameLinkEnglish, ga_label: 'navigation'});
	});
});