//Functions impression & actions GA
/*
* parameters for product must be arrays
*
*/

window.productImpression = function (product_id, product_name, product_brand, product_category, product_variation, product_unit_price, gaPPos, product_list, ga_category, ga_actions, ga_label) {

    utag.link({
        'gaImpPID'      : product_id,
        'gaImpPName'    : product_name,
        'gaImpPBrand'   : product_brand,
        'gaImpPCat'     : product_category,
        'gaImpPVariant' : product_variation,
        'gaImpPPrice'   : product_unit_price,
        'gaImpPPos'     : gaPPos,
        'gaImpPList'    : product_list,
    });

    utag.link({
            'ga_category'       : ga_category, //not arrays
            'ga_actions'        : ga_actions, //not arrays
            'ga_label'          : ga_label, //not arrays
            'ga_nonInteraction' : 1,  //not arrays
        });

    utag.link({
        'ga_nonInteraction' : 0
    });

};
/*
* parameters for product must be arrays
*
*/
window.productAction = function (product_id, product_name, product_brand, product_category, product_variation, product_unit_price, product_qty, gaPPos, product_list, enhancedEcommerceAction, ga_category, ga_actions, ga_label) {

    utag.link({
        'product_id'              : product_id,
        'product_name'            : product_name,
        'product_brand'           : product_brand,
        'product_category'        : product_category,
        'product_variation'       : product_variation,
        'product_unit_price'      : product_unit_price,
        'product_qty'             : product_qty,
        'gaPPos'                  : gaPPos,
        'product_list'            : product_list,
        'enhancedEcommerceAction' : enhancedEcommerceAction,
    });

    utag.link({
            'ga_category' : ga_category, //not arrays
            'ga_actions'  : ga_actions, //not arrays
            'ga_label'    : ga_label, //not arrays
        });

};

