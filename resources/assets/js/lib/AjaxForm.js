/**
 *
 * AjaxForm Class.
 *
 * @version 0.1.0
 *
 * @param string
 * @param Object options (optional)
 *
 */
function AjaxForm ( HtmlFormElementId )
{
  /**
   *
   * Master shortcut.
   *
   * @var Object
   *
   */
  var $this = this

  /**
   *
   * Shortcut to form element.
   *
   * @var Object
   *
   */
  $this.formElement = null

  /**
   *
   * Form action.
   *
   * @var string
   *
   */
  $this.formAction = null

  /**
   *
   * Object options.
   *
   * @var Object
   *
   */
  $this.optns = null


  /**
   *
   * Object constructor.
   *
   * @param Object args
   *
   */
  this.construct = function ( args )
  {
    // set object properties
    $this.formElement = $('#'+HtmlFormElementId)
    $this.formAction  = $this.formElement.attr('action')
    $this.optns       = $.extend( $this.getDefaultOptions(), args[1] || {} )

    // set event handlers
    $this.formElement.on( 'submit', $this.onSubmitHandler )
  }


  /**
   *
   * Handles form.submit event.
   *
   * @param Object event
   *
   */
  this.onSubmitHandler = function (e)
  {
    e.preventDefault()

    $this.formElement
      .removeClass('fail done progress')
      .addClass('progress')


    var request = $.post
    (
      $this.formAction,
      $this.formElement.serialize()
    );

    request
      .always( function(response)
      {
        $this.formElement.removeClass('progress')
        $this.optns.always( response, $this )
      })
      .done( function(response)
      {
        $this.formElement.addClass('done')
        $this.optns.done( response, $this )
      })
      .fail( function(response)
      {
        $this.formElement.addClass('fail')
        $this.optns.fail( response, $this )
      })
  }

  /**
   *
   * Gets default, mandatory options.
   *
   * @return Object
   *
   */
  this.getDefaultOptions = function ()
  {
    var r =
    {
      // Called on success
      done : function ( response, ajaxform ) {},

      // Called on failure
      fail : function ( response, ajaxform ) {},

      // Called on success + failure
      always : function ( response, ajaxform ) {}
    }

    return r
  }

  $this.construct(arguments)
}
