 $(function() {

 	var colortags = {
 		K200:[
			[['50-22420'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['Violet'],     ['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - Violet'],
			[['50-22417'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['Sandy Pearl'],['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - Sandy Pearl'],
			[['50-22422'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['white'],      ['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - white'],
			[['50-22421'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['black'],      ['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - black'],
			[['50-22418'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['Turquoise'],  ['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - Turquoise'],
			[['50-22423'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['Strawberry'], ['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - Strawberry'],
			[['50-22419'],['Keurig 2.0 K200 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['Orange Zest'],['119.99'],['1'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K200 Brewing System - Orange Zest']
 		],
 		K400:[
			[['50-22430'],['Keurig 2.0 K400 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['white'],      ['179.99'],['3'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K400 Brewing System - white'],
			[['50-62025'],['Keurig 2.0 K400 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['black'],      ['179.99'],['3'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K400 Brewing System - black'],
			[['50-22431'],['Keurig 2.0 K400 Brewing System'],['Keurig'],['brewers/k2/home/brewer/brewer'],['vintage red'],['179.99'],['3'],['caribou'],'caribou_landingpage','color-swich','Keurig 2.0 K400 Brewing System - vintage red']
 		]
 	}
 	var coloraction = {
 		K200:[
			[['50-22420'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['Violet'],      ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - Violet'],
			[['50-22417'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['Sandy Pearl'], ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - Sandy Pearl'],
			[['50-22422'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['white'],       ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - white'],
			[['50-22421'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['black'],       ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - black'],
			[['50-22418'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['Turquoise'],   ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - Turquoise'],
			[['50-22423'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['Strawberry'],  ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - Strawberry'],
			[['50-22419'], ['Keurig 2.0 K200 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['Orange Zest'], ['119.99'], ['1'], ['1'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K200 Brewing System - Orange Zest']
			],
		K300:[
			['50-62024'], ['Keurig 2.0 K300 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['black'], ['149.99'], ['1'], ['2'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K300 Brewing System - black'
		],
 		K400:[
			[['50-22430'], ['Keurig 2.0 K400 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['white'],       ['179.99'], ['1'], ['3'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K400 Brewing System - white'],
			[['50-62025'], ['Keurig 2.0 K400 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['black'],       ['179.99'], ['1'], ['3'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K400 Brewing System - black'],
			[['50-22431'], ['Keurig 2.0 K400 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['vintage red'], ['179.99'], ['1'], ['3'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K400 Brewing System - vintage red']
		],
		K500:[
			['50-62027'], ['Keurig 2.0 K500 Brewing System'], ['Keurig'], ['brewers/k2/home/brewer/brewer'], ['black'], ['199.99'], ['1'], ['4'], ['caribou'], ['add'], 'caribou_landingpage', 'add-to-cart', 'Keurig 2.0 K500 Brewing System - black'
		]
 	}
 	var colorlinks = {
 		K200:[
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Violet/p/Keurig-2-0-k200-brewer-violet'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Sandy-Pearl/p/Keurig-2-0-k200-brewer-sandy-pearl'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-White/p/Keurig-2-0-k200-brewer-white'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Black/p/Keurig-2-0-k200-brewer-black'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Turquoise/p/Keurig-2-0-k200-brewer-turquoise'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Strawberry/p/Keurig-2-0-k200-brewer-strawberry'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K200-Brewing-System-Orange-Zest/p/Keurig-2-0-k200-brewer-orange']
		],
 		K400:[
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K400-Brewing-System-White/p/Keurig-2-0-K400-Brewing-System-White'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K400-Brewing-System/p/Keurig-2-0-K400-Brewing-System'],
			['http://www.keurig.ca/Brewers/Keurig%C2%AE-2-0-K400-Brewing-System-Vintage-Red/p/Keurig-2-0-K400-Brewing-System-Vintage-Red']
		],
 	}




 //Colorpicker
 	$('.offer.offer1 .products .product.K200 a.btn').on('click', function(){
		productImpression.apply(null, coloraction.K200[6]);
 	});
 	$('.offer.offer1 .products .product.K300 a.btn').on('click', function(){
 		productImpression.apply(null, coloraction.K300);
 	});
 	$('.offer.offer2 .products .product.K400 a.btn').on('click', function(){
		productImpression.apply(null, coloraction.K400[2]);
 	});
 	$('.offer.offer2 .products .product.K500 a.btn').on('click', function(){
		productImpression.apply(null, coloraction.K500);
 	});
    $('.product .colorSelect span').on('click', function(){
        var brewer = $(this).parent().attr('data-brewer');
        var indexColor = $(this).index();
        var slider = $(this).parent().siblings('.brewerImgContainer');
        slider.find('.activeBrewer').removeClass('activeBrewer');
        slider.find('img').eq(indexColor).addClass('activeBrewer');
        productImpression.apply(null, colortags[brewer][indexColor]);

        //Change link
		$(this).parent().next('a.btn').attr('href', colorlinks[brewer][indexColor]);

        //Product Action
        $('.offer.offer1 .products .product.'+brewer+' a.btn').off('click');
        $('.offer.offer1 .products .product.'+brewer+' a.btn').on('click', function(){
			productImpression.apply(null, coloraction[brewer][indexColor]);

	 	});



    });

});