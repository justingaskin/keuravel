@extends('master')

@section('content')

<div class="find-store">
	<div class="container">
		<h2> {!! Lang::get('findstore.brewing') !!}</h2>
		<p>{!! Lang::get('findstore.infos') !!}</p>
		<h3>{!! Lang::get('findstore.nationalstores') !!}</h3>
		<hr>
		<ul>
			<li><a href="{!! Lang::get('findstore.link.ares') !!}" target="_blank"><img src="{!! img('findstore/ares-').Lang::get('copy.language') !!}.png" alt="ares"></a></li>
			<li><a href="{!! Lang::get('findstore.link.aviva') !!}" target="_blank"><img src="{!! img('findstore/aviva-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.batteries') !!}" target="_blank"><img src="{!! img('findstore/batteries-included-en.png')!!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.bedbath') !!}" target="_blank"><img src="{!! img('findstore/bed-bath-beyond-en.png')!!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.staples') !!}" target="_blank"><img src="{!! img('findstore/bureau-en-gros-').Lang::get('copy.language') !!}.png" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.canadiantire') !!}" target="_blank"><img src="{!! img('findstore/canadian-tire-en.png')!!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.centrerasoir') !!}" target="_blank"><img src="{!! img('findstore/centre-rasoir-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.corbeil') !!}" target="_blank"><img src="{!! img('findstore/corbeil-').Lang::get('copy.language') !!}.png" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.despres') !!}" target="_blank"><img src="{!! img('findstore/despres-laporte-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.futureshop') !!}" target="_blank"><img src="{!! img('findstore/future-shop-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.homeoutfitters') !!}" target="_blank"><img src="{!! img('findstore/home-outfitters-').Lang::get('copy.language') !!}.png" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.hudson') !!}" target="_blank"><img src="{!! img('findstore/hudson-bay-').Lang::get('copy.language') !!}.png" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.lasoupiere') !!}" target="_blank"><img src="{!! img('findstore/soupiere-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.korvette') !!}" target="_blank"><img src="{!! img('findstore/korvette-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.linenchest') !!}" target="_blank"><img src="{!! img('findstore/linen-chest-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.londondrugs') !!}" target="_blank"><img src="{!! img('findstore/london-drugs-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.personaledge') !!}" target="_blank"><img src="{!! img('findstore/personal-edge-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.rona') !!}" target="_blank"><img src="{!! img('findstore/rona-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.sears') !!}" target="_blank"><img src="{!! img('findstore/sears-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.stokes') !!}" target="_blank"><img src="{!! img('findstore/stokes-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.vinpassion') !!}" target="_blank"><img src="{!! img('findstore/vin-passion-en.png') !!}" alt=""></a></li>
			<li><a href="{!! Lang::get('findstore.link.walmart') !!}" target="_blank"><img src="{!! img('findstore/walmart-en.png') !!}" alt=""></a></li>
		</ul>
	</div>
</div>

@stop