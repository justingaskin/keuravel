 @if(env('APP_ENV') === 'local')
 	(function(a,b,c,d){
            a='//tags.tiqcdn.com/utag/gmcr/keurig.ca/dev/utag.js';
            b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
            a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
 	})();
 @elseif (env('APP_ENV') === 'stage')

 	(function(a,b,c,d){
            a='//tags.tiqcdn.com/utag/gmcr/keurig.ca/qa/utag.js';
            b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
            a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
 	})();
 @else
 	(function(a,b,c,d){
            a='//tags.tiqcdn.com/utag/gmcr/keurig.ca/prod/utag.js';
            b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
            a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
 	})();
 @endif