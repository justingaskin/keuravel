<div class="share">
	<p>{{ Lang::get('planner.share.title') }}</p>
  
	<ul>
		<!-- https://developers.pinterest.com/docs/widgets/pin-it/ -->
		<li>
			<a data-pin-do="buttonPin" data-pin-color="white" data-pin-custom="true" href="https://www.pinterest.com/pin/create/button/"
			target="_blank" class="pinterest" data-type-ga="pinterest">Pinterest</a>
		</li>

		<li>
			<a href="#" class='icon-facebook fb-feed facebook' data-type-ga="facebook" data-feed='{ "href":"{{ url() . '/' . locale() . '/' . Lang::get('planner.site.url') . '#' . $articleID }}",
				"picture":"{{ $imgUrl }}",
				"name":"{{ $articleTitle }}",
				"caption":"{{ $articleTitle }}",
				"description":"{{ Lang::get('planner.home.welcome.meta') }}" }'
				data-trackevent="contest-result-page|click|share-on-facebook">Facebook</a>
		</li>
		
		<li><a href="mailto:?&subject={{ Lang::get('planner.share.email.title') }}&body={{ Lang::get('planner.share.email.message') . $articleTitle . '%0A' . url() . '/' . locale() . '/' . Lang::get('planner.site.url') . '#' . $articleID . Lang::get('planner.share.email.message-end') }}" class="email" data-type-ga="email">Email</a></li>
	</ul>
</div>
