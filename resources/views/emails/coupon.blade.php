<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">
  <title>{!! Lang::get('copy.title') !!}</title>
  <style>
    * { -webkit-text-size-adjust: none; }
    body { margin: 0; background: #FFFFFF; }
    table { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
    td { font-size: 15px; font-family: "Reader",Helvetica,sans-serif; color: #000000; }
    a { color: #140911; }
    p{margin: 8px 0;}
    .bloc-grey a{color: #ffffff;}
    sup {font-size: 0.6em;line-height: normal;vertical-align: top;}
    .mobile-only{display: none;}
    @font-face{
	    font-family:"Reader";
	    src:url("fonts/reader-regular.eot");
	    src:url("fonts/reader-regular.eot?#iefix") format("embedded-opentype"),
	    url("fonts/reader-regular.woff") format("woff"),
	    url("fonts/reader-regular.ttf") format("truetype"),
	    url("fonts/reader-regular.svg#Reader") format("svg");
	    font-weight:normal;
	    font-style:normal
    }
    @font-face{
      font-family:"ReaderBold";
      src:url("fonts/reader-bold.eot");
      src:url("fonts/reader-bold.eot?#iefix") format("embedded-opentype"),
      url("fonts/reader-bold.woff") format("woff"),
      url("fonts/reader-bold.ttf") format("truetype"),
      url("fonts/reader-bold.svg#Reader") format("svg");
      font-weight:bold;
      font-style:normal
    }
    @media all and (max-width:599px)
    {
      .mobile-only{display: block;}
      img { width: 100%; max-width: 100%; }
      img[class="auto"], img[class="button"] { width: auto !important; }
      table[class="full-width"] { width: 100% !important; height: auto !important; }
      table[class="half-width"] { width: 50% !important; height: auto !important; float: left; }
      td[class="block"] { display: block; }
      td[class="centered"] { text-align: center; }
      td[class="centered"] img { text-align: center; margin-left: auto; margin-right: auto; }
      td[class="nav-cell"] { width: auto; height: auto; padding-left: 0 !important; padding-right: 0 !important; }
      a[class="nav-link"] { font-size: 15px; }
    }
  </style>
</head>
<body>

  <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
    <tr>
      <td align="center" valign="top" bgcolor="#FFFFFF" style="font:15px/1.3 Helvetica, Arial, sans-serif; color:#000000;">

        <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640" bgcolor="#eeeeee">
           <!-- HEADER -->
           <tr>
            <td style="padding: 30px 0;">
                <table align="left" width="" cellspacing="0" cellpadding="0" border="0" class="full-width">
                    <tr>
                      <td width="38">
                        &nbsp;
                      </td>
                      <td class="centered" align="left" style="font-weight: bold;" valign="middle" height="32">
                           <a href="{{ url(Localize::getCurrentLocale()) }}?utm_source=utmnooverride.com&utm_medium=referral"><img src="{{ asset('images/logo-keurig.jpg') }}" alt="Keurig"  style="display: block; width:107px; height: 19px;"/></a>
                           <p class="mobile-only" style="height: 20px;"></p>
                      </td>
                      <td width="38">
                        &nbsp;
                      </td>
                    </tr>
                </table>

                <table align="right"  cellspacing="0" cellpadding="0" border="0" class="full-width">
                    <tr>
                        <td width="38">
                          &nbsp;
                        </td>
                        <td style="font-size:15px; color: #000; text-transform: uppercase; font-family: Arial; font-weight: bold; line-height: 16px;" align="center" class="centered">
                              <a style="color:#000; text-decoration: none;" href="http://www.keurig.ca/keurig2-brewers?utm_source=utmnooverride.com&utm_medium=referral" target="_blank">{!! Lang::get('mail.link.shop1') !!}</a>
                          </td>
                        <td width="50">
                          &nbsp;
                        </td>
                        <td style="font-size:15px; color: #000; text-transform: uppercase;font-family: Arial; font-weight: bold; line-height: 16px;" align="center" class="centered">
                            <a style="color:#000; text-decoration: none;" href="http://www.keurig.ca/Beverages/Coffee/c/coffee101?utm_source=utmnooverride.com&utm_medium=referral" target="_blank">{!! Lang::get('mail.link.shop2') !!}</a>
                        </td>
                        <td width="38">
                          &nbsp;
                        </td>
                    </tr>
                </table>
                <!-- /HEADER -->
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>
  <!-- BANNER -->
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
    <tr>
      <td align="center">
        <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640">
          <tr>
            <td align="center">
               <img src="{!! asset('images/mail-banner-'.Lang::get('mail.language')) !!}.jpg" alt="{{ Lang::get('mail.banner.alt') }}" border="0" style="display:block;"  >
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!-- /BANNER -->
  <!-- CONTENT -->
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
    <tr>
      <td align="center" valign="top" bgcolor="#FFFFFF" style="font:15px/1.3 Helvetica, Arial, sans-serif; color:#000000;">
          <!-- WHITE -->
           <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640">
            <tr>
              <td colspan="3" height="30">&nbsp;</td>
            </tr>
            <tr>
                <td width="64">&nbsp;</td>
                <td style="font-size:18px; color: #000;" align="left" class="centered">
                 <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
                   <tr>
                     <td align="left" width="44"><img src="{{ asset('images/mail-01.gif') }}" alt="" height="31" width="31" style="display:block; width:31px; height: 31px;"/></td>
                     <td align="left"><p style="text-align:left;">{{ Lang::get('copy.thanks.print') }}<span style="font-family: 'ReaderBold',Helvetica,sans-serif;"></span></p></td>
                   </tr>
                 </table>
                <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
                  <tr>
                    <td align="left" width="44"><img src="{{ asset('images/mail-02.gif') }}" alt="" height="31" width="31" style="display:block; width:31px; height: 31px;"/></td>
                    <td align="left"> <p style="text-align:left;">{{ Lang::get('copy.thanks.cut') }}</p></td>
                  </tr>
                </table>

                 <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
                   <tr>
                     <td height="5"></td>
                   </tr>
                   <tr>
                     <td align="center"><img src="{{ asset('images/code-sample.jpg') }}" alt="" height="123" width="175" style="display:block; width:175px; height: 123px;"/></td>
                   </tr>
                    <tr>
                     <td height="30"></td>
                   </tr>
                 </table>
                 <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
                   <tr>
                     <td align="left" width="44"><img src="{{ asset('images/mail-03.gif') }}" alt="" height="31" width="31" style="display:block; width:31px; height: 31px;"/></td>
                     <td align="left"> <p style="text-align:left;">{{ Lang::get('copy.thanks.copy') }}</p></td>
                   </tr>
                 </table>
                <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
                  <tr>
                    <td align="left" width="44"><img src="{{ asset('images/mail-04.gif') }}" alt="" height="31" width="31" style="display:block; width:31px; height: 31px;"/></td>
                    <td align="left">
                        <p style="margin-bottom: 0; text-align:left;"> {{ Lang::get('copy.thanks.send') }}</p>
                        <span style="font-size: 15px; font-family: 'ReaderBold',Helvetica,sans-serif;">{!! Lang::get('copy.thanks.address') !!}</span>
                    </td>
                  </tr>
                </table>


                </td>
                <td width="64">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" height="30">&nbsp;</td>
            </tr>

          </table>
          <!-- /WHITE -->
          <!-- GREY -->
          <table class="full-width bloc-grey" cellspacing="0" cellpadding="0" border="0" width="640" bgcolor="#444444">
            <tr>
              <td colspan="3" height="30">&nbsp;</td>
            </tr>
            <tr>
                <td width="63">&nbsp;</td>
                <td style="font-size:22px; color: #fff;" align="center" class="centered">
                 {{ Lang::get('mail.infos') }}
              </td>
              <td width="63" >&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" height="30">&nbsp;</td>
            </tr>
          </table>
          <!-- /GREY -->
          <!-- WHITE -->
           <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640">
            <tr>
              <td colspan="3" height="20">&nbsp;</td>
            </tr>
            <tr>
                <td width="130">&nbsp;</td>
                <td style="font-size:18px; color: #000;" align="center" class="centered">
                 <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
                   <tr>
                     <td height="5"></td>
                   </tr>
                   <tr>
                     <td align="center"><img src="{{ asset('images/interrogation.gif') }}" alt="" width="30" height="30" style="display:block; width:30px; height: 30px;"/></td>
                   </tr>
                    <tr>
                     <td height="5"></td>
                   </tr>
                 </table>
                 <p style="font-size: 35px; line-height:29px;">{{ Lang::get('mail.question') }}</p>
                 <p style="line-height:29px;">{{ Lang::get('mail.contact') }}</p>
                 <span style="font-size: 32px; line-height:29px; color: #feb100; font-family: 'ReaderBold',Helvetica,sans-serif;">1-800-361-5628</span>

                </td>
                <td width="130">&nbsp;</td>
            </tr>

          </table>
          <!-- /WHITE -->
      </td>
    </tr>
  </table>
  <!-- /CONTENT -->
  <!-- FOOTER -->
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%;">
    <tr>
      <td align="center" valign="top" bgcolor="#FFFFFF" style="font:15px/1.3 Helvetica, Arial, sans-serif; color:#000000;">
        <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640" >
            <tr>
                <td height="50">&nbsp;</td>
            </tr>
        </table>
        <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640" style="border-top: 1px solid #dddddd;">
            <tr>
                <td height="30">&nbsp;</td>
            </tr>
        </table>
        <table class="full-width" cellspacing="0" cellpadding="0" border="0" width="640">
            <tr>
                <td style="color: #000; font-size: 11px; line-height:13px; text-align: center; padding: 0 18px;">
                  <p style="font-family:'ReaderBold',Helvetica,sans-serif; ">{!! Lang::get('mail.legal1.line1') !!}</p>
                  <p style=" font-size: 8px; line-height:11px;">{!! Lang::get('mail.legal1.line2') !!}</p>
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td style="color: #000; font-size: 11px; line-height:13px; text-align: center; padding: 0 18px;">
                  <p style="font-family:'ReaderBold',Helvetica,sans-serif; ">{!! Lang::get('mail.legal2.line1') !!}</p>
                  <p style=" font-size: 8px; line-height:11px;">{!! Lang::get('mail.legal2.line2') !!}</p>
                </td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
        </table>

      </td>
    </tr>
  </table>
  <!-- /FOOTER -->
</body>
</html>
