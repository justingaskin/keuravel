<!DOCTYPE html>

<html lang="{{ locale() }}">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="{!! Lang::get('planner.home.welcome.meta') !!}">
		<meta name="keywords" content="">
		<meta name="author" content="LeSite">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">

		<meta property="og:title" content="Keurig - {{ Lang::get('planner.home.title') }}" />
		<meta property="og:description" content="{!! Lang::get('planner.home.welcome.meta') !!}" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="{{ url() . '/' . locale() . '/' . Lang::get('planner.site.url') }}" />
		<meta property="og:image" content="{{ img('planner/og-facebook-share-' . locale() . '.jpg') }}" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="1200" />
		<meta property="og:image:height" content="630" />

		<title>Keurig - {{ Lang::get('planner.home.title') }}</title>

		<link rel="stylesheet" href="{{ css('planner/styles') }}" type="text/css">
		<!--[if IE]><link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}"><![endif]-->
        <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
        <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">
        <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 192x192 pixels in size. -->
        <link rel="icon" href="{{ asset('images/favicon.png') }}">
		<script>
		@if(locale() === 'en')
			var utag_data = {
				"login_status" : "not logged in",
				"URI" : "/holiday-planner/",
				"page_type": "holiday-planner",
				"page_name": "holiday-planner",
				"language": "en"
			};
		@else
			var utag_data = {
				"login_status" : "not logged in",
				"URI" : "/holiday-planner/",
				"page_type": "holiday-planner",
				"page_name": "holiday-planner",
				"language": "fr"
			};
		@endif
		</script>
	</head>
	<body>

		<script>
			@include("partials.planner.gtm")
		</script>

		<div class="main-wrapper">
			@yield('content')
		</div>

		<div id="scripts">
			<script src="{{ js('planner/scripts') }}"></script>
			<script async defer data-pin-custom="true" src="//assets.pinterest.com/js/pinit.js"></script>

			<script type="text/javascript">
				// Instanciate Facebook Share Functions using GRAPH API
				var fbShare = new FacebookShare('{{ env("FACEBOOK_APP_ID") }}');
			</script>
		</div>

	</body>
</html>
