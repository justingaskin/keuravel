
@extends('master')

@section('content')

   <div id="promo-end-container">
   	
	<div id="promo-end-content">
		<h1>{!! Lang::get('copy.end.title') !!}</h1>
		<p>{!! Lang::get('copy.end.text',['url' => '/'.Localize::getCurrentLocale().'/'.Lang::get('routes.form') ] ) !!}</p>
		<a href="{!! Lang::get('copy.end.link') !!}" class="btn">{!! Lang::get('copy.end.link-text') !!}</a>
	</div>

   </div>

@stop
