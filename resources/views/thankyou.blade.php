
@extends('master')

@section('content')

   <div class="thanks">
		<div class="container">
			<div class="inside">
				<div class="text-center">
					<h1>{{ Lang::get('copy.thanks.h1') }}</h1>
					<span>{{ Lang::get('copy.thanks.receive') }}</span>
					<span class="bold">- {{ Lang::get('copy.thanks.print') }}</span>
					<span class="bold">- {{ Lang::get('copy.thanks.cut') }}</span>
					<img src="{{ img('code.jpg') }}" alt="">
					<span class="bold">- {{ Lang::get('copy.thanks.copy') }}</span>
					<span class="bold">- {{ Lang::get('copy.thanks.send') }}</span>
					<span class="bold">{!! Lang::get('copy.thanks.address') !!}</span>
					<span>{{ Lang::get('copy.thanks.infos') }}</span>
				</div> <!-- /text-center -->
			</div> <!-- /inside -->
		</div><!-- /container -->
	</div><!-- /thanks -->

    {!! Form::open([ 'url' => Localize::getCurrentLocale() . '/generate', 'id' => 'generatePdfForm' ]) !!}

        @foreach ( $data as $name => $value )

            @if ( $value )

                {!!Form::hidden( $name, $value ) !!}

            @endif

        @endforeach

    {!! Form::close() !!}

@stop
