<!DOCTYPE html>

<html lang="{{ Localize::getCurrentLocale() }}">
    <head>
        <head>
		<meta charset="UTF-8">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="LeSite">
		<link rel="stylesheet" href="{{ css('style') }}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
		<title>Keurig</title>
        <script type="text/javascript">
            var uri = document.location.pathname + document.location.search;
            var utag_data = {
                    "login_status" : "not logged in",
                    "URI" : uri,
                    "page_type": "caribou2.0",
                    "page_name": "caribou2.0",
                    "gaImpPID": ['50-22419', '50-62024', '50-22431', '50-62027'],
                    "gaImpPName": ['Keurig 2.0 K200 Brewing System', 'Keurig 2.0 K300 Brewing System', 'Keurig 2.0 K400 Brewing System - Vintage Red', 'Keurig 2.0 K500 Brewing System'],
                    "gaImpPBrand": ['Keurig', 'Keurig', 'Keurig', 'Keurig'],
                    "gaImpPCat": ['brewers/k2/home/brewer/brewer', 'brewers/k2/home/brewer/brewer', 'brewers/k2/home/brewer/brewer', 'brewers/k2/home/brewer/brewer'],
                    "gaImpPVariant": ['Orange Zest', 'black', 'vintage red', 'black'],
                    "gaImpPPrice": ['119.99', '149.99', '179.99', '199.99'],
                    "gaImpPPos": [1, 2, 3, 4],
                    "gaImpPList": ['caribou2.0', 'caribou2.0', 'caribou2.0', 'caribou2.0']
                };
            utag_data['language'] = document.querySelector('html[lang=fr]') ? 'fr' : 'en';
        </script>
    </head>
    <body>
        <script>
            @include('partials.gtm');
        </script>
    	<div id="global">
    	<div class="overlay"></div>
	    	<header>
				<div class="container">
					<a href="{{ url(Localize::getCurrentLocale()) }}" class="logo">Keurig<sup>®</sup></a>
					<a href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.form')) }}?caribou-2.0=step1-form" class="bold desktop-only purchasedinstore" >{{ Lang::get('copy.header.purchaseinstore') }}</a>
					<div class="language-selector">
						<a href="{{ Localize::getLocalizedURL(Lang::get('copy.altLocale')) }}">
                            {{ ucfirst(Localize::getSupportedLocales()[Lang::get('copy.altLocale')]['native']) }}
                        </a>
					</div> <!-- /language-selector -->
					<div class="language-selector mobile-only">
						<a href="{{ Localize::getLocalizedURL(Lang::get('copy.altLocale')) }}">
                            {{ Lang::get('copy.altLocale') }}
                        </a>
					</div> <!-- /language-selector mobile -->
					<nav id="menu-main" class="main-menu desktop-only">
						<ul>
							<li><a href="{{ url(Localize::getCurrentLocale()) }}#step-2">{{ Lang::get('copy.header.menu.link1') }}</a></li>
        					<li><a target="_blank" href="{!! asset('pdf/social_media_consumer_promo_faq_v2_finale_').''.Lang::get('copy.language') !!}.pdf">FAQ</a></li>
        					<li><a target="_blank" href="{!! asset('pdf/consumer_promo_legal_notes_sept_25_').''.Lang::get('copy.language') !!}.pdf">{{ Lang::get('copy.header.menu.link3') }}</a></li>
        					<li><a target="_blank" href="{{ Lang::get('copy.link.keurig') }}">{{ Lang::get('copy.header.menu.link4') }}</a></li>
						</ul>
					</nav>
					<nav class="main-menu mobile-only">
					<span id="btn-menu"></span>
						<ul>
							<li><a href="{{ url(Localize::getCurrentLocale()) }}#step-2">{{ Lang::get('copy.header.menu.link1') }}</a></li>
        					<li><a target="_blank" href="{!! asset('pdf/social_media_consumer_promo_faq_v2_finale_').''.Lang::get('copy.language') !!}.pdf">{{ Lang::get('copy.header.menu.link2') }}</a></li>
        					<li><a target="_blank" href="{!! asset('pdf/consumer_promo_legal_notes_sept_25_').''.Lang::get('copy.language') !!}.pdf">{{ Lang::get('copy.header.menu.link3') }}</a></li>
        					<li><a target="_blank" href="{{ Lang::get('copy.link.keurig') }}">{{ Lang::get('copy.header.menu.link4') }}</a></li>
        					<li><a class="purchasedinstore" target="_blank" href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.form')) }}">{{ Lang::get('copy.header.purchaseinstore') }}</a></li>
						</ul>
					</nav>
				</div> <!-- /container -->
			</header>

	       	@yield('content')
		</div>

		<footer class="text-center">
			<ul>
    			<lh><a target="_blank" href="{!! asset('pdf/social_media_consumer_promo_faq_v2_finale_').''.Lang::get('copy.language') !!}.pdf">FAQ</a></lh>
    			<lh><a target="_blank" href="{!! asset('pdf/consumer_promo_legal_notes_sept_25_').''.Lang::get('copy.language') !!}.pdf">{{ Lang::get('copy.footer.terms') }}</a></lh>
    			<lh>
                    <a href="{{ Lang::get('copy.footer.privacy.link') }}">
                        {{ Lang::get('copy.footer.privacy') }}
                    </a>
                </lh>
    		</ul>
			<ul>
    			<lh><a target="_mail" href="{{ Lang::get('copy.link.mail') }}">{{ Lang::get('copy.footer.contact') }}</a></lh>
    			<li>
                    <span>{{ Lang::get('copy.footer.customer') }}</span>
                </li>
    			<li>
    				<a target="_mail" href="{{ Lang::get('copy.link.mail') }}">{{ Lang::get('copy.footer.email') }}</a>
    			</li>
    		</ul>
    		<div class="copy">{{ Lang::get('copy.footer.copy') }}</div>
		</footer>

        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="{{ js('scripts.min') }}"></script>

    </body>
</html>