@extends('master')

@section('content')
	<div class="sub-header text-center">

		@if ( $countDown > 0 )
			{!! Lang::choice( 'copy.banner.dogear', $countDown, [ 'days' => $countDown ]) !!}
		@else
			{!! Lang::get('copy.banner.dogear.end') !!}
		@endif

	</div> <!-- /sub-header -->
	<div class="banner text-center">
		<div class="container">

			<div class="container-left">
				<span class="title">{!! Lang::get('copy.banner.left.title') !!}</span>
				<p>{!! Lang::get('copy.banner.left.text') !!}</p>
				<a data-scroll href="#discover" class="btn">{!! Lang::get('copy.banner.left.btn') !!}</a>
			</div>
			<div class="container-right">
				<span class="title">{!! Lang::get('copy.banner.right.title') !!}</span>
				<p>{!! Lang::get('copy.banner.right.text') !!}</p>
				<a data-scroll href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.form')) }}?caribou-2.0=step1-form" class="btn">{!! Lang::get('copy.banner.right.btn') !!}</a>
			</div>
		</div> <!-- /container -->
		<div class="copy">{!! Lang::get('copy.banner.copy') !!}</div>
	</div> <!-- /banner -->
	<div class="step" id="discover">
		<div class="container">
			<div class="circle">
				<span>{{ Lang::get('copy.step') }}</span>
				<span class="num">1</span>
			</div>
			<span class="title">{{ Lang::get('copy.step1.text') }}</span>
		</div> <!-- /container -->
	</div> <!-- /sub-banner -->
	<div class="main-content">
		<div class="container">

			<div class="offer offer1">
				<div class="infos text-center">
					<span class="deco"></span>
					<h2>{{ Lang::get('copy.offer1.title') }}</h2>
					<div class="big">
						{!! Lang::get('copy.offer1.info') !!}
					</div>
					<p>{!! Lang::get('copy.offer1.text') !!}</p>
				</div><!-- /infos -->
				<div class="products">
					<div class="product K200">
						<span class="title">K200</span>
						<span class="desc">{{ Lang::get('copy.offer1.desc1') }}</span>

						<div class="brewerImgContainer k200Img">
							<img alt="K200 Violet" id="k200-violet" src="{{ asset('images/k200-violet.png') }}" class="">
							<img alt="K200 Pearl" id="k200-pearl" src="{{ asset('images/k200-pearl.png') }}">
							<img alt="K200 White" id="k200-white" src="{{ asset('images/k200-white.png') }}">
							<img alt="K200 Black" id="k200-black" src="{{ asset('images/k200-black.png') }}">
							<img alt="K200 Turquoise" id="k200-turquoise" src="{{ asset('images/k200-turquoise.png') }}" >
							<img alt="K200 Red" id="k200-red" src="{{ asset('images/k200-red.png') }}">
							<img alt="K200 Orange" class="activeBrewer" id="k200-orange" src="{{ asset('images/k200-orange.png') }}">
						</div>
						<span class="price">$99.99</span>

						<div id="k200ColorSelect" class="colorSelect" data-brewer="K200">
							<span class="k200violet" data-brewer-id="K200-violet"> </span>
							<span class="k200pearl" data-brewer-id="K200-pearl"> </span>
							<span class="k200white" data-brewer-id="K200-white"> </span>
							<span class="k200black" data-brewer-id="K200-black"> </span>
							<span class="k200turquoise" data-brewer-id="K200-turquoise"> </span>
							<span class="k200red" data-brewer-id="K200-red"> </span>
							<span class="k200orange"  data-brewer-id="K200-orange"> </span>
						</div>
						<a target="_blank" href="{{ Lang::get('copy.link.buy.k200') }}" class="btn K200">{{ Lang::get('copy.offer.btn') }} K200</a>
						<a class="bis cta" data-brewername="K200" href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.findStore')) }}">{{ Lang::get('copy.offer.link') }}</a>
					</div> <!-- /product -->
					<div class="product K300">
						<span class="title">K300</span>
						<span class="desc">{{ Lang::get('copy.offer1.desc2') }}</span>
						<img src="{{ asset('images/k300.jpg') }}" alt="K300">
						<span class="price">$129.99</span>
						<span class="options">-</span>
						<a target="_blank" href="{{ Lang::get('copy.link.buy.k300') }}" class="btn K300">{{ Lang::get('copy.offer.btn') }} K300</a>
						<a class="bis cta" data-brewername="K300" href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.findStore')) }}">{{ Lang::get('copy.offer.link') }}</a>
					</div> <!-- /product -->
				</div><!-- /products -->
			</div> <!-- /offer -->
			<div class="offer offer2">
				<div class="infos text-center">
					<span class="deco"></span>
					<h2>{{ Lang::get('copy.offer2.title') }}</h2>
					<div class="big">
						{!! Lang::get('copy.offer2.info') !!}
					</div>
					<p>{!! Lang::get('copy.offer2.text') !!}</p>
				</div><!-- /infos -->
				<div class="products">
					<div class="product K400">
						<span class="title">K400</span>
						<span class="desc">{{ Lang::get('copy.offer2.desc1') }}</span>

						<div class="brewerImgContainer k400Img">
							<img id="k400-white" alt="K400 White" src="{{ asset('images/k400-white.png') }}">
							<img id="k400-black" alt="K400 Black" src="{{ asset('images/k400-black.png') }}">
							<img id="k400-red" class="activeBrewer" alt="K400 Red" src="{{ asset('images/k400-red.png') }}">
						</div>
						<span class="price">$149.99</span>

						<div id="k400ColorSelect" class="colorSelect" data-brewer="K400">
							<span class="k400white" data-brewer-id="K400-white"> </span>
							<span class="k400black" data-brewer-id="K400-black"> </span>
							<span class="k400red" data-brewer-id="K400-red"> </span>
						</div>
						<a target="_blank"  href="{{ Lang::get('copy.link.buy.k400') }}" class="btn K400">{{ Lang::get('copy.offer.btn') }} K400</a>
						<a class="bis cta" data-brewername="K400" href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.findStore')) }}">{{ Lang::get('copy.offer.link') }}</a>
					</div> <!-- /product -->
					<div class="product K500">
						<span class="title">K500</span>
						<span class="desc">{{ Lang::get('copy.offer2.desc2') }}</span>
						<img src="{{ asset('images/k500.jpg') }}" alt="K500">
						<span class="price">$179.99</span>
						<span class="options">-</span>
						<a target="_blank" href="{{ Lang::get('copy.link.buy.k500') }}" class="btn K500">{{ Lang::get('copy.offer.btn') }} K500</a>
						<a class="bis cta" data-brewername="K500" href="{{ url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.findStore')) }}">{{ Lang::get('copy.offer.link') }}</a>
					</div> <!-- /product -->
				</div><!-- /products -->
				<div class="compare">
					<span class="comp">{{ Lang::get('copy.compare') }}</span>
				</div> <!-- /compare -->
			</div><!-- /offer -->
		</div><!-- /container -->
	</div> <!-- /main-content -->
	<div id="step-2" class="step">
		<div class="container">
			<div class="circle">
				<span>{{ Lang::get('copy.step') }}</span>
				<span class="num">2</span>
			</div>
			<span class="title">{!! Lang::get('copy.step2.text') !!}</span>
		</div> <!-- /container -->
	</div> <!-- /step -->
	<div class="journey black">
		<div class="container">
			<div class="yellow"><span>{!! Lang::get('copy.journey1.yellow') !!}</span></div>
			<div class="block first">
				<p>{{ Lang::get('copy.journey1.block1') }}</p>
			</div>
			<div class="block two">
				<p>{{ Lang::get('copy.journey1.block2') }}</p>
			</div>
			<div class="block three last">
				<p>{!! Lang::get('copy.journey1.block3') !!}</p>
			</div>
		</div><!-- /container -->
	</div> <!-- /journey -->
	<div class="bg-grey-sep">
		<span>{{ Lang::get('copy.journey.ship') }}</span>
	</div> <!-- bg-grey -->
	<div class="journey grey">
		<div class="container">
			<div class="yellow"><span>{!! Lang::get('copy.journey2.yellow') !!}</span></div>
			<div class="block first">
				<p>{!! Lang::get('copy.journey2.block1', ['url' => url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.findStore'))]) !!}</p>
			</div>
			<div class="block two">
				<p>{!! Lang::get('copy.journey2.block2', ['url' => url(Localize::getCurrentLocale().'/'.Localize::transRoute('routes.form'))]) !!}</p>
			</div>
			<div class="block three">
				<p>{!! Lang::get('copy.journey2.block3') !!}</p>
			</div>
			<div class="block four last">
				<p>{!! Lang::get('copy.journey2.block4') !!}</p>
			</div>
		</div><!-- /container -->
	</div> <!-- /journey -->
	<div class="popup popcompare">
        <span class="close">x</span>
        <img src="{!! asset('images/compare-').''.Lang::get('copy.language') !!}.jpg" alt="">
    </div>
@stop
