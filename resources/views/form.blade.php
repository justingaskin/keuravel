@extends('master')

@section('content')

<div class="how-it-works">
    <div class="sub-banner text-center">
        <div class="container">
            <p>
                <span>{!! Lang::get('copy.formbanner.title1') !!}</span>
                <span class="uppercase">{!! Lang::get('copy.formbanner.title2') !!}</span>
            </p>
        </div>
    </div> <!-- /sub-header -->

    <div class="journey grey">
        <div class="container">
            <div class="block first">
                <p>{!! Lang::get('copy.form.journey.block1') !!}</p>
            </div>
            <div class="block two">
                <p>{!! Lang::get('copy.form.journey.block2') !!}</p>
            </div>
            <div class="block three">
                <p class="hover-box underline">{!! Lang::get('copy.form.journey.block3') !!}</p>
            </div>
            <div class="block four">
                <p>{!! Lang::get('copy.form.journey.block4') !!}</p>
            </div>
            <div class="block five last">
                <p class="hover-box underline">{!! Lang::get('copy.form.journey.block5') !!}</p>
            </div>
            <span class="tick">{!! Lang::get('copy.form.journey.ship') !!}</span>
        </div><!-- /container -->
    </div> <!-- /journey -->

    <div class="form">
        <div class="container text-center">
            {!! Form::open([ 'url' => Localize::getCurrentLocale() . '/' . Localize::transRoute('routes.submitForm'), 'id' => 'requestCouponForm' ]) !!}

                {!! Form::hidden( 'lang', Localize::getCurrentLocale() ) !!}

                <h2>{!! Lang::get('copy.form.title') !!}</h2>

                @if ( $errors->any() )

                    <div id="error-container">
                        <p>{{ Lang::get('copy.form.error') }}</p>
                    </div>

                @endif

                <div class="col pull-left">
                    <div class="field {{ $errors->has('firstname') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.firstname') }}*</span>
                            {!! Form::text( 'firstname', null, [ 'required' ] ) !!}
                        </label>
                    </div>
                    <div class="field {{ $errors->has('lastname') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.lastname') }}*</span>
                            {!! Form::text( 'lastname', null, [ 'required' ] ) !!}
                        </label>
                    </div>
                    <div class="field {{ $errors->has('email') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.email') }}*</span>
                            {!! Form::email( 'email', null, [ 'required' ] ) !!}
                        </label>
                    </div>
                    <div class="field {{ $errors->has('address_1') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.address1') }}*</span>
                            {!! Form::text( 'address_1', null, [ 'required' ] ) !!}
                        </label>
                    </div>
                    <div class="field">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.address2') }}</span>
                            {!! Form::text('address_2') !!}
                        </label>
                    </div>
                    <div class="field {{ $errors->has('city') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.city') }}*</span>
                            {!! Form::text( 'city', null, [ 'required' ] ) !!}
                        </label>
                    </div>
                    <div class="field select {{ $errors->has('province') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.province') }}*</span>
                            {!! Form::select( 'province',
                                [
                                    'AB' => 'Alberta',
                                    'BC' => Lang::get('copy.province.BC'),
                                    'MB' => 'Manitoba',
                                    'NB' => Lang::get('copy.province.NB'),
                                    'NL' => Lang::get('copy.province.NL'),
                                    'NS' => Lang::get('copy.province.NS'),
                                    'ON' => 'Ontario',
                                    'PE' => Lang::get('copy.province.PE'),
                                    'QC' => Lang::get('copy.province.QC'),
                                    'SK' => 'Saskatchewan',
                                    'NT' => Lang::get('copy.province.NT'),
                                    'NU' => 'Nunavut',
                                    'YT' => 'Yukon'
                                ],
                                null,
                                [ 'placeholder' => Lang::get('copy.form.choose'), 'required' ]
                            ) !!}
                        </label>
                    </div>
                    <div class="field {{ $errors->has('postalcode') ? 'invalid' : '' }}">
                        <label>
                        <span class="label">{{ Lang::get('copy.form.field.postalcode') }}*</span>
                            {!! Form::text('postalcode', null, [ 'required', 'maxlength' => 7 ] ) !!}
                        </label>
                    </div>
                    <div class="field {{ $errors->has('phone') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">
                                {{ Lang::get('copy.form.field.phone') }}* (XXX-XXX-XXXX)
                            </span>
                            {!! Form::text('phone', null, [ 'required', 'maxlength' => 12 ] ) !!}
                        </label>
                    </div>
                </div><!-- /col -->
                <div class="col pull-right">
                    <div class="field select {{ $errors->has('model') ? 'invalid' : '' }}">
                        <label>
                        <span class="label">{{ Lang::get('copy.form.field.model') }}*</span>
                            {!! Form::select( 'model',
                                [
                                    'K200' => Lang::get('copy.form.field.model.opt1'),
                                    'K300' => Lang::get('copy.form.field.model.opt2'),
                                    'K400' => Lang::get('copy.form.field.model.opt3'),
                                    'K500' => Lang::get('copy.form.field.model.opt4')
                                ],
                                null,
                                [ 'placeholder' => Lang::get('copy.form.choose'), 'required' ]
                            ) !!}
                        </label>
                    </div>
                    <span class="legal">{!! Lang::get('copy.form.legal.offer') !!}</span>
                    <div class="field {{ $errors->has('purchasedate') ? 'invalid' : '' }}">
                        <label>
                            <span class="label">{{ Lang::get('copy.form.field.date') }} {{ Lang::get('copy.form.field.dateformat') }}*</span>
                            {!! Form::text('purchasedate', null, [ 'required', 'maxlength' => 12 ]) !!}
                        </label>
                    </div>
                    <div class="field select {{ $errors->has('purchasedfrom') ? 'invalid' : '' }}">
                        <label>
                        <span class="label">{{ Lang::get('copy.form.field.from') }}*</span>
                            {!! Form::select( 'purchasedfrom',
                                [
                                    'WAL-MART' => 'WAL-MART',
                                    'CANADIAN TIRE' => 'CANADIAN TIRE',
                                    'BEST BUY' => 'BEST BUY',
                                    'BED BATH & BEYOND' => 'BED BATH & BEYOND',
                                    'HUDSON BAY' => Lang::get('copy.form.hudsonsbay'),
                                    'LONDON DRUGS' => 'LONDON DRUGS',
                                    'OTHER' => Lang::get('copy.form.other')
                                ],
                                null,
                                [ 'placeholder' => Lang::get('copy.form.choose'), 'required' ]
                            ) !!}
                        </label>
                        <div class="field" id="other-purchased-from">
                            <label>
                                <span class="label">{{ Lang::get('copy.form.field.specify') }}</span>
                                {!! Form::text('purchasedfromother') !!}
                            </label>
                        </div>
                    </div>
                    <span class="info">* {{ Lang::get('copy.form.required') }}</span>
                </div> <!-- /col -->
                <div class="col full pull-left">
                    <div class="fields checkbox">
                        <div class="field">
                            <label>
                                {!! Form::checkbox('contact') !!}
                                <span class="label">{!! Lang::get('copy.form.checkbox1') !!}</span>
                            </label>
                        </div>
                        <div class="field {{ $errors->has('accepted') ? 'invalid' : '' }}">
                            <label>
                                {!! Form::checkbox('accepted',1,false,['required']) !!}
                                <span class="label">{!! Lang::get('copy.form.checkbox2', ['url'=> asset('pdf/consumer_promo_legal_notes_sept_25_').Lang::get('copy.language').'.pdf']) !!} *</span>
                            </label>
                        </div>
                    </div> <!-- /fields -->
                    {!! Form::button( Lang::get('copy.form.submit'), ['class' => 'button', 'type' => 'submit']) !!}
                </div> <!-- /full -->

            {!! Form::close() !!}

            <div class="blocks">
                 <div class="block faq">
                     <h3>FAQ</h3>
                 <span>{!! Lang::get('copy.form.block1.text') !!}</span>
                 <a target="_blank" href="{!! asset('pdf/social_media_consumer_promo_faq_v2_finale_').''.Lang::get('copy.language') !!}.pdf" class="btn">{!! Lang::get('copy.form.blocks.learnmore') !!}</a>
                 </div>
                 <div class="block git">
                 <h3>{!! Lang::get('copy.form.block2.title') !!}</h3>
                 <span>{!! Lang::get('copy.form.block2.text') !!}</span>
                 <a target="_blank" href="{!! Lang::get('copy.form.block2.link') !!}" class="btn">{!! Lang::get('copy.form.blocks.phone') !!}</a>
                 </div>
             </div>
        </div> <!-- /container -->
    </div> <!-- /form -->
    <div class="popup pop1">
        <span class="close">x</span>
        <span>- {{ Lang::get('copy.popup.print') }}</span>
        <span>- {{ Lang::get('copy.popup.copy') }}</span>
        <span>- {{ Lang::get('copy.popup.cut') }}</span>
        <img src="{{ img('code.jpg') }}" alt="">
        <span>- {!! Lang::get('copy.popup.send') !!}</span>
        <span>- {{ Lang::get('copy.popup.infos') }}</span>
    </div>
    <div class="popup pop2">
        <span class="close">x</span>
        <span>{{ Lang::get('copy.thanks.value') }}</span>
        <span>{{ Lang::get('copy.thanks.box35') }}</span>
        <span>{{ Lang::get('copy.thanks.box75') }}</span>
    </div>


</div>

@stop
