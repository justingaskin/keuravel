<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="{{ css('pdf') }}">
    </head>
    <body>

      <div id="wrapper" style=" width: 100%; font-family: Arial, Helvetica, sans-serif;">

      <div id="layer-1">
        <img src="{{ img('keurig-logo.png') }}" alt="logo">
      </div>

      <div id="layer-2">
        <h1>{{ Lang::get('pdf.title') }}</h1>
        <table>
          <tr>
            <td class="l">ID</td>
            <td class="v">{{ $id }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.firstname') }}</td>
            <td class="v">{{ Request::input('firstname') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.lastname') }}</td>
            <td class="v">{{ Request::input('lastname') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.email') }}</td>
            <td class="v">{{ Request::input('email') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.address1') }}</td>
            <td class="v">{{ Request::input('address_1') }}</td>
          </tr>

          @if ( Request::input('address_2') )

            <tr>
              <td class="l">{{ Lang::get('copy.form.field.address2') }}</td>
              <td class="v">{{ Request::input('address_2') }}</td>
            </tr>

          @endif

          <tr>
            <td class="l">{{ Lang::get('copy.form.field.city') }}</td>
            <td class="v">{{ Request::input('city') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.province') }}</td>
            <td class="v">{{ Request::input('province') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.postalcode') }}</td>
            <td class="v">{{ Request::input('postalcode') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.phone') }}</td>
            <td class="v">
              {{ Request::input('phone') }}
              @if ( Request::input('ext') )
               {{ ' (' . Lang::get('Ext.') . ' ' . Request::input('ext') . ')' }}
              @endif
            </td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.model') }}</td>
            <td class="v">{{ Request::input('model') }}</td>
          </tr>
          <tr>
            <td class="l">{{ Lang::get('copy.form.field.date') }}</td>
            <td class="v">{{ Request::input('purchasedate') }}</td>
          </tr>

          @if ( Request::input('purchasedfromother') )

            <tr>
              <td class="l">{{ Lang::get('copy.form.field.from') }}</td>
              <td class="v">{{ ucfirst(Lang::get('copy.form.other')) }} / {{ Request::input('purchasedfromother') }}</td>
            </tr>

          @else

            <tr>
              <td class="l">{{ Lang::get('copy.form.field.from') }}</td>
              <td class="v">{{ Request::input('purchasedfrom') === 'HUDSON BAY' ? Lang::get('copy.form.hudsonsbay') : Request::input('purchasedfrom') }}</td>
            </tr>

          @endif

        </table>
      </div>

      <div id="layer-3" class="{{ Request::input('contact') ? 'on' : 'off' }}">
        {!! Lang::get('copy.form.checkbox1') !!}
      </div>

      <div id="layer-4">
        <div class="content">
          <h3>{{ Lang::get('copy.thanks.h1') }}</h3>
          <ul>
            <li>{{ Lang::get('copy.thanks.receive') }}</li>
            <li>{{ Lang::get('copy.thanks.print') }}</li>
            <li>{{ Lang::get('copy.thanks.cut') }}</li>
            <li>{{ Lang::get('copy.thanks.copy') }}</li>
            <li>{!! Lang::get('copy.thanks.send') !!}</li>
            <li>{!! Lang::get('copy.thanks.address') !!}</li>
            <li>{{ Lang::get('copy.thanks.infos') }}</li>
          </ul>
        </div>
      </div>

      <table id="layer-5" width: "775px;">
        <tr>
          <td width="100%" style="padding-top: 25px; font-size: 10px; line-height: 14px; text-align: center;">

            @if ( Request::input('lang') === 'en' )

              <p>
                <strong>$35 offer<br>
                  Offer is available from October 1st, 2015 at 9:00 a.m. (EDT) to November 15th, 2015 at 5:00 p.m. (EDT), or until supplies last. Valid exclusively online at Keurig.ca.</strong> Coupon is valid for a total of two (2) free boxes of K-Cup® pods, as follows: one (1) 30-count All-Time Favourites Variety Box (value of $21.49) and your choice of any one (1) box of K-Cup® pods (minimum value of $16.99). Free shipping is included. Only one (1) promo code can be used per order. Unique discount code must be applied at checkout. This promotion does not apply towards previously purchased merchandise. Offer subject to change or cancellation without prior notice. Shipping surcharges may be applied for locations outside of metropolitan areas. Cannot be combined with any other offer, coupon, or promotion.
              </p>
              <p>
                <strong>$70 offer<br>
                  Offer is available from October 1st, 2015 at 9:00 a.m. (EDT) to November 15th, 2015 at 5:00 p.m. (EDT), or until supplies last. Valid exclusively online at Keurig.ca.</strong> Coupon is valid for a total of four (4) free boxes of K-Cup® pods, as follows: one (1) free 30-count All-Time Favourites Variety Box (value of $21.49) and your choice of any three (3) boxes of K-Cup® pods (minimum value of $50.97). Free shipping is included. Only one (1) promo code can be used per order. Unique discount code must be applied at checkout. This promotion does not apply towards previously purchased merchandise. Offer subject to change or cancellation without prior notice. Shipping surcharges may be applied for locations outside of metropolitan areas. Cannot be combined with any other offer, coupon, or promotion.
              </p>

            @else

              <p>
                <strong>Offre 35 $<br>
                  L’offre est disponible du 1er octobre 2015 à 9 heures (HAE) au 15 novembre 2015 à 17 heures (HAE), ou jusqu’à épuisement des stocks. Valable exclusivement en ligne à Keurig.ca.</strong> Le coupon est valide pour un total de deux (2) boîtes gratuites de capsules K-Cup® comme suit : une (1) boîte Variée vos cafés favoris de 30 capsules (valeur de 21,49 $) et votre choix d’une (1) boîte de capsules K-Cup® (valeur minimum de 16,99 $). La livraison gratuite est incluse. Un (1) seul code promotionnel peut être utilisé par achat. Le code promotionnel unique doit être appliqué à l’achat. Cette offre ne s’applique pas aux achats antérieurs. L’offre peut être modifiée ou annulée sans préavis. Des frais d’expédition supplémentaires peuvent s’appliquer aux emplacements situés à l’extérieur des zones métropolitaines. Ne peut être combinée avec d’autres offres, codes coupons ou promotions.
              </p>
              <p>
                <strong>Offre 70 $<br>
                  L’offre est disponible du 1er octobre 2015 à 9 heures (HAE) au 15 novembre 2015 à 17 heures (HAE), ou jusqu’à épuisement des stocks. Valide exclusivement en ligne à Keurig.ca.</strong> Le coupon est valide pour un total de quatre (4) boîtes gratuites de 30 capsules K-Cup® comme suit : une (1) boîte Variée vos cafés favoris de 30 capsules (valeur de 21,49 $) et votre choix de trois (3) boîtes de capsules K-Cup® (valeur minimum de 50,97 $). La livraison gratuite est incluse. Un seul (1) code promotionnel peut être utilisé par achat. Le code promotionnel unique doit être appliqué à l’achat. Cette offre ne s’applique pas aux achats antérieurs. L’offre peut être modifiée ou annulée sans préavis. Des frais d’expédition supplémentaires peuvent s’appliquer aux emplacements situés à l’extérieur des zones métropolitaines. Ne peut être combinée avec d’autres offres, codes coupons ou promotions.
              </p>

            @endif

          </td>
        </tr>
      </table>

    </div>

    </body>
</html>