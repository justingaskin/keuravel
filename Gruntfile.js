module.exports = function(grunt){


  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass:
    {
      dist:
      {
        options:
        {
          style : 'compact',
          sourcemap : 'none'
        },
        files:
        {
          'public/css/style.css' : 'resources/assets/scss/style.scss',
          'public/css/pdf.css'   : 'resources/assets/scss/pdf.scss'
        }
      }
    },

    postcss:
    {
      options:
      {
        map: false,
        processors:
        [
          require('autoprefixer-core')
          ({
              browsers: ['last 3 versions','ie > 9']
          })
        ]
      },
      dist:
      {
        src: ['public/css/style.css']
      }
    },

    uglify:
    {
      dist:
      {
        files :
        {
          'public/js/scripts.min.js' :
          [
            'resources/assets/js/lib/*.js',
            'resources/assets/js/colorpicker.js',
            'resources/assets/js/scripts.js'
          ]
        }
      }
    },

    watch:
    {
      css:
      {
        files : ['resources/assets/scss/*.scss','resources/assets/js/**/*.js'],
        tasks : ['sass','postcss:dist','uglify:dist'],
        options:
        {
          spawn : false,
        }
      }, //end of sass watch

    },
  });


  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask( 'default', ['sass','postcss:dist','uglify:dist'] );

}